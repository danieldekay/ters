# == Route Map
#
#                   Prefix Verb   URI Pattern                              Controller#Action
#                     root GET    /                                        visitors#index
#         new_user_session GET    /users/sign_in(.:format)                 devise/sessions#new
#             user_session POST   /users/sign_in(.:format)                 devise/sessions#create
#     destroy_user_session DELETE /users/sign_out(.:format)                devise/sessions#destroy
#            user_password POST   /users/password(.:format)                devise/passwords#create
#        new_user_password GET    /users/password/new(.:format)            devise/passwords#new
#       edit_user_password GET    /users/password/edit(.:format)           devise/passwords#edit
#                          PATCH  /users/password(.:format)                devise/passwords#update
#                          PUT    /users/password(.:format)                devise/passwords#update
# cancel_user_registration GET    /users/cancel(.:format)                  devise/registrations#cancel
#        user_registration POST   /users(.:format)                         devise/registrations#create
#    new_user_registration GET    /users/sign_up(.:format)                 devise/registrations#new
#   edit_user_registration GET    /users/edit(.:format)                    devise/registrations#edit
#                          PATCH  /users(.:format)                         devise/registrations#update
#                          PUT    /users(.:format)                         devise/registrations#update
#                          DELETE /users(.:format)                         devise/registrations#destroy
#                    users GET    /users(.:format)                         users#index
#                          POST   /users(.:format)                         users#create
#                 new_user GET    /users/new(.:format)                     users#new
#                edit_user GET    /users/:id/edit(.:format)                users#edit
#                     user GET    /users/:id(.:format)                     users#show
#                          PATCH  /users/:id(.:format)                     users#update
#                          PUT    /users/:id(.:format)                     users#update
#                          DELETE /users/:id(.:format)                     users#destroy
#             inscriptions GET    /inscriptions(.:format)                  inscriptions#index
#                          POST   /inscriptions(.:format)                  inscriptions#create
#          new_inscription GET    /inscriptions/new(.:format)              inscriptions#new
#         edit_inscription GET    /inscriptions/:id/edit(.:format)         inscriptions#edit
#              inscription GET    /inscriptions/:id(.:format)              inscriptions#show
#                          PATCH  /inscriptions/:id(.:format)              inscriptions#update
#                          PUT    /inscriptions/:id(.:format)              inscriptions#update
#                          DELETE /inscriptions/:id(.:format)              inscriptions#destroy
#              suggestions GET    /suggestions(.:format)                   suggestions#index
#                          POST   /suggestions(.:format)                   suggestions#create
#           new_suggestion GET    /suggestions/new(.:format)               suggestions#new
#          edit_suggestion GET    /suggestions/:id/edit(.:format)          suggestions#edit
#               suggestion GET    /suggestions/:id(.:format)               suggestions#show
#                          PATCH  /suggestions/:id(.:format)               suggestions#update
#                          PUT    /suggestions/:id(.:format)               suggestions#update
#                          DELETE /suggestions/:id(.:format)               suggestions#destroy
#              invitations GET    /invitations(.:format)                   invitations#index
#                          POST   /invitations(.:format)                   invitations#create
#           new_invitation GET    /invitations/new(.:format)               invitations#new
#          edit_invitation GET    /invitations/:id/edit(.:format)          invitations#edit
#               invitation GET    /invitations/:id(.:format)               invitations#show
#                          PATCH  /invitations/:id(.:format)               invitations#update
#                          PUT    /invitations/:id(.:format)               invitations#update
#                          DELETE /invitations/:id(.:format)               invitations#destroy
#             portal_index GET    /portal/index(.:format)                  portal#index
#                          GET    /backend/invitations/:action(.:format)   backend/invitations#:action
#                          GET    /backend/registrations/:action(.:format) backend/registrations#:action
#                          GET    /backend/suggestions/:action(.:format)   backend/suggestion_processing#:action
#                  backend GET    /backend(.:format)                       backend/backend#index
#                     page GET    /pages/*id                               high_voltage/pages#show
#

Rails.application.routes.draw do

  get 'backend_tools/guest_emails'

  get 'errors/not_found'
  get 'errors/internal_server_error'

  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all

  devise_for :users
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
    match '/users/sign_out', to: 'devise/sessions#destroy', via: 'delete'
  end

  ## WARNING - needs to be commented out when creating a DATABASE from scratch
  ## BOOTSTRAPPING / SETUP
  ActiveAdmin.routes(self)

  root to: 'visitors#index'

  resources :users
  get '/users/:id/reset_password', to: 'users#reset_password'
  resources :inscriptions
  resources :suggested_participants
  resources :previous_participants
  resources :registrations, controller: 'inscriptions', type: 'Registration', model_name: 'Registration'

  match '/invitations/new_couple', to: 'invitations#new_couple', via: 'get'
  match '/invitations/create_couple', to: 'invitations#create_couple', via: 'post'
  get "/invitations/send_invite_to_user/:id", to: "invitations#send_invite_to_user"
  get "/invitation/accept/:id/:invitation_token", to: "invitations#accept"
  get "/invitation/convert/:id", to: "invitations#convert"

  resources :invitations do
    member do
      get 'reset_invitation_key'
      delete 'expire'
    end
  end

  get '/rsvp/:action', :controller => 'rsvp'


  scope :backend do
    resource :suggested_participants
    resource :invitations
    get '/statistics/:action', :controller => 'backend_statistics'
    get '/tools/:action', :controller => 'backend_tools'
  end
  match "/backend/suggestions/by_guests", to: 'backend/suggestion_processing#convert_to_invitation', via: 'post'
  match "/backend/suggestions/previous_participants", to: 'backend/suggestion_processing#convert_pp_to_invitation', via: 'post'
  match '/backend/invitations/button_action', :controller => 'backend/invitations', via: 'post'
  match '/backend/registrations/button_action', :controller => 'backend/registrations', via: 'post'

  match '/backend/registration/:id/show', :controller => 'backend/registrations', :action => 'show', via: 'get'
  match '/backend/registration/:id/edit', :controller => 'backend/registrations', :action => 'edit', via: 'get'
  match '/backend/registration/:id/update', :controller => 'backend/registrations', :action => 'admin_update', via: 'post'


  match '/portal', to: 'portal#index', via: 'get'
  get 'portal/index'
  get 'portal/guestlist'
  get 'portal/map'
  get 'portal/suggest'
  match '/portal/update_dietary', controller: 'portal', action: 'update_dietary', via: 'patch'

  get '/backend/invitations/:action', :controller => 'backend/invitations'
  get '/backend/registrations/:action', :controller => 'backend/registrations'
  get '/backend/suggestions/:action', :controller => 'backend/suggestion_processing'


  get '/backend', to: 'backend/backend#dashboard'
  get '/backend/stats', to: 'backend/backend#stats'
  get '/backend/stats2', to: 'backend/backend#stats2'
  get '/backend/signup_stats', to: 'backend/backend#signup_stats'
  get '/backend/signup_vs_accepted', to: 'backend/backend#signup_vs_accepted'
  get '/backend/guestlist', to: 'backend/backend#guestlist'
  get '/backend/aushang_guestlist', to: 'backend/backend#aushang_guestlist'
  get '/backend/csv', to: 'backend/backend#csv'
  get '/backend/fb_pics', to: 'backend/backend#fb_pics'
  get '/backend/view_portal_for_user_select', to: 'backend/backend#view_portal_for_user_select'
  get "/backend/user_portal_check/", to: "backend/backend#view_portal_for_user"

end
