# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path
Rails.application.config.assets.paths << Rails.root.join("vendor", "assets", "bower_components")

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w( search.js backend.css )
Rails.application.config.assets.precompile += %w( bootstrap.js bootstrap.css
                                    bootstrap_and_overrides.scss bootstrap_and_overrides.css
                                    backend.css backend.js
                                    sessions.css sessions.js
                                    guests.css guests.js
                                    requests.js shuttle_trips.js djs.css djs.js contents.js contents.css
                                    chroma.min.js
                                    active_admin.js
                                  )
Rails.application.config.assets.precompile += %w( active_admin.css wice_grid.css )
