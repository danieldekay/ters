def decorate_status_label(status, text)
  temp= "<span class='label label-#{status}'>" + text + "</span>"
  return temp.html_safe
end

def decorate_raw_object(object)
  require "awesome_print"
  #temp = ap object, options = {
  #    :indent     => 4,      # Indent using 4 spaces.
  #    :index      => true,   # Display array indices.
  #    :html       => true,  # Use ANSI color codes rather than HTML.
  #    :multiline  => true,   # Display in multiple lines.
  #    :plain      => false,  # Use colors.
  #    :raw        => false,  # Do not recursively format object instance variables.
  #    :sort_keys  => false,  # Do not sort hash keys.
  #    :limit      => true,  # Limit large output for arrays and hashes. Set to a boolean or integer.
  #    :color => {
  #        :args       => :pale,
  #        :array      => :white,
  #        :bigdecimal => :blue,
  #        :class      => :yellow,
  #        :date       => :greenish,
  #        :falseclass => :red,
  #        :fixnum     => :blue,
  #        :float      => :blue,
  #        :hash       => :pale,
  #        :keyword    => :cyan,
  #        :method     => :purpleish,
  #        :nilclass   => :red,
  #        :rational   => :blue,
  #        :string     => :yellowish,
  #        :struct     => :pale,
  #        :symbol     => :cyanish,
  #        :time       => :greenish,
  #        :trueclass  => :green,
  #        :variable   => :cyanish
  #    }
  #}
  #return temp.html_safe
  return simple_format object.to_yaml
end

def decorate_date_label(date, style, age = '0.days')
  span_class = (date + eval(age)).past? ? style : "label-default"
  date_text  = date.stamp('Aug-30').to_s
  export     = "<span class='label #{span_class}'>#{date_text}</span>".html_safe
end

## FORMATTERS & DECORATORS
def print_in_euro(value)
  return "#{value} &euro;".html_safe
end
