feature 'Active Admin page visits', :devise do

  before :each do
    user = FactoryGirl.create(:admin_user)
    signin(user.email, user.password)
  end

  scenario 'visit the active admin interface' do
    visit '/admin'
    expect(page).to have_content 'Dashboard'
  end
  scenario 'Processes' do
    visit '/admin/processes'
    expect(page).to have_content 'Dashboard'
  end
  scenario 'Mails' do
    visit '/admin/mails'
    expect(page).to have_content 'Dashboard'
  end
  scenario 'Couples' do
    visit '/admin/couples'
    expect(page).to have_content 'Dashboard'
  end
  scenario 'Messages' do
    visit '/admin/messages'
    expect(page).to have_content 'Dashboard'
  end
  scenario 'Guestlist' do
    visit '/admin/guestlists'
    expect(page).to have_content 'Dashboard'
  end
  scenario 'Waitinglist' do
    visit '/admin/waitinglists'
    expect(page).to have_content 'Dashboard'
  end
  scenario 'Raw Inscriptions' do
    visit '/admin/registrations'
    expect(page).to have_content 'Dashboard'
  end
  scenario 'Raw Invitations' do
    visit '/admin/invitations'
    expect(page).to have_content 'Dashboard'
  end
end
