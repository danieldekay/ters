feature 'Invitation workflow step 1 - create invitations', :devise do

  before :each do
    user = FactoryGirl.create(:admin_user)
    signin(user.email, user.password)
  end

  scenario 'Log in' do
    expect(page).to have_content I18n.t 'devise.sessions.signed_in'
    expect(page).to have_content 'Backend'
    expect(page.current_path).to eq backend_path
  end

  scenario 'Create an invitation' do
    params = FactoryGirl.attributes_for(:invitation)
    visit new_invitation_path
    fill_in 'First name', with: params[:first_name]
    fill_in 'Last name', with: params[:last_name]
    fill_in 'Email', with: params[:email]
    choose 'invitation_gender_man'
    choose 'invitation_role_leader'

    click_on 'Create Invitation'
    expect(page).to have_content 'Created invitation'
    expect(page).to have_content 'token'
    expect(page).to have_content Invitation.last.user.invitation_token
  end

  scenario 'Create an invitation - invalid' do
    params = FactoryGirl.attributes_for(:invitation)
    visit new_invitation_path
    click_on 'Create Invitation'
    expect(page).to have_content 'Please review the problems below'
  end

end

feature 'Invitation workflow step 2 - send emails', :devise do

  before :each do
    user                             = FactoryGirl.create(:admin_user)
    valid_params                     = { invitation: FactoryGirl.attributes_for(:invitation) }
    valid_params[:invitation][:user] = { email: 'test@test.com', password: 'password', password_confirmation: 'password' }

    @invitation  = Invitation::Create.(valid_params).model

    signin(user.email, user.password)
  end

  scenario 'invitation is visible on list' do
    visit '/backend/invitations/list'
    expect(page).to have_content(@invitation.user.email)
  end
  scenario 'invitation is visible on processing' do
    visit '/backend/invitations/processing'
    expect(page).to have_content("Open")
    expect(page).to have_content(@invitation.first_name.titleize)
    expect(page).to have_content(@invitation.last_name.titleize)
  end
  scenario 'invitation email is sent' do
    expect { visit "/invitations/send_invite_to_user/#{@invitation.user.id}" }.to change { ActionMailer::Base.deliveries.count }.by(1)
    expect(page).to have_content('sent')
  end


end
