# Feature: Registration Form
#   As a visitor
#   I want to see the registration form
#   So I can register for the event
feature 'Registration', :devise do

  scenario 'allows anonymous registration submission' do
    visit root_path

    # everything editable.
    expect(page).to have_css 'form #inscription_first_name'

    # invalid.
    click_button 'Submit Registration'
    expect(page).to have_css '.alert-error'

    # correct submit.
    test = FactoryGirl.attributes_for(:inscription)

    fill_in 'First name', with: test[:first_name]
    fill_in 'Last name', with: test[:last_name]
    fill_in 'Street', with: test[:street]
    fill_in 'Postcode/ZIP', with: test[:zip]
    fill_in 'City', with: test[:city]
    fill_in 'Birthday', with: test[:birthday].stamp('1977-12-31')
    select(test[:country], :from => 'Country')
    fill_in 'Do you want to leave us a message?', with: test[:message].to_s
    choose 'inscription_gender_man'
    choose 'inscription_role_leader'
    fill_in 'Email', with: test[:email]
    fill_in 'New password', with: 'password1234'
    fill_in 'Confirm your new password', with: 'password1234'
    click_button 'Submit Registration'

    expect(page.current_path).to eq portal_index_path
    expect(page).to have_content 'Hans'
    expect(page).to have_content 'status of your registration'
    expect(page).to have_content 'Logout'
  end

  scenario 'validates invalid submissions' do
    visit root_path

    # everything editable.
    expect(page).to have_css 'form #inscription_first_name'

    test = Inscription.new

    fill_in 'New password', with: 'p'
    click_button 'Submit Registration'

    expect(page.current_path).to eq inscriptions_path
    expect(page).to have_content 'is too short'
    expect(page).to have_content "can't be blank"
  end

end
