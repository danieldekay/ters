# Feature: 'About' page
#   As a visitor
#   I want to visit an 'about' page
#   So I can learn more about the website
feature 'Backend user workflow', :devise do

  before :each do
    user = FactoryGirl.create(:admin_user)
    signin(user.email, user.password)
  end

  scenario 'Log in' do
    expect(page).to have_content I18n.t 'devise.sessions.signed_in'
    expect(page).to have_content 'Backend'
    expect(page.current_path).to eq backend_path
  end

  scenario 'Create an invitation' do
    params = FactoryGirl.attributes_for(:invitation)
    visit new_invitation_path
    fill_in 'First name', with: params[:first_name]
    fill_in 'Last name', with: params[:last_name]
    fill_in 'Email', with: params[:email]
    choose 'invitation_gender_man'
    choose 'invitation_role_leader'

    click_on 'Create Invitation'
    expect(page).to have_content 'Created invitation'
    expect(page).to have_content 'token'
    expect(page).to have_content Invitation.last.user.invitation_token
  end

  scenario 'Create an invitation - invalid' do
    params = FactoryGirl.attributes_for(:invitation)
    visit new_invitation_path
    click_on 'Create Invitation'
    expect(page).to have_content 'Please review the problems below'
  end

end
