feature 'Backend page visits', :devise do

  before :each do
    user = FactoryGirl.create(:admin_user)
    signin(user.email, user.password)
  end

  scenario 'gets redirected to backend' do
    visit '/'
    expect(page).to have_content 'Dashboard'
  end
  scenario 'Registrations' do
    visit '/backend'
    expect(page).to have_content 'Registrations'
  end
  if Settings.features.suggestions.enabled
    scenario 'Suggestions' do
      visit '/backend'
      expect(page).to have_content 'Suggestions'
    end
  end
  if Settings.features.invitations.enabled
    scenario 'Invitations' do
      visit '/backend'
      expect(page).to have_content 'Invitations'
    end
  end
end
