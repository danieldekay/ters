# == Schema Information
#
# Table name: inscriptions
#
#  id                       :integer          not null, primary key
#  type                     :string
#  first_name               :string
#  last_name                :string
#  email                    :string
#  gender                   :string
#  role                     :string
#  birthday                 :date
#  street                   :string
#  city                     :string
#  zip                      :string
#  country                  :string
#  facebook                 :string
#  gmaps                    :boolean
#  latitude                 :float
#  longitude                :float
#  distance                 :decimal(, )
#  zodiac_sign_id           :integer
#  dietary_class            :integer          default(0)
#  dietary_incompatibility  :integer          default(0)
#  permission_to_store_data :boolean          default(TRUE)
#  message                  :text
#  telephone                :string
#  partner_email            :string
#  partner_first_name       :string
#  partner_last_name        :string
#  user_id                  :integer
#  suggestion_id            :integer
#  partner_id               :integer
#  created_at               :datetime
#  updated_at               :datetime
#  state                    :string           default("start")
#  processing_list          :string           default("start")
#  email_state              :string           default("start")
#

require 'rails_helper'

RSpec.describe Inscription, type: :model do

  describe '#name' do
    it 'returns full name' do
      ins    = FactoryGirl.create(:inscription)
      result = ins.name
      expect(result).to eq 'Hans Gruber'
    end
  end

  describe '#matched_inscriptions' do
    context 'when adding inscriptions' do
      let(:parent) { FactoryGirl.create(:alice) }
      let(:child) { FactoryGirl.create(:bob) }

      it "adds a child to the parent's matched_inscriptions collection" do
        expect(parent.matched_inscriptions).to be_empty
        parent.matched_inscriptions.replace [child]
        expect(parent.reload.matched_inscriptions).to match_array [child]
      end

      it "adds parent to the child's matched_inscriptions collection" do
        expect(child.matched_inscriptions).to be_empty
        parent.matched_inscriptions.replace [child]
        expect(child.reload.matched_inscriptions).to match_array [parent]
      end
    end

    context 'when removing inscriptions' do
      let(:parent) { FactoryGirl.create(:alice, matched_inscriptions: [child]) }
      let(:child) { FactoryGirl.create(:bob) }

      it "removes the child from the parent's matched_inscriptions collection" do
        expect(parent.matched_inscriptions).to eq [child]
        parent.matched_inscriptions.replace []
        expect(parent.reload.matched_inscriptions).to be_empty
      end

      it "removes parent from the child's matched_inscriptions collection" do
        expect(child.matched_inscriptions).to eq [parent]
        parent.matched_inscriptions.replace []
        expect(child.reload.matched_inscriptions).to be_empty
      end
    end

  end

  describe 'delegated methods' do
    it 'should delegate email to user' do
      ins =  FactoryGirl.create(:inscription)
      expect(ins.email).to eq(ins.user.email)

    end

  end
end
