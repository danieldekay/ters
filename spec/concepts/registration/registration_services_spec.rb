describe 'Registration services' do

  before :each do
    @reg = FactoryGirl.create(:alice)
    @reg = Inscription::ChangeType.({ model: @reg, target: Registration }).model
    expect(@reg.class).to eq(Registration)
  end

  it 'sign_up' do
    Registration::SignUp.({ model: @reg })
    expect(@reg.state).to eq('signed_up')
    expect(@reg.is_guest?).to be_falsey

  end

  it 'accept' do
    Registration::SignUp.({ model: @reg })
    Registration::Accept.({ model: @reg })
    expect(@reg.state).to eq('accepted')
    expect(@reg.state).to_not eq('signed_up')
    expect(@reg.is_guest?).to be_falsey
  end

  it 'doesnt accept everything' do
    Registration::Accept.run({ model: @reg })
    expect(@reg.state).to_not eq('accepted')
    expect(@reg.is_guest?).to be_falsey

  end

  it 'pays fully' do
    Registration::SignUp.({ model: @reg })
    Registration::Accept.({ model: @reg })
    Registration::Pay.({ model: @reg, amount: Settings.payment.price })
    expect(@reg.state).to eq('paid')
    expect(@reg.state).to_not eq('accepted')
    expect(@reg.is_guest?).to be_truthy
  end

  it 'can do free tickets' do
    Registration::SignUp.({ model: @reg })
    Registration::Accept.({ model: @reg })
    Registration::Pay.({ model: @reg, ticket: :free })
    expect(@reg.state).to eq('free_ticket')
    expect(@reg.is_guest?).to be_truthy
    expect(@reg.state).to_not eq('accepted')
  end

  it 'pays at door' do
    Registration::SignUp.({ model: @reg })
    Registration::Accept.({ model: @reg })
    Registration::Pay.({ model: @reg, ticket: :pays_at_door })
    expect(@reg.state).to eq('pays_at_door')
    expect(@reg.is_guest?).to be_truthy
    expect(@reg.state).to_not eq('accepted')
  end

  it 'pays partially' do
    Registration::SignUp.({ model: @reg })
    Registration::Accept.({ model: @reg })
    Registration::Pay.({ model: @reg, amount: (Settings.payment.price-1) })
    expect(@reg.state).to eq('pays_at_door')
    expect(@reg.is_guest?).to be_truthy
    expect(@reg.state).to_not eq('accepted')
  end

end
