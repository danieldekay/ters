require 'rails_helper'

RSpec.describe Registration, :type => :model do

  before(:each) do
    @inscription  = FactoryGirl.create(:alice)
    expect(@inscription.type).to eq(nil)
    res, op       = Inscription::ChangeType.run({ model: @inscription, target: Registration })
    @registration = op.model
  end

  describe 'states' do
    # state :start
    # state :signed_up # self-registration state after registration is sent to us, sends email receipt
    # state :accepted # triggered when registration is actually moved to "waiting for payment"
    # state :paid # = IS IN
    # state :pays_at_door # IS IN, and is noted on CHECKIN-List
    # state :free_ticket # IS IN, and is noted in stats
    # state :cancelled # IS OUT, and is noted in stats
    # state :refund # IS OUT, and is noted to be refunded


    it 'is a Registration' do
      expect(@registration.type).to eq('Registration')
    end

    it "has a starting state of 'start'" do
      expect(@registration.state).to eq('start')
    end

    it 'start -> accepted' do
      # start->submit goes to list of people who must pay
      # INVITATION workflow
      expect(@registration.submit).to be_truthy
      expect(@registration.state).to eq('accepted')
      expect(@registration.submit).to be_falsey # should no longer be submit-able
    end

    it 'start -> signed up' do
      # start->sign_up goes to list of applications

      expect(@registration.sign_up).to be_truthy
      expect(@registration.state).to eq('signed_up')
      expect(@registration.sign_up).to be_falsey # should no longer be sign_up-able
    end

    it 'signed_up -> submitted via: accept' do
      # signed_up -> submitted goes to list of people who must pay
      expect(@registration.sign_up).to be_truthy
      expect(@registration.accept).to be_truthy
      expect(@registration.state).to eq('accepted')
      expect(@registration.accept).to be_falsey # should no longer be sign_up-able
    end

    it 'payment_arrived' do
      # signed_up -> submitted  goes to list of people who must pay
      expect(@registration.sign_up).to be_truthy
      expect(@registration.accept).to be_truthy
      expect(@registration.payment_arrived).to be_truthy
      expect(@registration.state).to eq('paid')
      expect(@registration.payment_arrived).to be_falsey # should no longer be sign_up-able
      expect(@registration.is_guest?).to be_truthy
    end

  end

  describe 'paid ticktes' do
    before (:each) do
      expect(@registration.type).to eq('Registration')
      @registration.submit
      @registration.payment_arrived
    end
    it 'counts paid tickets on guestlist' do
      expect(@registration.is_guest?).to be_truthy
    end
    it 'marks cancelled paid tickets for refund' do
      @registration.cancel_registration
      expect(@registration.is_guest?).to be_falsey
      expect(@registration.state).to eq('refund')
    end
  end
  describe 'free ticktes' do
    before (:each) do
      expect(@registration.type).to eq('Registration')
      @registration.submit
      @registration.free_ticket
    end
    it 'counts free tickets on guestlist' do
      expect(@registration.is_guest?).to be_truthy
    end
    it 'marks cancelled paid tickets NOT for refund' do
      @registration.cancel_registration
      expect(@registration.is_guest?).to be_falsey
      expect(@registration.state).to eq('cancelled')
    end
  end
  describe 'pays at door' do
    before (:each) do
      expect(@registration.type).to eq('Registration')
      @registration.submit
      @registration.pays_at_door
    end
    it 'counts pays at door tickets on guestlist' do
      expect(@registration.is_guest?).to be_truthy
    end
    it 'marks cancelled paid tickets NOT for refund' do
      @registration.cancel_registration
      expect(@registration.is_guest?).to be_falsey
      expect(@registration.state).to eq('cancelled')
    end
  end

end
