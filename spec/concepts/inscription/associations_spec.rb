require 'rails_helper'

RSpec.describe Inscription, :type => :model do

  before(:each) do
    @pw                                = 'password'
    @valid_params                      = { inscription: FactoryGirl.attributes_for(:inscription) }
    @valid_params[:inscription][:user] = { email:    'test@test.com',
                                           password: 'password', password_confirmation: 'password' }

  end

  describe 'associations' do

    it 'has one user' do
      inscription = Inscription::Create.(@valid_params).model
      expect(inscription.user).to be_valid
    end

    it "it's user has only one inscription" do
      res, op = Inscription::Create.run(@valid_params)
      inscription = op.model
      expect(inscription.user.inscription).to be_valid
      expect(inscription.user.inscription).to eq(inscription)
    end
  end

end
