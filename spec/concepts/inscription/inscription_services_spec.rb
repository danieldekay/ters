describe 'Inscriptin STI' do

  # Suggestion > Invitation > Registration > EOL

  it 'make a Registration' do
    insc  = FactoryGirl.create(:alice)

    expect(insc.type).to eq(nil)

    Inscription::ChangeType.({
        model: insc,
        target: Registration
    })
    expect(insc.type).to eq('Registration')
  end



end
