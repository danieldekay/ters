require 'rails_helper'

RSpec.describe Inscription, :type => :model do

  before(:each) do
    @pw                                = 'password'
    @valid_params                      = { inscription: FactoryGirl.attributes_for(:inscription) }
    @valid_params[:inscription][:user] = { email:    'test@test.com',
                                           password: 'password', password_confirmation: 'password' }

  end

  describe 'validation' do
    it 'uniqueness' do
      res, op = Inscription::Create.run(@valid_params)
      res_dup, op_dup = Inscription::Create.run(@valid_params)
      expect(op_dup.contract.errors.to_hash[:'user.email']).to include('has already been taken')
    end


    it 'invalid=empty forms' do
      res, op = Inscription::Create.run(inscription: { first_name: '' })

      expect(op.model.persisted?).to be_falsey
      expect(res).to eq false
      expect(op.contract.errors.to_hash[:'first_name']).to include("can't be blank")
      expect(op.contract.errors.to_hash[:'last_name']).to include("can't be blank")
      expect(op.contract.errors.to_hash[:'role']).to include("can't be blank")
      expect(op.contract.errors.to_hash[:'gender']).to include("can't be blank")
      expect(op.contract.errors.to_hash[:'city']).to include("can't be blank")
    end

    it 'empty email' do
      params                              = @valid_params
      params[:inscription][:user][:email] = ''
      res, op                             = Inscription::Create.run(params)

      expect(op.model.persisted?).to be_falsey
      expect(res).to eq false
      expect(op.contract.errors.to_hash[:'user.email']).to include("can't be blank")
    end

  end

end
