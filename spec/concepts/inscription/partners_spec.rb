require 'rails_helper'

def create_valid_params_for_inscription_factory(factory)
  @valid_params                      = { inscription: FactoryGirl.attributes_for(factory) }
  @valid_params[:inscription][:user] = { email:    @valid_params[:inscription][:email],
                                         password: 'password', password_confirmation: 'password' }
end


RSpec.describe Inscription, :type => :model do


  before(:each) do
    @pw                                = 'password'
    @valid_params                      = { inscription: FactoryGirl.attributes_for(:alice) }
    @valid_params[:inscription][:user] = { email:    @valid_params[:inscription][:email],
                                           password: 'password', password_confirmation: 'password' }

  end

  describe 'missing partners' do

    it 'responds to partner with nil' do
      create_valid_params_for_inscription_factory(:alice)
      res, op = Inscription::Create.run(@valid_params)
      expect(op.model.user).to be_valid
      expect(op.model.partner).to eq(nil)
    end

    it 'finds a partner if that partner exists and has also said he is partnered' do
      alice = FactoryGirl.create(:alice)
      bob = FactoryGirl.create(:bob)
      expect(bob.partner).to eq(alice)
      expect(alice.partner).to eq(bob)
    end


  end

end
