require 'rails_helper'

RSpec.describe Inscription, :type => :model do
  @pw = 'password'

  describe 'Inscription CRUD' do


    it 'saves a valid inscription' do

      valid_params                      = { inscription: FactoryGirl.attributes_for(:inscription) }
      valid_params[:inscription][:user] = { email:    'test@test.com', password: 'password', password_confirmation: 'password' }

      inscription = Inscription::Create.(valid_params).model

      expect(inscription.persisted?).to be_truthy
      expect(inscription.user.persisted?).to be_truthy

      expect(inscription.first_name).to eq 'Hans'

      expect(inscription.user.present?).to be_truthy
      expect(inscription.email).to eq 'test@test.com'

      expect(User.last).to eq inscription.user
    end

    it 'rejects invalid forms' do

      res, op = Inscription::Create.run(inscription: { first_name: '' })

      expect(op.model.persisted?).to be_falsey
      expect(res).to eq false
      expect(op.contract.errors.to_s).to include ":first_name=>[\"can't be blank"

    end

  end

end
