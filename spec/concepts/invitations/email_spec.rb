require 'rails_helper'

RSpec.describe InvitationMailer, type: :mailer do
  describe '.invite' do

    before do
      valid_params                     = { invitation: FactoryGirl.attributes_for(:invitation) }
      valid_params[:invitation][:user] = { email: 'test@test.com', password: 'password', password_confirmation: 'password' }

      @invite = Invitation::Create.(valid_params).model
    end

    it "has appropriate subject" do
      @mail = InvitationMailer.invite(@invite.user)
      expect(@mail).to have_content("Subject: This is your personal invitation")
    end

    it "has the invitation link" do
      @mail = InvitationMailer.invite(@invite.user)
      expect(@mail).to have_content(@invite.user.invitation_token)
    end

    it "sends from the default email" do
      @mail = InvitationMailer.invite(@invite.user)
      expect(@mail).to be_delivered_from(Settings.marathon.email)
    end

    it "sends to the subscriber" do
      @mail = InvitationMailer.invite(@invite.user)
      expect(@mail).to be_delivered_to(@invite.user.email)
    end

    context "HTML body" do
      it "includes the confirm link" do
        @mail       = InvitationMailer.invite(@invite.user)
      end
    end

    context "plain text body" do
      it "includes the confirm URL" do
        @mail = InvitationMailer.invite(@invite.user)
      end
    end
  end
end
