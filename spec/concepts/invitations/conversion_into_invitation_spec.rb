require 'rails_helper'

RSpec.describe Invitation, :type => :model do

  describe 'Invitation creation from a ' do


    it 'suggestion' do
      valid_params = {suggestion: FactoryGirl.attributes_for(:suggestion) }
      suggestion = Suggestion::Create.(valid_params).model

      inscription = Invitation::FromSuggestion.(suggestion: suggestion).model

      expect(inscription.persisted?).to be_truthy
      expect(inscription.user.persisted?).to be_truthy

      expect(inscription.first_name).to eq suggestion.first_name

      expect(inscription.user.present?).to be_truthy
      expect(User.last).to eq inscription.user

      expect(inscription.user.email).to eq suggestion.email

    end

    it 'previous participant' do
      suggestion = FactoryGirl.create(:previous_participant)

      inscription = Invitation::FromSuggestion.(suggestion: suggestion).model

      expect(inscription.persisted?).to be_truthy
      expect(inscription.user.persisted?).to be_truthy

      expect(inscription.first_name).to eq suggestion.first_name

      expect(inscription.user.present?).to be_truthy
      expect(User.last).to eq inscription.user

      expect(inscription.user.email).to eq suggestion.email

    end

  end

end
