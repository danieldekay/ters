require 'rails_helper'

RSpec.describe Invitation, :type => :model do

  describe 'Invitation Creation' do


    it 'saves a valid inscription' do

      valid_params                      = { invitation: FactoryGirl.attributes_for(:invitation) }
      valid_params[:invitation][:user] = { email:    'test@test.com', password: 'password', password_confirmation: 'password' }

      inscription = Invitation::Create.(valid_params).model

      expect(inscription.persisted?).to be_truthy
      expect(inscription.user.persisted?).to be_truthy

      expect(inscription.first_name).to eq valid_params[:invitation][:first_name]

      expect(inscription.user.present?).to be_truthy
      expect(inscription.email).to eq valid_params[:invitation][:user][:email]

      expect(User.last).to eq inscription.user
    end

    it 'rejects invalid forms' do

      res, op = Inscription::Create.run(inscription: { first_name: '' })

      expect(op.model.persisted?).to be_falsey
      expect(res).to eq false
      expect(op.contract.errors.to_s).to include ":first_name=>[\"can't be blank"

    end

  end

end
