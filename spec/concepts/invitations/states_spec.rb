require 'rails_helper'

RSpec.describe 'Invitation' do

  before :each do
    valid_params                     = { invitation: FactoryGirl.attributes_for(:invitation) }
    valid_params[:invitation][:user] = { email: 'test@test.com', password: 'password', password_confirmation: 'password' }

    @invitation = Invitation::Create.(valid_params).model

  end

  describe 'States' do
    it 'has initial state -start-' do
      expect(@invitation.state).to eq('start')
    end
  end

  describe 'associations' do

    it 'has a user' do
      expect(@invitation.user).to be_instance_of(User)
    end

    it 'the user has it' do
      user = @invitation.user
      expect(user.invitation).to eq(@invitation)
    end

  end

end
