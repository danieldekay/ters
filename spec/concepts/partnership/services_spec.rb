require 'rails_helper'

RSpec.describe Partnership::DefinePartner, :type => :model do

  describe 'it works' do

    before :each do
      @alice  = FactoryGirl.create(:alice)
      @bob    = FactoryGirl.create(:bob)
      res, op = Partnership::DefinePartner.run(model: @alice, partner: @bob)
    end

    it 'on database level' do
      string = ''
      Partnership.all.each do |ps|
        string += ps.to_s
      end
      expect(string).to include('->')
      expect(string).to include('as partner')
    end

    it 'alice' do
      expect(@alice.partner).to eq(@bob)
    end
    it 'bob' do
      expect(@bob.partner).to eq(@alice)
    end
  end
end
