require 'rails_helper'

RSpec.describe Suggestion, :type => :model do

  before(:each) do
    @alice  = FactoryGirl.create(:alice)
    @charly = FactoryGirl.create(:charly)
    Suggestion::SuggestedBy.(model: @alice, suggests: @charly)
  end

  describe 'Alice suggests Charly' do

    it 'Alice knows her suggested' do
      expect(@alice.suggestions).to be_truthy
      expect(@alice.suggestions).to include @charly
    end

    it 'Charly knows who suggested him' do
      expect(@charly.suggested_by).to be_truthy
      expect(@charly.suggested_by).to include @alice
    end

  end

end
