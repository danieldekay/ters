# == Schema Information
#
# Table name: partnerships
#
#  id                     :integer          not null, primary key
#  inscription_id         :integer
#  matched_inscription_id :integer
#  kind                   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :partnership do
    inscription nil
    matched_inscription nil
  end
end
