FactoryGirl.define do
  factory :registration do
    fake_first_name = 'Hans'
    fake_last_name  = 'Gruber'

    first_name { fake_first_name }
    last_name { fake_last_name }
    email { Faker::Internet.email }
    street { 'Street Address' }
    zip { '12345' }
    city { 'Mainhausen' }
    country { %w(Germany France Switzerland).sample }
    message { Faker::Lorem.sentences(3) }
    gender { [:man, :woman].sample }
    role { [:leader, :follower, :both].sample }
    dietary_class { %w(omnivore vegetarian vegan).sample }
    dietary_incompatibility { ['noLactose', ''] }
    birthday { Faker::Date.between(19.years.ago, 60.years.ago) }

  end

end
