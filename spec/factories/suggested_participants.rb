# == Schema Information
#
# Table name: suggested_participants
#
#  id             :integer          not null, primary key
#  first_name     :string
#  last_name      :string
#  email          :string
#  gender         :string
#  role           :string
#  city           :string
#  country        :string
#  facebook       :string
#  message        :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  inscription_id :integer
#

FactoryGirl.define do
  factory :suggested_participant do
    
  end

end
