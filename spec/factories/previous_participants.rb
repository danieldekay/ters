# == Schema Information
#
# Table name: previous_participants
#
#  id             :integer          not null, primary key
#  first_name     :string
#  last_name      :string
#  email          :string
#  city           :string
#  country        :string
#  birthday       :date
#  role           :string
#  known_from     :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  zodiac_sign_id :integer
#  facebook       :string
#

FactoryGirl.define do
  factory :previous_participant do |s|
    s.first_name { Faker::Name.first_name }
    s.last_name { Faker::Name.last_name }
    s.email { Faker::Internet.email }
    s.city { Faker::Address.city }
    s.country 'Germany'
    s.role { %w(Leader Follower Both).sample }
  end

end
