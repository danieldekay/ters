# == Schema Information
#
# Table name: suggestions
#
#  id            :integer          not null, primary key
#  first_name    :string(255)
#  last_name     :string(255)
#  email         :string(255)
#  city          :string(255)
#  country       :string(255)
#  birthday      :date
#  role          :string(255)
#  message       :text
#  experience    :text
#  facebook_id   :string(255)
#  status        :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  user_id       :integer
#  partner_email :string(255)
#  partner_name  :string(255)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :suggestion do |s|
    s.first_name { Faker::Name.first_name }
    s.last_name { Faker::Name.last_name }
    s.email { Faker::Internet.email }
    s.city { Faker::Address.city }
    s.country 'Germany'
    s.birthday { Faker::Date.birthday(18, 65) }
    s.role { %w(Leader Follower Both).sample }
    s.gender { %w(Man Woman).sample }
    s.message { Faker::Lorem.sentence(word_count=50) }

    user
  end
end
