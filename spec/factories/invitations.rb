# == Schema Information
#
# Table name: invitations
#
#  id              :integer          not null, primary key
#  first_name      :string(255)      not null
#  last_name       :string(255)      not null
#  email           :string(255)      not null
#  city            :string(255)
#  country         :string(255)
#  role            :string(255)      not null
#  message         :text
#  facebook        :string(255)
#  inviter_id      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :integer
#  status          :text
#  scopable_status :string(255)
#  couple_status   :text             default("solo")
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invitation do |i|

    i.first_name { Faker::Name.first_name }
    i.last_name { Faker::Name.last_name }
    i.email { Faker::Internet.email }
    i.city { Faker::Address.city }
    i.country { Faker::Address.country }
    i.message { Faker::Lorem.sentences(3) }
    i.role { [:leader, :follower, :both].sample }

    user

  end
end
