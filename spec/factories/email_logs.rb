# == Schema Information
#
# Table name: email_logs
#
#  id             :integer          not null, primary key
#  message_ID     :string
#  status         :string
#  sent_at        :datetime
#  smart_email_ID :string
#  email          :string
#  message        :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryGirl.define do
  factory :email_log do
    MessageID "MyString"
Status "MyString"
SentAt "2016-05-29 12:09:18"
SmartEmailID "MyString"
Email "MyString"
Message "MyString"
  end

end
