# == Schema Information
#
# Table name: inscriptions
#
#  id                       :integer          not null, primary key
#  type                     :string
#  first_name               :string
#  last_name                :string
#  email                    :string
#  gender                   :string
#  role                     :string
#  birthday                 :date
#  street                   :string
#  city                     :string
#  zip                      :string
#  country                  :string
#  facebook                 :string
#  gmaps                    :boolean
#  latitude                 :float
#  longitude                :float
#  distance                 :decimal(, )
#  zodiac_sign_id           :integer
#  dietary_class            :integer          default(0)
#  dietary_incompatibility  :integer          default(0)
#  permission_to_store_data :boolean          default(TRUE)
#  message                  :text
#  telephone                :string
#  partner_email            :string
#  partner_first_name       :string
#  partner_last_name        :string
#  user_id                  :integer
#  suggestion_id            :integer
#  partner_id               :integer
#  created_at               :datetime
#  updated_at               :datetime
#  state                    :string           default("start")
#  processing_list          :string           default("start")
#  email_state              :string           default("start")
#

FactoryGirl.define do

  factory :inscription do
    fake_first_name = 'Hans'
    fake_last_name  = 'Gruber'

    first_name { fake_first_name }
    last_name { fake_last_name }
    email { Faker::Internet.email }
    partner_email { Faker::Internet.email }
    street { 'Street Address' }
    zip { '12345' }
    city { 'Mainhausen' }
    country { %w(Germany France Switzerland).sample }
    message { Faker::Lorem.sentences(3).to_s }
    gender { [:man, :woman].sample }
    role { [:leader, :follower, :both].sample }
    dietary_class { %w(omnivore vegetarian vegan).sample }
    dietary_incompatibility { ['noLactose', ''] }
    birthday { Faker::Date.between(19.years.ago, 60.years.ago) }

    user
  end

  factory :known_email_inscription, :parent => :inscription do |p|
    known_email = 'test@test.com'
    email { known_email }
    association :user, email: known_email

  end

  factory :fake_inscription, :parent => :inscription do |p|
    fake_first_name = Faker::Name.first_name
    fake_last_name  = Faker::Name.last_name
    fake_email      = "#{fake_first_name}.#{fake_last_name}@#{Faker::Internet.domain_name}"

    first_name { fake_first_name }
    last_name { fake_last_name }
    email { fake_email }
    street { Faker::Address.street_address }
    zip { Faker::Address.zip }
    city { Faker::Address.city }

    association :user, email: fake_email
  end

  factory :ffaker_inscription, :parent => :inscription do |p|
    fake_first_name = FFaker::Name.first_name
    fake_last_name  = FFaker::Name.last_name
    fake_email      = "#{fake_first_name}.#{fake_last_name}@#{FFaker::Internet.domain_name}"

    first_name { fake_first_name }
    last_name { fake_last_name }
    email { fake_email }
    street { FFaker::AddressDE.street_address }
    zip { FFaker::AddressDE.zip_code }
    city { FFaker::AddressDE.city }

    association :user, email: fake_email
  end

  factory :alice, :parent => :inscription do |p|
    email { 'alice@test.com' }
    first_name { 'Alice' }
    partner_first_name { 'Bob' }
    partner_email { 'bob@test.com' }

    association :user, email: 'alice@test.com'
  end
  factory :bob, :parent => :inscription do |p|
    partner_email { 'alice@test.com' }
    first_name { 'Bob' }
    partner_first_name { 'Alice' }
    email { 'bob@test.com' }

    association :user, email: 'bob@test.com'
  end
  factory :charly, :parent => :inscription do |p|
    first_name { 'Charly' }
    partner_first_name { '' }
    partner_last_name { '' }
    partner_email { '' }
    email { 'charly@test.com' }
    association :user, email: 'charly@test.com'
  end


end
