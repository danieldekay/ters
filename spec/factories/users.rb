# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string
#  admin                  :boolean          default(FALSE)
#  invitation_token       :string
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_id          :integer
#  invited_by_type        :string
#  invitations_count      :integer          default(0)
#

FactoryGirl.define do
  factory :user do |f|
    fake_email = Faker::Internet.email

    f.name { 'Test User' }
    f.email { fake_email }
    f.password { 'password1234' }
    f.password_confirmation { 'password1234' }
  end

  factory :useralice, :parent => :user do |p|
    email { 'alice@test.com' }
  end
  factory :userbob, :parent => :user do |p|
    email { 'bob@test.com' }
  end
  factory :usercharly, :parent => :user do |p|
    email { 'charly@test.com' }
  end


end
