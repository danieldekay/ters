namespace :ters do

  task :leader do
    valid_params                               = { inscription: FactoryGirl.attributes_for(:fake_inscription) }
    valid_params[:inscription][:role]          = :leader
    valid_params[:inscription][:gender]        = :man
    valid_params[:inscription][:first_name]    = Faker::Name.first_name
    valid_params[:inscription][:last_name]     = Faker::Name.last_name
    valid_params[:inscription][:partner_email] = nil
    valid_params[:inscription][:user]          = { email:                 valid_params[:inscription][:email], password: 'password',
                                                   password_confirmation: 'password' }

    model = Inscription::Create.(valid_params).model

    model = Inscription::ChangeType.({ model: model, target: Registration }).model
    model = Registration::SignUp.({ model: model }).model
    puts "Single.  #{model.name}/#{model.email} - #{model.role}"
  end


  task :follower do
    valid_params                               = { inscription: FactoryGirl.attributes_for(:fake_inscription) }
    valid_params[:inscription][:role]          = :follower
    valid_params[:inscription][:gender]        = :woman
    valid_params[:inscription][:partner_email] = nil
    valid_params[:inscription][:user]          = { email:                 valid_params[:inscription][:email],
                                                   password:              'password',
                                                   password_confirmation: 'password' }
    model                                      = Inscription::Create.(valid_params).model

    model = Inscription::ChangeType.({ model: model, target: Registration }).model
    model = Registration::SignUp.({ model: model }).model
    puts "Single.  #{model.name}/#{model.email} - #{model.role}"
  end

  task :add_twenty do
    5.times {
      5.times { Rake::Task["ters:follower"].invoke }
      3.times { Rake::Task["ters:leader"].invoke }
    }
  end

  task :accept do
    Registration.signed_up.sample(10).each do |reg|
      reg.accept
    end
  end

  task :pay do
    Registration.accepted.sample(2).each do |reg|
      reg.payment_arrived
    end
  end

  task :invitation_state do
    Invitation.all.each do |i|
      if i.start?
        if i.user.invitation_sent_at
          i.email_is_sent
        end
      end
    end
  end

  task :geocode do
    Registration.all.each do |i|
      if !i.geocoded?
        i.geocode
        i.save
      end
    end
  end


end
