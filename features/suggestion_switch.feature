Feature: Suggestion Hints for non guests

  Background: A user is registered
    Given I visit the home page
    Then I see a registration form
    And I fill in a valid form for Bob
    And I click on the submit button
    Then I should be redirected to my portal page

  Scenario: Suggestions are turned on, but I am not a guest yet
    Given Settings: Suggestions are set to true
    Then I see the suggestion info

  Scenario: Suggestions are turned off, but I am not a guest yet
    Given Settings: Suggestions are set to false
    Then I do not see the suggestion info
