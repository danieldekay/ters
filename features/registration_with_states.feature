Feature: Users can register and see their registration status on the portal

  Background: A user is registered
    Given I visit the home page
    Then I see a registration form
    And I fill in a valid form for Bob
    And I click on the submit button
    Then I should be redirected to my portal page
    When I am not logged in

  Scenario: New registration
    And I log in as Bob
    Then I should be redirected to my portal page
    Then I see that my registration status is new

  Scenario: Waitinglist
    Given My registration has been moved to the waiting list
    And I log in as Bob
    Then I should be redirected to my portal page
    Then I see that my registration status is new

  Scenario: Accepted
    Given My registration has been accepted
    And I log in as Bob
    Then I should be redirected to my portal page
    Then I see the payment info
    Then I should be able to make a suggestion

  Scenario: Paid
    Given My registration has been accepted
    And I have paid
    And I log in as Bob
    Then I should be redirected to my portal page
    Then I should see that I am IN
    Then I should be able to make a suggestion
