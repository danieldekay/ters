Feature: Users can interact with the site in many ways.

  Scenario: A User visits the home page
    Given I visit the home page
    Then I see a registration form

  Scenario: A User visits homepage and fills in the form
    Given I visit the home page
    And I fill in a valid form for Bob
    When I click on the submit button
    Then I should be redirected to my portal page

  Scenario:
    Given I visit the home page
    When I click on the submit button
    Then I see a registration form
    And I see an error message in the signup form

  Scenario Outline: Portal Navigation
    Given I am <User>, registered, and in state <state>
    Given I am not logged in
    When I log in as <User>
    And I should be redirected to my portal page
    Then I should <visibility> the navigation elements for <navigation>

    Examples:
      | User  | state     | navigation  | visibility |
      | Alice | signed_up | guestlist   | not see    |
      | Alice | accepted  | guestlist   | not see    |
      | Alice | paid      | guestlist   | see        |
      | Alice | cancelled | guestlist   | not see    |
      | Alice | accepted  | suggestions | see        |
      | Alice | paid      | suggestions | see        |

  Scenario: Guests can see guestlist
    Given I am Alice, registered, and in state paid
    Given I am not logged in
    When I log in as Alice
    When I visit the guestlist
    Then I see my name
