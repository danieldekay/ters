Feature: Inscription Unit Tests

  Scenario: It validates unique email
    Given The database has no Registrations
    Given I am Alice and sign up
    Then I see that my registration status is new
    Given I am not logged in
    Given I am Alice and sign up
    Then I see an error message in the signup form
