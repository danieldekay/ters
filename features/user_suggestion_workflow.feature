Feature: Suggestion Workflow

  Background: A user is registered, and accepted
    Given Settings: Suggestions are set to true
    Given I visit the home page
    Then I see a registration form
    And I fill in a valid form for Bob
    And I click on the submit button
    Then I should be redirected to my portal page
    When I am not logged in
    Given My registration has been accepted
    And I log in as Bob
    * Setup variable for current registration
    Then I should be redirected to my portal page

  Scenario: Have no suggestions
    Then I see the suggestion info
    Then I should have no suggestions

  Scenario: Make a suggestion
    Then I see the suggestion info
    When I go to the suggestion page
    And I enter a new suggestion form
    Then I have a suggestion
    Then I can see that I have suggested a new user
