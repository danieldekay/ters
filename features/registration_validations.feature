Feature: Validations during signup process

  Background: A user is registered
    Given I visit the home page
    Then I see a registration form
    And I fill in a valid form for Alice
    And I click on the submit button
    And I sign out

  Scenario: I sign up with the same data
    Given I visit the home page
    Then I see a registration form
    And I fill in a valid form for Alice
    And I click on the submit button
    Then I see an error message in the signup form
