Feature: Users can sign up with a partner

  Scenario: Alice (but not yet Bob)
    Given I am Alice and sign up
    And I see that my registration status is new
    And I see that my partner has not signed up yet

  Scenario: Alice + Bob
    Given I am Alice and sign up
    And I am not logged in
    Given I am Bob and sign up
    And I see that my registration status is new
    And I see that my partner has signed up

  Scenario: Charly
    Given I am Charly and sign up
    And I see that my registration status is new
    And I see that I have not registered with a partner
