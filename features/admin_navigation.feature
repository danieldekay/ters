Feature: Admins can navigate to all relevant pages in backend

  Background: Admin user is logged in
    Given I visit the home page
    Given I am a admin user
    Then I am redirected to the backend

  Scenario: Active Admin Dashboard Navigation
    Given I visit the admin interface
    Then I am in the active admin dashboard
    And I visit the admin screen for Guestlist
    And I visit the admin screen for Couples
    And I visit the admin screen for Messages
    And I visit the admin screen for Processes
    And I visit the admin screen for Processes
    And I visit the admin screen for Mails
    And I visit the admin screen for Comments

  Scenario: Backend Pages
    Given I am redirected to the backend
    Then I visit the backend screen for Dashboard
    Then I visit the backend screen for Registrations
    Then I visit the backend screen for ActiveAdmin
