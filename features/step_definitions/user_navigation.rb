Given(/^I visit the home page$/) do
  visit root_path
end


Then(/^I see a registration form$/) do
  expect(page).to have_content('First name')
  expect(page).to have_content('Last name')
  expect(page).to have_content('Email')
  expect(page).to have_content('Role')
  expect(page).to have_content('Gender')
end


And(/^I fill in a valid form for (Alice|Bob|Charly)$/) do |name|
  # correct submit.
  test = FactoryGirl.attributes_for(name.downcase.to_sym)
  user = FactoryGirl.attributes_for("user#{name.downcase}".to_sym)

  fill_in 'First name', with: test[:first_name]
  fill_in 'Last name', with: test[:last_name]
  fill_in 'Street', with: test[:street]
  fill_in 'Postcode/ZIP', with: test[:zip]
  fill_in 'City', with: test[:city]
  fill_in 'Birthday', with: test[:birthday].stamp('1977-12-31')
  select(test[:country], :from => 'Country')
  fill_in 'Do you want to leave us a message?', with: test[:message]
  choose 'man'
  choose 'leader'
  fill_in 'Email', with: user[:email]
  fill_in 'New password', with: user[:password]
  fill_in 'Confirm your new password', with: user[:password]
  fill_in 'Partner email', with: test[:partner_email]

end


And(/^I complete the valid form for (Alice|Bob|Charly)$/) do |name|
  # correct submit.
  test = FactoryGirl.attributes_for(name.downcase.to_sym)
  user = FactoryGirl.attributes_for("user#{name.downcase}".to_sym)

  fill_in 'Street', with: test[:street]
  fill_in 'Postcode/ZIP', with: test[:zip]
  fill_in 'City', with: test[:city]
  fill_in 'Birthday', with: test[:birthday].stamp('1977-12-31')
  select(test[:country], :from => 'Country')
  fill_in 'Do you want to leave us a message?', with: test[:message]
  fill_in 'New password', with: user[:password]
  fill_in 'Confirm your new password', with: user[:password]
  fill_in 'Partner email', with: test[:partner_email]

end


When(/^I click on the submit button$/) do
  click_on 'Submit Registration'
end


And(/^I see an error message in the signup form$/) do
  expect(page.current_path).to eq inscriptions_path
  expect(page).to have_selector '.has-error'
end

Given /^I am not logged in$/ do
  # visit('/users/sign_out') # ensure that at least
  click_on 'Logout'
  expect(page.current_path).to eq root_path
end

Given /^I am not signed in$/ do
  current_driver = Capybara.current_driver
  begin
    Capybara.current_driver = :rack_test
    page.driver.submit :delete, "/users/sign_out", {}
  ensure
    Capybara.current_driver = current_driver
  end
end


And(/^I sign out$/) do
  click_on 'Logout'
end


When(/^I log in as (.*)$/) do |name|
  # There are only factories for Alice, Bob and Charly
  user = FactoryGirl.attributes_for("user#{name.downcase}".to_sym)

  visit '/users/sign_in'
  fill_in :user_email, with: user[:email]
  fill_in :user_password, with: user[:password]
  click_button 'Login'
end
