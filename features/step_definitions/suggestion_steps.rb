Given(/^Settings: Suggestions are set to (.*)$/) do |flag|
  Settings.features.suggestions.enabled = flag
end

Then(/^I see the suggestion info$/) do
  expect(page).to have_content('Suggest friends')
end

Then(/^I do not see the suggestion info$/) do
  expect(page).to_not have_content('Suggest friends')
end

Then(/^I should have no suggestions$/) do
  expect(page).to have_content('not suggested')
end

When(/^I go to the suggestion page$/) do
  click_on('Click here')
  expect(page.current_path).to eq portal_suggest_path
end

And(/^I enter a new suggestion form$/) do
  click_on('Suggest Friend')

  test = FactoryGirl.attributes_for(:suggestion)
  fill_in 'First name', with: test[:first_name]
  fill_in 'Last name', with: test[:last_name]
  fill_in 'Email', with: test[:email]
  fill_in 'City', with: test[:city]
  choose 'suggested_participant_gender_man'
  choose 'suggested_participant_role_leader'
  select(test[:country], :from => 'Country')

  click_on('Suggest your friend')
end

Then(/^I can see that I have suggested a new user$/) do
  expect(page).to have_content('You have 2 suggestions left')
  expect(page).to have_content('Edit')
end

When(/^The suggestion is accepted$/) do
  expect(page).to have_content('invited')
end

Then(/^I see that my suggestion has been invited$/) do
  expect(page).to have_content('invited')
end

Then(/^I have a suggestion$/) do
  expect(page).to have_content('1 Suggestions')
  expect(@reg.suggestions.count).to eq 1
end
