Then(/^I should be redirected to my portal page$/) do
  expect(page.current_path).to eq portal_index_path
end

Then(/^I see that my registration status is new$/) do
  expect(body).to have_css '.signed_up'
end

Then(/^I see the payment info$/) do
  expect(body).to have_content('Please pay')
  if Settings.payment.bank.enabled
    expect(body).to have_content(Settings.payment.bank.iban)
    expect(body).to have_content(Settings.payment.bank.bic)
    expect(body).to have_content(Settings.payment.bank.account)
    expect(body).to have_content(Settings.payment.bank.bank)
  end
  if Settings.payment.paypal.enabled
    expect(body).to have_content("Paypal")
    expect(body).to have_content(Settings.payment.paypal.email)
  end
  if Settings.payment.cash.enabled
    expect(body).to have_content("cash")
    expect(body).to have_content(Settings.payment.cash.price)
  end
end

Then(/^I should see that I am IN$/) do
  expect(body).to have_css '.paid'
end


Then(/^I should (.*) the navigation elements for (.*)$/) do |visibility, navigation|
  case visibility
    when "see"
      expect(body).to have_css ".#{navigation}"
    when "not see"
      expect(body).not_to have_css ".#{navigation}"
  end
end


Then(/^I see my name$/) do
  expect(page).to have_content(Registration.first.first_name.titleize)
end

When(/^I visit the guestlist$/) do
  visit '/portal/guestlist'
end


Then(/^I should be able to make a suggestion$/) do
  if Settings.features.suggestions.enabled
    expect(body).to have_css ".add_suggestion"
  else
    expect(body).not_to have_css ".add_suggestion"
  end
end


Then(/^I should not be able to make a suggestion$/) do
  if Settings.features.suggestions.enabled
    expect(body).to have_css ".add_suggestion_info"
  else
    expect(body).not_to have_css ".add_suggestion_info"
  end
end


And(/^I should see that I need to pay$/) do
  expect(body).to have_content("pay")
end
