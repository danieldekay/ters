And(/^I create a new invitation for Bob$/) do
  visit '/invitations/new'
  # invite bob
  test = FactoryGirl.attributes_for('userbob'.to_sym)

  fill_in 'First name', with: 'Bob'
  fill_in 'Last name', with: 'Meier'
  fill_in 'City', with: test[:city]
  choose 'invitation_gender_man'
  choose 'invitation_role_leader'
  fill_in 'Email', with: test[:email]

  click_button 'Create Invitation'
  expect(page.current_path).to eq('/backend/invitations/processing')
end

Then(/^There should be an invitation for Bob$/) do
  expect(Invitation.last.first_name).to eq 'Bob'
end

And(/^I send the invitation$/) do
  visit '/backend/invitations/processing'
  visit "/invitations/send_invite_to_user/#{Invitation.last.user.id}"
end

Given(/^I receive an invitation email$/) do
  # pending
end

And(/^I click on the link in the email$/) do
  @invitation_link ||= "/invitation/accept/#{Invitation.last.user.id}/#{Invitation.last.user.invitation_token}"
  visit @invitation_link
end

Then(/^I should see my invitation$/) do
  expect(page.current_path).to eq rsvp_index_path
  expect(page).to have_content(Invitation.last.first_name)
  expect(page).to have_css('.invitation')
  expect(page).to have_css('.accept')
  expect(page).to have_css('.decline')
end

And(/^I accept the invitation$/) do
  expect(page.current_path).to eq rsvp_index_path
  expect(page).to have_css('#accept_button')
  click_on 'accept_button'
end

And(/^I accept the terms/) do
  expect(page.current_path).to eq rsvp_accept_path
  click_on 'accept_terms_button'
end

And(/^I decline the invitation$/) do
  expect(page.current_path).to eq rsvp_index_path
  expect(page).to have_css('#decline_button')
  click_button 'decline_button'
end

And(/^My invitation is no longer valid$/) do
  expect(Invitation.last.state).to eq 'declined'
end

And(/^The form is pre\-filled with my data$/) do
  expect(page).to have_content(Inscription.last.first_name)
  expect(page).to have_content(Inscription.last.last_name)
  expect(page).to have_content(Inscription.last.email)
end


Then(/^I see the sorry page$/) do
  expect(page.current_path).to eq rsvp_decline_path
end


When(/^I click my old invitation link$/) do
  visit @invitation_link
end

Then(/^The link is not valid anymore$/) do
  expect(page).to have_content("valid")
end

And(/^I don't react in the time limit$/) do
  @invitation_link = "/invitation/accept/#{Invitation.last.user.id}/#{Invitation.last.user.invitation_token}"
  puts @invitation_link
  Invitation::Expire.(invitation: Invitation.last)
end
