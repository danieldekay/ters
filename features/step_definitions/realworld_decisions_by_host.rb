Given(/^My registration has been moved to the waiting list$/) do
  pending
  Registration::List.(model: Registration.first, list: :waitinglist)
end

Given(/^My registration has been accepted$/) do
  Registration::Accept.(model: Registration.first)
end
