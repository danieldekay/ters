Then(/^An error in the form is thrown$/) do
  expect(page).to have_css('.has-error')
end


Given(/^An inscription is created for anyone$/) do
  steps '
  * I visit the home page
  * I see a registration form
  * I fill in a valid form
  * I click on the submit button
  Given I am not logged in
  '
end

Given(/^An inscription is created for (Alice|Bob|Charly)$/) do |name|
  steps '
  * I visit the home page
  * I see a registration form
  * I fill in a valid form for #{name}
  * I click on the submit button
  Given I am not logged in
  '
end


Given(/^I am (Alice|Bob|Charly) and sign up$/) do |name|
  steps %{
  * I visit the home page
  * I see a registration form
  * I fill in a valid form for #{name}
  * I click on the submit button
  }
end

And(/^I see that my partner has not signed up yet$/) do
  expect(page).to have_css('.partner_not_registered')
end

And(/^I see that my partner has signed up$/) do
  expect(page).to have_css('.partner_has_registered')
end

And(/^I see that I have not registered with a partner$/) do
  expect(page).to have_css('.no_partner')
end


Given(/^The database has no Registrations$/) do
  expect(Registration.all.count).to eq(0)
end


Given(/^I am (.*), registered, and in state (.*)$/) do |name, state|
  steps %{
  * I visit the home page
  * I see a registration form
  * I fill in a valid form for #{name}
  * I click on the submit button
  And the registration is placed in state #{state}
        }
end

And(/^the registration is placed in state (.*)$/) do |state|
  case state
    when "signed_up"

    when "accepted"
      Registration::Accept.(model: Registration.first)
    when "paid"
      Registration::Accept.(model: Registration.first)
      Registration::Pay.(model: Registration.first, amount: Settings.payment.price)
    when "cancelled"
      pending
  end

end
