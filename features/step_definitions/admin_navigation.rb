Given /^I am a admin user$/ do
  user = FactoryGirl.create(:admin_user)

  visit '/users/sign_in'
  fill_in 'user_email', :with => user.email
  fill_in 'user_password', :with => user.password
  click_button 'Login'
end



Then(/^I am redirected to the backend$/) do
  expect(page.current_path).to eq backend_path
end


Given(/^I visit the admin interface$/) do
  visit '/admin'
end

Then(/^I am in the active admin dashboard$/) do
  expect(page.current_path).to eq admin_root_path
end

And(/^I visit the admin screen for (\w+)$/) do |menuitem|
  click_on menuitem
end


And(/^I visit the backend screen for (\w+)$/) do |menuitem|
  click_on menuitem
end
