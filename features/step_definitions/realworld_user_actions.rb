And(/^I have paid$/) do
  Registration::Pay.(model: Registration.last, amount: Settings.payment.price)
end

And(/^I have cancelled$/) do
  pending
  Registration::Pay.(model: Registration.last)
end
