Feature: Admins can invite Users, and they can accept the invitation

  Background: An admin creates an invitation
    Given I am a admin user
    Then I am redirected to the backend
    And I create a new invitation for Bob
    Then There should be an invitation for Bob
    And I send the invitation
    And I sign out

  Scenario: Invitation
    Given I receive an invitation email
    And I click on the link in the email
    Then I should see my invitation

  @javascript
  Scenario: Accept
    Given I receive an invitation email
    And I click on the link in the email
    And I accept the invitation
    And I accept the terms
    Then I see a registration form
    And The form is pre-filled with my data
    When I complete the valid form for Bob
    When I click on the submit button
    Then I should be redirected to my portal page
    And I should see that I need to pay

  @javascript
  Scenario: Accept with missing data
    Given I receive an invitation email
    And I click on the link in the email
    And I accept the invitation
    And I accept the terms
    Then I see a registration form
    And The form is pre-filled with my data
    When I click on the submit button
    When I complete the valid form for Bob
    When I click on the submit button
    Then I should be redirected to my portal page
    And I should see that I need to pay

  @javascript
  Scenario: Decline
    Given I receive an invitation email
    And I click on the link in the email
    And I decline the invitation
    Then I see the sorry page
    And My invitation is no longer valid
    # Does the declined invitation link still work?
    When I click my old invitation link
    Then The link is not valid anymore
    And I see a registration form

  @javascript
  Scenario: Wait too long
    Given I receive an invitation email
    And I don't react in the time limit
    And I click on the link in the email
    Then The link is not valid anymore
    And I see a registration form
