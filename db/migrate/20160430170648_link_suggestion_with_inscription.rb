class LinkSuggestionWithInscription < ActiveRecord::Migration
  def change
    add_column :suggested_participants, :inscription_id, :integer
  end
end
