class CreatePartnerships < ActiveRecord::Migration
  def change
    create_table :partnerships do |t|
      t.references :inscription, index: true, foreign_key: true
      t.references :matched_inscription, index: true
      t.string :kind

      t.timestamps null: false
    end

  end
end
