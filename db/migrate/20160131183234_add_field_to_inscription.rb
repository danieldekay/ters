class AddFieldToInscription < ActiveRecord::Migration
  def change
    add_column :inscriptions, :state, :string, :default => 'start'
    add_column :inscriptions, :processing_list, :string, :default => 'start'
    add_column :inscriptions, :email_state, :string, :default => 'start'

  end
end
