class CreateInscriptions < ActiveRecord::Migration
  def change
    create_table :inscriptions do |t|
      t.string :type
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :gender
      t.string :role
      t.date :birthday
      t.string :street
      t.string :city
      t.string :zip
      t.string :country
      t.string :facebook
      t.boolean :gmaps
      t.float :latitude
      t.float :longitude
      t.decimal :distance
      t.integer :zodiac_sign_id
      t.integer :dietary_class, :default => 0
      t.integer :dietary_incompatibility, :default => 0
      t.boolean :permission_to_store_data, :default => true
      t.text :message
      t.string :telephone
      t.string :partner_email
      t.string :partner_first_name
      t.string :partner_last_name
      t.references :user
      t.references :suggestion
      t.references :partner
      t.timestamps
    end

  end
end
