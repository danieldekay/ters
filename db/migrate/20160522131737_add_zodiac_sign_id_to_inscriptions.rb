class AddZodiacSignIdToInscriptions < ActiveRecord::Migration
  def change
    add_index  :inscriptions, :zodiac_sign_id

    Registration.all.each do |inscription|
      inscription.send(:update_sign_id)
      inscription.save
    end
  end
end
