class CreateSuggestedParticipants < ActiveRecord::Migration
  def change
    create_table :suggested_participants do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :gender
      t.string :role
      t.string :city
      t.string :country
      t.string :facebook
      t.text :message
      t.timestamps null: false
    end
  end
end
