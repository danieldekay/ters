class CreateEmailLogs < ActiveRecord::Migration
  def change
    create_table :email_logs do |t|
      t.string :message_ID
      t.string :status
      t.datetime :sent_at
      t.string :smart_email_ID
      t.string :email
      t.string :message

      t.timestamps null: false
    end
  end
end
