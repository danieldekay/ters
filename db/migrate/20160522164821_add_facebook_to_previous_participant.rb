class AddFacebookToPreviousParticipant < ActiveRecord::Migration
  def change
    add_column :previous_participants, :facebook, :string
  end
end
