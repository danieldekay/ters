class AddRegistrationIdToEmailLog < ActiveRecord::Migration
  def change
    add_column :email_logs, :inscription_id, :integer
  end
end
