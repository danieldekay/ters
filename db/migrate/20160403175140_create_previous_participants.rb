class CreatePreviousParticipants < ActiveRecord::Migration
  def change
    create_table :previous_participants do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :city
      t.string :country
      t.date :birthday
      t.string :role
      t.string :known_from

      t.timestamps null: false
    end
  end
end
