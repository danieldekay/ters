class AddZodiacSignIdToPreviousParticipants < ActiveRecord::Migration
  def change
    add_column :previous_participants, :zodiac_sign_id, :integer
    add_index  :previous_participants, :zodiac_sign_id
    
    PreviousParticipant.all.each do |previous_participant|
      previous_participant.send(:update_sign_id)
      previous_participant.save
    end
  end
end
