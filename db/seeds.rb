# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
user = CreateAdminService.new.call
# AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
CreateDemoDatabase.new.call
CreateDemoDatabase.new.move_along
