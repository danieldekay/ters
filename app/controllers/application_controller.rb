class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  include Trailblazer::Operation::Controller

  private
  def logged_in
    authenticate_user!
  end

  def logged_in_as_admin
    authenticate_user!
    unless current_user.admin?
      redirect_to root_path
    end
  end
end
