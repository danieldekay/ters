# == Schema Information
#
# Table name: previous_participants
#
#  id             :integer          not null, primary key
#  first_name     :string
#  last_name      :string
#  email          :string
#  city           :string
#  country        :string
#  birthday       :date
#  role           :string
#  known_from     :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  zodiac_sign_id :integer
#  facebook       :string
#

class PreviousParticipantsController < ApplicationController
  respond_to :html
  before_filter :logged_in_as_admin

  layout 'backend'

  def new
    form PreviousParticipant::Create
  end

  def edit
    @form = PreviousParticipant.find(params[:id])
  end

  def show
    @model = PreviousParticipant.find(params[:id])
  end

  def update
    run PreviousParticipant::Update do |op|
      return redirect_to op.model
    end
    render action: :new
  end


  def destroy
  end

  private
  def render_form
    render text:   concept("previous_participant/cell/form", @operation),
           layout: true
  end

end
