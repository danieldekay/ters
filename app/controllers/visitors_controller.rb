class VisitorsController < ApplicationController
  def index

    if user_signed_in?
      if current_user.admin?
        redirect_to backend_path
      else
        redirect_to portal_index_path
      end
    elsif !Settings.features.registrations.enabled
      redirect_to '/users/sign_in'
    end

    form Inscription::Create

  end

end
