class Backend::SuggestionProcessingController < ApplicationController

  before_filter :authenticate_user!
  before_filter :authenticate_admin_user!

  layout 'backend'


  def index
    case params[:filter]
      when 'leaders'
        @where_clause = "Suggestion.where(:role => 'leader')"
      when 'followers'
        @where_clause = "Suggestion.where(:role => 'follower')"
      when 'accepted'
        @where_clause = "Suggestion.where(:scopable_status => 'accepted')"
      when 'no_email'
        @where_clause = "Suggestion.includes(:user).where('user.id is NULL')"
      when 'expired'
        @where_clause = 'Suggestion.joins(:user).where(user: {invitation_sent_at: (Time.now.midnight - 100.days)..(Time.now.midnight - 10.days)})'
      else
        @where_clause = 'Suggestion'
    end

    @tasks_grid = initialize_grid(eval(@where_clause),
                                  :include         => [:user],
                                  :order           => 'id',
                                  :order_direction => 'desc',
                                  :per_page        => 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @suggestions }
    end
  end

  def previous_participants

    tag_filter = PreviousParticipant.tags_on(:events).map { |i| "'" + i.to_s + "'" }.join(" || ")

    case params[:filter]
      when 'all'
        @where_clause = "PreviousParticipant"
      when 'leaders'
        @where_clause = "PreviousParticipant.where(:role => 'leader')"
      when 'both'
        @where_clause = "PreviousParticipant.where(:role => 'both')"
      when 'followers'
        @where_clause = "PreviousParticipant.where(:role => 'follower')"
      when 'empty'
        @where_clause = "PreviousParticipant.where('role IS ?', nil)"
      when nil
        @where_clause = "PreviousParticipant"
      else
        @where_clause = "PreviousParticipant.tagged_with('#{params[:filter]}')"
    end

    if params[:zodiac]
      @where_clause = "PreviousParticipant.#{params[:zodiac]}"
    end


    @tasks_grid = initialize_grid(eval(@where_clause),
                                  :order           => 'first_name',
                                  :order_direction => 'asc',
                                  :per_page        => 50)

    if params[:g] && params[:g][:selected]
      @selected = params[:g][:selected]
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @suggestions }
    end
  end

  def by_guests

    case params[:filter]
      when 'all'
        @where_clause = "SuggestedParticipant"
      when 'leaders'
        @where_clause = "SuggestedParticipant.where(:role => 'leader')"
      when 'followers'
        @where_clause = "SuggestedParticipant.where(:role => 'follower')"
      else
        @where_clause = "SuggestedParticipant"
    end

    @tasks_grid = initialize_grid(eval(@where_clause),
                                  :order           => 'id',
                                  :order_direction => 'desc',
                                  :per_page        => 50)

    if params[:g] && params[:g][:selected]
      @selected = params[:g][:selected]
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @suggestions }
    end
  end


  def convert_to_invitation
    flash[:notice] = "Creating #{params[:grid][:selected].count} invitations: "

    params[:grid][:selected].each do |id|
      res, operation = Invitation::FromSuggestion.run(suggestion: SuggestedParticipant.find(id)) do |op|
        Invitation::PrepareUser.(invitation: op.model)
        flash[:notice] += "#{op.model.user.email} "
        # => the record will be created, but the invitation email will not be sent
      end
    end

    redirect_to backend_suggestions_by_guests_path
  end

  def convert_pp_to_invitation
    flash[:notice] = "Creating #{params[:grid][:selected].count} invitations: "

    params[:grid][:selected].each do |id|
      res, operation = Invitation::FromSuggestion.run(suggestion: PreviousParticipant.find(id)) do |op|

        Invitation::PrepareUser.(invitation: op.model)
        flash[:notice] += "#{op.model.user.email} "
        # => the record will be created, but the invitation email will not be sent
      end

    end

    redirect_to backend_suggestions_previous_participants_path
  end
end
