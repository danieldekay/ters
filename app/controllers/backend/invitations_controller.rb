class Backend::InvitationsController < ApplicationController

  before_filter :authenticate_user!
  layout 'backend'


  def list
    @tasks_grid = initialize_grid(Invitation,
                                  :include => [:user],
                                  :order   => 'id')

    @invitations = Invitation.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @invitations }
    end
  end

  def processing
    case params[:filter]
      when 'no_reply'
        @where_clause = "Invitation.where(:state => 'paid')"
      when 'declined'
        @where_clause = "Invitation.where(:state => 'declined')"
      when 'sent'
        @where_clause = "Invitation.sent"
      when 'not_sent'
        @where_clause = "Invitation.not_sent"
      when 'open'
        @where_clause = "Invitation.open"
      when 'no_email'
        @where_clause = "Invitation.joins(:user).where(:users => {:invitation_sent_at => nil})"
      when 'expired'
        @where_clause = "Invitation.expired"
      else
        @where_clause = 'Invitation'
    end

    @tasks_grid = initialize_grid(eval(@where_clause),
                                  :include         => [:user],
                                  :order           => 'id',
                                  :order_direction => 'desc',
                                  :per_page        => 100)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @invitations }
    end
  end

  def messages
    @tasks_grid = initialize_grid(Invitation.where("message <> ''"),
                                  :include         => [:user],
                                  :order           => 'id',
                                  :order_direction => 'desc',
                                  :per_page        => 100)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @invitations }
    end
  end

  def create_from_previous_participant
    @invitation = Invitation.new_with_previous_participant(PreviousParticipant.find(params[:id]), current_user)
    #@invitation.inviter_id=current_user.id

    User.invite!(:email => @invitation.email, :name => @invitation.name) do |u|
      u.skip_invitation = true
      u.facebook_id     = @invitation.facebook
      u.save
      @invitation.user=u
      #@invitation.save!
    end
    # => the record will be created, but the invitation email will not be sent

    if @invitation.save
      redirect_to edit_invitation_path(@invitation)
    end
  end

  def button_action
    if !params[:grid][:selected]
      flash[:error] = "Please select a row before pressing the buttons."
    elsif params[:reset_keys] == ""
      reset_keys
    elsif params[:send_invitation] == ""
      send_invitation
    elsif params[:decline_invitation] == ""
      decline_invitation
    elsif params[:delete_invitation] == ""
      delete_invitation
    elsif params[:expire_invitation] == ""
      expire_invitation
    else
    end
    redirect_to :action => :processing
  end

  def reset_keys
    flash[:notice] = "Resetting keys for "
    params[:grid][:selected].each do |id|
      res, operation = Invitation::ResetInvitationKey.run(invitation: Invitation.find(id)) do |op|
        flash[:notice] += "#{op.model.email} "
        # => the record will be created, but the invitation email will not be sent
      end
    end
  end

  def send_invitation
    flash[:notice] = "Sending invitation emails for #{params[:grid][:selected]}"
    params[:grid][:selected].each do |id|
      res, operation = Invitation::SendEmail.run(user: Invitation.find(id).user) do |op|
        flash[:notice] += "#{op.model.email} "
      end
    end
  end

  def decline_invitation
    flash[:notice] = "Manually declining invitation for #{params[:grid][:selected]}"
    params[:grid][:selected].each do |id|
      res, operation = Invitation::Decline.run(user: Invitation.find(id).user) do |op|
        flash[:notice] += "#{op.model.email} "
      end
    end
  end

  def delete_invitation
    flash[:error] = "Deleting invitation for #{params[:grid][:selected]}"
    params[:grid][:selected].each do |id|
      Invitation.find(id).delete
    end
  end

  def expire_invitation
    flash[:error] = "Expiring invitation for #{params[:grid][:selected]}"
    params[:grid][:selected].each do |id|
      res, operation = Invitation::Expire.run(model: Invitation.find(id)) do |op|
        flash[:error] += "#{op.model.email} "
      end
    end

  end


end
