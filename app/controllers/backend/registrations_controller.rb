class Backend::RegistrationsController < InscriptionsController
  before_filter :authenticate_user!

  layout 'backend'

  def redirect_back(*params)
    # TODO make buttons create params that send the people back where they were
    uri                    = session[:original_uri]
    session[:original_uri] = nil
    if uri
      redirect_to uri
    else
      redirect_to(*params)
    end
  end

  def processing
    case params[:filter]
      when 'paid'
        @where_clause = "Registration.where(:state => 'paid')"
      when 'accepted'
        @where_clause = "Registration.where(:state => 'accepted')"
      when 'free_ticket'
        @where_clause = "Registration.free"
      when 'signed_up'
        @where_clause = "Registration.where(:state => 'signed_up')"
      when 'error'
        @where_clause = "Registration.where(:state => 'start')"
      when 'cancelled'
        @where_clause = "Registration.where(:state => 'cancelled')"
      when 'refund'
        @where_clause = "Registration.where(:state => 'refund')"
      else
        @where_clause = 'Registration'
    end

    @tasks_grid = initialize_grid(eval(@where_clause),
                                  :include         => [:user],
                                  :order           => 'updated_at',
                                  :order_direction => 'desc')

    @registrations = Registration.all
  end

  def dietary
    case params[:filter]
      when 'omnivore'
        @where_clause = "Registration.mostly_in.tagged_with('omnivore', :any => true, :on => :dietary_class)"
      when 'vegetarian'
        @where_clause = "Registration.mostly_in.tagged_with('vegetarian', :any => true, :on => :dietary_class)"
      when 'vegan'
        @where_clause = "Registration.mostly_in.tagged_with('vegan', :any => true, :on => :dietary_class)"
      when 'no_lactose'
        @where_clause = "Registration.mostly_in.tagged_with('no_lactose', :any => true, :on => :dietary_incompatibility)"
      when 'no_gluten'
        @where_clause = "Registration.mostly_in.tagged_with('no_gluten', :any => true, :on => :dietary_incompatibility)"
      when 'untagged'
        @where_clause = "Registration.mostly_in.tagged_with(ActsAsTaggableOn::Tag.all.map(&:to_s), :exclude => true)"
      else
        @where_clause = 'Registration'
    end

    @tasks_grid = initialize_grid(eval(@where_clause),
                                  :include         => [:user],
                                  :order           => 'updated_at',
                                  :order_direction => 'desc')

    @registrations = Registration.all
  end

  def roles
    @where_clause = "Registration.mostly_in"
    case params[:filter]
      when 'men'
        @where_clause += ".men"
      when 'women'
        @where_clause += ".women"
      when 'leaders'
        @where_clause += ".leaders"
      when 'followers'
        @where_clause += ".followers"
      when 'both'
        @where_clause += ".both"
      when 'untagged'
        @where_clause += ".where('gender IS ?', nil)"
      else
        @where_clause = 'Registration'
    end

    @tasks_grid = initialize_grid(eval(@where_clause),
                                  :include         => [:user],
                                  :order           => 'updated_at',
                                  :order_direction => 'desc')

    @registrations = Registration.all
  end


  def debug
    @tasks_grid = initialize_grid(Registration,
                                  :include         => [:user],
                                  :order           => 'id',
                                  :order_direction => 'desc')

    @registrations = Registration.all
  end

  def messages
    @tasks_grid = initialize_grid(Registration.where("message <> ''"),
                                  :include         => [:user],
                                  :order           => 'id',
                                  :order_direction => 'desc')

    @registrations = Registration.all
  end

  def partners
    @tasks_grid = initialize_grid(Registration.where("partner_email <> ''"),
                                  :include         => [:user],
                                  :order           => 'id',
                                  :order_direction => 'desc')

    @registrations = Registration.all
  end

  def emails
    case params[:filter]
      when 'paid'
        @where_clause = "Registration.where(:state => 'paid')"
      when 'accepted'
        @where_clause = "Registration.where(:state => 'accepted')"
      when 'free_ticket'
        @where_clause = "Registration.free"
      when 'signed_up'
        @where_clause = "Registration.where(:state => 'signed_up')"
      when 'error'
        @where_clause = "Registration.where(:state => 'start')"
      when 'cancelled'
        @where_clause = "Registration.where(:state => 'cancelled')"
      when 'refund'
        @where_clause = "Registration.where(:state => 'refund')"
      else
        @where_clause = 'Registration'
    end

    case params[:email_filter]
      when 'please_pay_sent'
        @email_clause = "Registration.where(:state => 'accepted')"
      when 'please_pay_not_sent'
        @email_clause = "Registration.where(:state => 'accepted')"
      when 'IN_sent'
        @email_clause = "Registration.where(:state => 'paid')"
      when 'IN_not_sent'
        @email_clause = "Registration.where(:state => 'paid')"
    end

    if @email_clause
      @where_clause = @email_clause
    end

    @tasks_grid = initialize_grid(eval(@where_clause),
                                  :include         => [:user, :email_logs],
                                  :order           => 'id',
                                  :order_direction => 'desc')

    @registrations = Registration.all
  end

  def map
    @registrations = Registration.all
    @markers       = MapWorker.generate_markers

    respond_to do |format|
      format.html { render :layout => 'backend_fullscreen' }
    end
  end

  def show
    @reg = Registration.find(params[:id])

    require 'createsend'
    # Authenticate with your API key
    @auth           = { :api_key => Rails.application.secrets.email_provider_apikey }

  end

  def button_action
    if !params[:grid][:selected]
      flash[:error] = "Please select a row before pressing the buttons."
    elsif params[:send_please_pay] == ""
      send_please_pay
    elsif params[:send_you_are_in] == ""
      send_you_are_in
    elsif params[:send_next_email] == ""
      send_next_email
    elsif params[:payment_arrived] == ""
      payment_arrived
    elsif params[:suggest_partner] == ""
      suggest_partner
    elsif params[:cancel_registration] == ""
      cancel_registration
    elsif params[:pays_at_door] == ""
      pays_at_door
    elsif params[:free_ticket] == ""
      free_ticket
    else
    end

    redirect_back(:action => 'processing')
  end

  def suggest_partner
    flash[:notice] = "Making a suggestion from partner for IDs #{params[:grid][:selected]}: "
    flash[:error]  = ''

    params[:grid][:selected].each do |id|
      reg = Registration.find(id)
      if reg
        @me            = reg
        res, operation = SuggestedParticipant::SuggestPartner.run(model: @me) do |op|
          flash[:notice] = "Suggested \"#{op.model.name} with email: #{op.model.user.email rescue 'unkown'}\""

          begin
            sugg = op.model
            SuggestedParticipant::SuggestedBy.(model: @me, suggests: sugg)
            Rails.logger.info { "#{current_user.inscription.name} suggested #{sugg}" }
          rescue => e
            Rails.logger.error { "#{e.message} #{e.backtrace.join('\n')}" }
          end
        end

      else
        flash[:error] += "ERROR: #{reg.email} not in -accepted- state "
      end
    end

  end

  def send_please_pay
    flash[:notice] = "Sending please_pay emails for registrations with IDs #{params[:grid][:selected]}: "
    flash[:error]  = ''

    params[:grid][:selected].each do |id|
      reg = Registration.find(id)
      if reg.accepted?
        res, operation = Registration::SendPleasePay.run(user: reg.user) do |op|
          flash[:notice] += "SUCCESS: #{op.model.email} "
        end
      else
        flash[:error] += "ERROR: #{reg.email} not in -accepted- state "
      end
    end

  end

  def payment_arrived
    flash[:notice] = "Payment arrived for registrations with IDs #{params[:grid][:selected]}: "
    flash[:error]  = ''

    params[:grid][:selected].each do |id|
      reg = Registration.find(id)
      if  reg.respond_to? :payment_arrived
        res, operation = Registration::Pay.run(model: reg, amount: Settings.payment.price) do |op|
          flash[:notice] += "#{op.model.email} "
        end
      else
        flash[:error] += "ERROR: #{reg.email} not in -accepted- state "
      end
    end
  end

  def free_ticket
    flash[:notice] = "Free tickets for registrations with IDs #{params[:grid][:selected]}: "
    flash[:error]  = ''

    params[:grid][:selected].each do |id|
      reg = Registration.find(id)
      if reg.respond_to? :free_ticket
        res, operation = Registration::Pay.run(model: reg, ticket: :free) do |op|
          flash[:notice] += "#{op.model.email} (free ticket) "
        end
      else
        flash[:error] += "ERROR: #{reg.email} not in -accepted- state "
      end
    end
  end

  def pays_at_door
    flash[:notice] = "Pays at Door marked for registrations with IDs #{params[:grid][:selected]}: "
    flash[:error]  = ''

    params[:grid][:selected].each do |id|
      reg = Registration.find(id)
      if reg.respond_to? :pays_at_door
        res, operation = Registration::Pay.run(model: reg, ticket: :pays_at_door) do |op|
          flash[:notice] += "#{op.model.email} (pays at door) "
        end
      else
        flash[:error] += "ERROR: #{reg.email} not in -accepted- state "
      end
    end
  end


  def cancel_registration
    flash[:notice] = "Cancelling registrations with IDs #{params[:grid][:selected]}: "
    flash[:error]  = ''

    params[:grid][:selected].each do |id|
      reg = Registration.find(id)
      if reg
        res, operation = Registration::Cancel.run(model: reg) do |op|
          flash[:notice] += "#{op.model.email} (free ticket) "
        end
      else
        flash[:error] += "ERROR: #{reg.email} not in -accepted- state "
      end
    end
  end


  def send_you_are_in
    flash[:notice] = "Sending you_are_in emails for registrations with IDs #{params[:grid][:selected]}: "
    flash[:error]  = ''


    params[:grid][:selected].each do |id|
      reg = Registration.find(id)
      if reg.is_guest?
        res, operation = Registration::SendYouAreIn.run(user: reg.user) do |op|
          flash[:notice] += "#{op.model.email} "
        end
      else
        flash[:error] += "ERROR: #{reg.email} not on -guest_list-"
      end
    end
  end

  def edit
    @reg = Registration.find(params[:id])
  end

  def admin_update
    @reg                                                 = Registration.find(params[:id])
    params[:registration][:dietary_incompatibility_list] = params[:registration][:dietary_incompatibility_list].join(',')
    @reg.update_attributes(reg_params)
    if @reg.save
      redirect_to "/backend/registration/#{@reg.id}/show"
    else
      redirect_to "/backend/registration/#{@reg.id}/edit"
    end
  end


  private

  def reg_params
    params.require(:registration).permit(:first_name, :last_name, :partner_email, :role, :gender,
                                         :dietary_class, :dietary_incompatibility, :dietary_incompatibility_list,
                                         :dietary_class_list,
                                         :birthday, :facebook)
  end

end
