class Backend::BackendController < ApplicationController
  before_filter :logged_in_as_admin

  #layout 'backend_sb_admin'
  layout 'backend'

  def index

  end

  def dashboard

    invitations   = Invitation.all
    registrations = Registration.all
    suggestions   = Suggestion.all

    @incomplete_invitations = Invitation.all
    @cancelled_invitations  = Registration.cancelled

    startDate = Settings.marathon.registration.start rescue Date.today
    endDate     = DateTime.now + 1.day
    dateCounter = startDate
    @countHash  = Hash.new
    @sumHash    = Hash.new
    # @log = ""


    while dateCounter < endDate do
      temp = dateCounter.to_date.to_s

      tempHash              = Hash.new
      tempHash['regs']      = Registration.mostly_in.where(:updated_at => dateCounter.beginning_of_day..dateCounter.end_of_day).count
      tempHash['invs']      = Invitation.where(:created_at => dateCounter.beginning_of_day..dateCounter.end_of_day).count + Registration.where(:created_at => dateCounter.beginning_of_day..dateCounter.end_of_day).count
      tempHash['open_invs'] = Invitation.sent.where(:updated_at => dateCounter.beginning_of_day..dateCounter.end_of_day).open.count
      tempHash['potential_regs'] = Invitation.where(:created_at => dateCounter.beginning_of_day..dateCounter.end_of_day).open.count + Registration.where(:created_at => dateCounter.beginning_of_day..dateCounter.end_of_day).count
      tempHash['suggs']     = SuggestedParticipant.where(:created_at => dateCounter.beginning_of_day..dateCounter.end_of_day).count

      @countHash[temp] = tempHash

      dateCounter = dateCounter + 1.day

    end

    firstRound   = :true
    @previousVal = Hash.new
    @countHash.sort.each do |key, val|
      # @log += "<br/>KEY="
      # @log += key
      # @log += " VAL="
      # @log += val.map{|k,v| "#{k}=#{v}"}.join('/')
      # @log += " PREV="
      # @log += @previousVal.map{|k,v| "#{k}=#{v}"}.join('/')

      if firstRound == :true
        @sumHash[key] = val
        @previousVal  = val
        firstRound    = :false
      else
        tempHash              = Hash.new
        tempHash['regs']      = @previousVal['regs'] + val['regs']
        tempHash['potential_regs']      = @previousVal['potential_regs'] + val['potential_regs']
        tempHash['invs']      = @previousVal['invs'] + val['invs']
        tempHash['open_invs'] = @previousVal['open_invs'] + val['open_invs']
        tempHash['suggs']     = @previousVal['suggs'] + val['suggs']
        @sumHash[key]         = tempHash
        @previousVal          = tempHash
      end
    end
  end

  def stats
    redirect_to portal_path, alert: 'Not authorized' unless current_user.is_admin?

    states  = %w(submitted paid free_ticket pays_at_door)
    @states = states

    @leaders   = Registration.basically_in.leaders
    @followers = Registration.basically_in.followers
    @both      = Registration.basically_in.both

    leaders   = @leaders.group_by { |x| x.how_old? }.sort
    @l_age    = Hash[leaders.map { |n| [n[0], n[1].count] }]
    followers = @followers.group_by { |x| x.how_old? }.sort
    @f_age    = Hash[followers.map { |n| [n[0], n[1].count] }]

    @ages_hash = Hash.new

    for i in 0..25
      # age group i..i+4
      age_group = (5*i).to_s + '+'

      counter      = Hash.new
      counter['l'] = 0
      counter['f'] = 0

      for j in 5*i..5*i+4
        counter['l'] = counter['l'] + @l_age[j] unless @l_age[j] == nil
        counter['f'] = counter['f'] - @f_age[j] unless @f_age[j] == nil
      end

      @ages_hash[age_group] = counter
    end

    leaders   = @leaders.group_by { |x| x.distance_to_venue_classified }.sort
    @l_dist   = Hash[leaders.map { |n| [n[0], n[1].count] }]
    followers = @followers.group_by { |x| x.distance_to_venue_classified }.sort
    @f_dist   = Hash[followers.map { |n| [n[0], n[1].count] }]

    @distance_hash = Hash.new

    for i in 0..20
      distance       = i*50
      distance_group = (distance).to_s + ' km'

      counter                        = Hash.new
      counter['l']                   = 0
      counter['f']                   = 0
      counter['l']                   += @l_dist[distance] unless @l_dist[distance] == nil
      counter['f']                   -= @f_dist[distance] unless @f_dist[distance] == nil
      @distance_hash[distance_group] = counter
    end


  end


  def stats2
    redirect_to portal_path, alert: 'Not authorized' unless current_user.has_role? :admin

    @leaders   = Registration.leaders
    @followers = Registration.followers
    @both      = Registration.both


    # stats for guestlist
    all_regs   = Registration.guestlist
    leaders    = @leaders.group_by { |x| x.how_old? }.sort
    @l_age     = Hash[leaders.map { |n| [n[0], n[1].count] }]
    followers  = @followers.group_by { |x| x.how_old? }.sort
    @f_age     = Hash[followers.map { |n| [n[0], n[1].count] }]

    @ages_hash = Hash.new

    interval = 5
    for i in 2..((2014-1900)/interval).to_i

      age_group = '-' + (interval*i+interval-1).to_s

      counter      = Hash.new
      counter['l'] = 0
      counter['f'] = 0

      for j in interval*i..interval*i+interval-1
        counter['l'] = counter['l'] + @l_age[j] unless @l_age[j] == nil
        counter['f'] = counter['f'] + @f_age[j] unless @f_age[j] == nil
      end

      @ages_hash[age_group] = counter
    end

  end


  def test_users
    redirect_to portal_path, alert: 'Not authorized' unless current_user.has_role? :admin

    @tasks_grid = initialize_grid(User,
                                  :include         => [:invitation, :registration],
                                  :order           => 'id',
                                  :order_direction => 'desc')

  end

  def guestlist
    @women = Registration.basically_in.women.sort_by { |e| e[:first_name].upcase.to_s.rjust(3, '0') + e[:last_name] }
    @men   = Registration.basically_in.men.sort_by { |e| e[:first_name].upcase.to_s.rjust(3, '0') + e[:last_name] }
    @not_paid   = Registration.where(:state => 'pays_at_door').sort_by { |e| e[:zodiac_sign_id].to_s.rjust(3, '0') + e[:role] }
    render layout: 'clean'
  end


  def aushang_guestlist
    @guests = Registration.all
  end

  def csv
  end

  def fb_pics
    @registrations = Registration.all
  end

  def view_portal_for_user
    id    = params[:dormitory][:registration_ids][1]
    @user = Registration.find(id).user
  end

  def view_portal_for_user_select
    @registrations = Registration.all
  end

  def signup_stats
    @leaders   = Registration.leaders
    @followers = Registration.followers
    @both      = Registration.both


    leaders   = @leaders.group_by { |x| x.how_old? }.sort
    @l_age    = Hash[leaders.map { |n| [n[0], n[1].count] }]
    followers = @followers.group_by { |x| x.how_old? }.sort
    @f_age    = Hash[followers.map { |n| [n[0], n[1].count] }]

    @ages_hash = Hash.new

    for i in 0..25
      # age group i..i+4
      age_group = (5*i).to_s + '-' + (5*i+4).to_s

      counter      = Hash.new
      counter['l'] = 0
      counter['f'] = 0

      for j in 5*i..5*i+4
        counter['l'] = counter['l'] + @l_age[j] unless @l_age[j] == nil
        counter['f'] = counter['f'] + @f_age[j] unless @f_age[j] == nil
      end

      @ages_hash[age_group] = counter
    end

    leaders   = @leaders.group_by { |x| x.distance_to_venue_classified }.sort
    @l_dist   = Hash[leaders.map { |n| [n[0], n[1].count] }]
    followers = @followers.group_by { |x| x.distance_to_venue_classified }.sort
    @f_dist   = Hash[followers.map { |n| [n[0], n[1].count] }]

    @distance_hash = Hash.new

    for i in 0..20
      distance       = i*50
      distance_group = (distance).to_s + ' km'

      counter                        = Hash.new
      counter['l']                   = 0
      counter['f']                   = 0
      counter['l']                   += @l_dist[distance] unless @l_dist[distance] == nil
      counter['f']                   += @f_dist[distance] unless @f_dist[distance] == nil
      @distance_hash[distance_group] = counter
    end

    @registrations = Registration.all

    @json = Gmaps4rails.build_markers @registrations do |reg, marker|
      if reg.geocoded?
        marker.lat reg.latitude
        marker.lng reg.longitude
        marker.title "#{reg.name}"
        marker.infowindow render_to_string(:partial => 'guests/map_infowindow', :locals => { reg: reg })
        if reg.role == 'leader'
          imageurl = 'https://dl.dropbox.com/u/5350163/gpampa/dot_leader.png'
        elsif reg.role == 'follower'
          imageurl = 'https://dl.dropbox.com/u/5350163/gpampa/dot_follower.png'
        else
          imageurl = 'https://dl.dropbox.com/u/5350163/gpampa/dot_leader.png'
        end
        marker.picture({ picture: imageurl,
                         width:   32,
                         heigth:  39 })
      else
        logger.info "trying to save #{reg.name} to get new geocoordinates"
        reg.save!
      end
    end


  end

  def signup_vs_accepted
    redirect_to portal_path, alert: 'Not authorized' unless current_user.has_role? :admin

    @leaders   = Registration.leaders
    @followers = Registration.followers
    @both      = Registration.both

    @scale      = Hash.new
    @scale['l'] = -35
    @scale['f'] = +35


    leaders   = @leaders.group_by { |x| x.how_old? }.sort
    l_age     = Hash[leaders.map { |n| [n[0], n[1].count] }]
    followers = @followers.group_by { |x| x.how_old? }.sort
    f_age     = Hash[followers.map { |n| [n[0], n[1].count] }]

    @ages_hash_all = Hash.new

    for i in 0..25
      # age group i..i+4
      age_group = (5*i).to_s + '-' + (5*i+4).to_s

      counter      = Hash.new
      counter['l'] = 0
      counter['f'] = 0

      for j in 5*i..5*i+4
        counter['l'] = counter['l'] + l_age[j] unless l_age[j] == nil
        counter['f'] = counter['f'] - f_age[j] unless f_age[j] == nil
      end

      @ages_hash_all[age_group] = counter
    end

    leaders   = @leaders.basically_in.group_by { |x| x.how_old? }.sort
    l_age     = Hash[leaders.map { |n| [n[0], n[1].count] }]
    followers = @followers.basically_in.group_by { |x| x.how_old? }.sort
    f_age     = Hash[followers.map { |n| [n[0], n[1].count] }]

    @ages_hash_accepted = Hash.new

    for i in 0..25
      # age group i..i+4
      age_group = (5*i).to_s + '-' + (5*i+4).to_s

      counter      = Hash.new
      counter['l'] = 0
      counter['f'] = 0

      for j in 5*i..5*i+4
        counter['l'] = counter['l'] + l_age[j] unless l_age[j] == nil
        counter['f'] = counter['f'] - f_age[j] unless f_age[j] == nil
      end

      @ages_hash_accepted[age_group] = counter
    end

    leaders   = @leaders.signed_up.group_by { |x| x.how_old? }.sort
    l_age     = Hash[leaders.map { |n| [n[0], n[1].count] }]
    followers = @followers.signed_up.group_by { |x| x.how_old? }.sort
    f_age     = Hash[followers.map { |n| [n[0], n[1].count] }]

    @ages_hash_waitinglist = Hash.new

    for i in 0..25
      # age group i..i+4
      age_group = (5*i).to_s + '-' + (5*i+4).to_s

      counter      = Hash.new
      counter['l'] = 0
      counter['f'] = 0

      for j in 5*i..5*i+4
        counter['l'] = counter['l'] + l_age[j] unless l_age[j] == nil
        counter['f'] = counter['f'] - f_age[j] unless f_age[j] == nil
      end

      @ages_hash_waitinglist[age_group] = counter
    end

    @ages_hash_all['scale']         = @scale
    @ages_hash_accepted['scale']    = @scale
    @ages_hash_waitinglist['scale'] = @scale


    leaders   = @leaders.group_by { |x| x.distance_to_venue_classified }.sort
    @l_dist   = Hash[leaders.map { |n| [n[0], n[1].count] }]
    followers = @followers.group_by { |x| x.distance_to_venue_classified }.sort
    @f_dist   = Hash[followers.map { |n| [n[0], n[1].count] }]

    @distance_hash_all = Hash.new

    for i in 0..20
      distance       = i*50
      distance_group = (distance).to_s + ' km'

      counter                            = Hash.new
      counter['l']                       = 0
      counter['f']                       = 0
      counter['l']                       += @l_dist[distance] unless @l_dist[distance] == nil
      counter['f']                       -= @f_dist[distance] unless @f_dist[distance] == nil
      @distance_hash_all[distance_group] = counter
    end


    leaders   = @leaders.basically_in.group_by { |x| x.distance_to_venue_classified }.sort
    @l_dist   = Hash[leaders.map { |n| [n[0], n[1].count] }]
    followers = @followers.basically_in.group_by { |x| x.distance_to_venue_classified }.sort
    @f_dist   = Hash[followers.map { |n| [n[0], n[1].count] }]

    @distance_hash_accepted = Hash.new

    for i in 0..20
      distance       = i*50
      distance_group = (distance).to_s + ' km'

      counter                                 = Hash.new
      counter['l']                            = 0
      counter['f']                            = 0
      counter['l']                            += @l_dist[distance] unless @l_dist[distance] == nil
      counter['f']                            -= @f_dist[distance] unless @f_dist[distance] == nil
      @distance_hash_accepted[distance_group] = counter
    end

    leaders   = @leaders.signed_up.group_by { |x| x.distance_to_venue_classified }.sort
    @l_dist   = Hash[leaders.map { |n| [n[0], n[1].count] }]
    followers = @followers.signed_up.group_by { |x| x.distance_to_venue_classified }.sort
    @f_dist   = Hash[followers.map { |n| [n[0], n[1].count] }]

    @distance_hash_waitinglist = Hash.new

    for i in 0..20
      distance       = i*50
      distance_group = (distance).to_s + ' km'

      counter                                    = Hash.new
      counter['l']                               = 0
      counter['f']                               = 0
      counter['l']                               += @l_dist[distance] unless @l_dist[distance] == nil
      counter['f']                               -= @f_dist[distance] unless @f_dist[distance] == nil
      @distance_hash_waitinglist[distance_group] = counter
    end

    @distance_hash_all['scale']         = @scale
    @distance_hash_accepted['scale']    = @scale
    @distance_hash_waitinglist['scale'] = @scale


  end


  def hotel_reservations #lists all made during registration
    @registrations = Registration.basically_in.sort_by { |reg| reg.partner }.sort_by { |reg| reg.room_type }
  end

end


class Backend::BackendPrintController < ApplicationController
  before_filter :authenticate_user!

  layout 'print'

  def hotel_reservations_for_hotel
    @hotel_rooms = HotelRoom.where { responsible_person_id.not_eq nil }.sort_by { |room| room.hotel_room_category_id }.sort_by { |room| room.from }
  end

end
