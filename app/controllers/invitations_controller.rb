# == Schema Information
#
# Table name: inscriptions
#
#  id                       :integer          not null, primary key
#  type                     :string
#  first_name               :string
#  last_name                :string
#  email                    :string
#  gender                   :string
#  role                     :string
#  birthday                 :date
#  street                   :string
#  city                     :string
#  zip                      :string
#  country                  :string
#  facebook                 :string
#  gmaps                    :boolean
#  latitude                 :float
#  longitude                :float
#  distance                 :decimal(, )
#  zodiac_sign_id           :integer
#  dietary_class            :integer          default(0)
#  dietary_incompatibility  :integer          default(0)
#  permission_to_store_data :boolean          default(TRUE)
#  message                  :text
#  telephone                :string
#  partner_email            :string
#  partner_first_name       :string
#  partner_last_name        :string
#  user_id                  :integer
#  suggestion_id            :integer
#  partner_id               :integer
#  created_at               :datetime
#  updated_at               :datetime
#  state                    :string           default("start")
#  processing_list          :string           default("start")
#  email_state              :string           default("start")
#

class InvitationsController < ApplicationController
  before_filter :authenticate_user!, :except => [:accept, :send_invite_to_user]
  layout 'backend'

  def index
    @invitations = Invitation.all
  end


  def new
    form Invitation::Create
  end

  def edit
    form Invitation::Update
  end

  def show
    @invitation = Invitation.find(params[:id])
    require 'createsend'
    # Authenticate with your API key
    @auth = { :api_key => Rails.application.secrets.email_provider_apikey }

    present Invitation::Update
  end

  def update
    run Invitation::Update do |op|
      return redirect_to op.model
    end
    render action: :new
  end


  def destroy
  end

  # GET /invitations/new
  # GET /invitations/new.json
  def new_couple
    @invitation1      = Invitation.new
    @invitation1.role = :follower
    @invitation2      = Invitation.new
    @invitation2.role = :leader
  end

  def create_couple
    # TODO check for missing fields!
    @save    = [false, false]
    @message = params[:invitation][:message]
    @user    = [nil, nil]

    @invitation               = Invitation.new(params[:invitation])
    @invitation.inviter_id    =current_user.id.to_i
    @invitation.couple_status = :couple
    User.invite!(:email => @invitation.email, :name => @invitation.name) do |u|
      u.skip_invitation = true
      u.facebook_id     = @invitation.facebook
      u.save!
      @invitation.user=u
      @user[1]        = u
    end
# => the record will be created, but the invitation email will not be sent

    if @invitation.save
      @save[1] = true
    else
      @save[1] = false
    end
    @invitation               = Invitation.new(params[:invitation2])
    @invitation.inviter_id    =current_user.id.to_i
    @invitation.couple_status = :couple
    @invitation.message       =@message

    User.invite!(:email => @invitation.email, :name => @invitation.name) do |u|
      u.skip_invitation = true
      u.facebook_id     = @invitation.facebook
      u.save!
      @invitation.user=u
      @user[2]        = u
    end
# => the record will be created, but the invitation email will not be sent
    if @invitation.save
      @save[2] = true
    else
      @save[2] = false
    end

    @user[1].set_partner(@user[2], true)

    respond_to do |format|
      if @save[1] and @save[2]
        format.html { redirect_to '/backend/invitations/processing', notice: 'Invitation was successfully created.' }
      else
        format.html { render action: 'new_couple' }
        format.json { render json: @invitation.errors, status: :unprocessable_entity }
      end
    end
  end


  def create
    run Invitation::Create do |op|
      flash[:notice] = "Created invitation for \"#{op.model.name} with email: #{op.model.user.email}\""
      begin
        Invitation::PrepareUser.(invitation: op.model)
        flash[:notice] += "and token #{op.model.user.invitation_token} generated succesfully"
        Rails.logger.info { "#{reg} created #{e.backtrace.join('\n')}" }
      rescue => e
        Rails.logger.error { "#{e.message} #{e.backtrace.join('\n')}" }
      end

      return redirect_to '/backend/invitations/processing'
    end
    render :action => :new
  end


  # GET /invitations/send_invite_to_user/:id
  def send_invite_to_user
    @user = User.find(params[:id])
    Invitation::SendEmail.(user: @user)
    redirect_to '/backend/invitations/processing',
                notice: "Invitation Email was successfully sent out to #{@user.name}/#{@user.email}."
  end

  # GET /invitation/accept/:id/:invitation_token
  def accept
    # This is where users end when they click on the invitation link
    # Here we check if the invitation is valid and still open.
    @user = User.find_by_id(params[:id])

    if @user
      logger.info "User #{@user.id} found."
      # user, and
      @inscription = @user.inscription
      if @inscription
        logger.info "Inscription #{@inscription.id} found: #{@inscription.inspect}"

        if @inscription.type == "Invitation"
          # Invitation is here
          if @user.invitation.declined?
            # but it is declined
            flash[:info] = "Your invitation was previously declined. Please contact us via #{Settings.marathon.email}."
            redirect_to root_path and return

          elsif @user.invitation.expired?
            # but it is expired
            flash[:info] = "Your invitation key has expired. Please contact us via #{Settings.marathon.email}."
            redirect_to '/rsvp/expired' and return

          elsif @user.invitation_token == params[:invitation_token]
            # and it matches the key
            logger.info "User #{@user.id} is responding to invitation"
            logger.debug @user.inspect

            if @user.invitation.declined?
              # but it is declined
              flash[:info] = "Your invitation was previously declined. Please contact us via #{Settings.marathon.email}."
              redirect_to root_path and return

            elsif @user.invitation.expired?
              # but it is expired
              flash[:info] = "Your invitation key has expired. Please contact us via #{Settings.marathon.email}."
              redirect_to '/rsvp/expired' and return

            elsif InvitationWorkflowHelper.invitation_expired?(@user.invitation)
              Invitation::Expire.(invitation: @user.invitation)
              redirect_to '/rsvp/expired' and return
            else
              # it is valid
              sign_in @user
              Rails.logger.info "#{@user} signed in via invitation"
              redirect_to '/rsvp/index' and return
            end

          else
            # Token mismatch
            logger.error "Token mismatch. #{@user.inspect} with #{@inscription.inspect}"
            flash[:error] = "Something is wrong."
            redirect_to root_path and return
          end

        elsif @inscription.type == "Registration"
          # already registered
          redirect_to root_path, alert: "You are already registered. Please sign in."
        else
          # inscription is not in expected possible state
          logger.error "Weird Inscription. #{@inscription.inspect}"
          flash[:error] = "Something is wrong."
          redirect_to root_path and return
        end

      else
        # user has no inscription = bad, should not be
        logger.error "User found, but has no inscription. #{@user.inspect}"
        flash[:error] = "Something is wrong: We can't find your invitation! Please contact us via #{Settings.marathon.email}."
        redirect_to root_path and return
      end
    else
      # no user = bad, should not be
      logger.error "User not found. Invalid URL"
      flash[:error] = "Something is wrong: We can't find your invitation! Please contact us via #{Settings.marathon.email}."
      redirect_to root_path and return
    end
  end


  # POST /invitations/1/reset_invitation_key
  def reset_invitation_key
    result, operation = Invitation::ResetInvitationKey.run(invitation: Invitation.find(params[:id]).reload) do |op|
      redirect_to '/backend/invitations/processing', notice: "Invitation key reset for #{op.model.name}!" and return
    end
    redirect_to '/backend/invitations/processing', error: "Error:  #{result}!"
  end


  # POST /invitations/1/expire
  def expire
    result, operation = Invitation::Decline.run(invitation: Invitation.find(params[:id]).reload) do |op|
      redirect_to '/backend/invitations/processing', notice: "Invitation for #{@invitation.name} is now expired. Link is no longer working." and return
    end
    redirect_to '/backend/invitations/processing', error: "Error:  #{result}!"
  end

  def convert
    @invitation = Invitation.find(params[:id]).reload
    reg         = Inscription::ChangeType.(model: @invitation, target: Registration).model
    reg.save
    redirect_to edit_registration_path(@invitation)
  end

end
