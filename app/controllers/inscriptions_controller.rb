# == Schema Information
#
# Table name: inscriptions
#
#  id                       :integer          not null, primary key
#  type                     :string
#  first_name               :string
#  last_name                :string
#  email                    :string
#  gender                   :string
#  role                     :string
#  birthday                 :date
#  street                   :string
#  city                     :string
#  zip                      :string
#  country                  :string
#  facebook                 :string
#  gmaps                    :boolean
#  latitude                 :float
#  longitude                :float
#  distance                 :decimal(, )
#  zodiac_sign_id           :integer
#  dietary_class            :integer          default(0)
#  dietary_incompatibility  :integer          default(0)
#  permission_to_store_data :boolean          default(TRUE)
#  message                  :text
#  telephone                :string
#  partner_email            :string
#  partner_first_name       :string
#  partner_last_name        :string
#  user_id                  :integer
#  suggestion_id            :integer
#  partner_id               :integer
#  created_at               :datetime
#  updated_at               :datetime
#  state                    :string           default("start")
#  processing_list          :string           default("start")
#  email_state              :string           default("start")
#

class InscriptionsController < ApplicationController
  respond_to :html

  def index

  end

  def create
    run Inscription::Create do |op|
      flash[:notice] = "Created registration for \"#{op.model.name} with email: #{op.model.user.email}\""

      sign_in User.find_by_email(op.model.user.email), :bypass => true

      begin
        reg = Inscription::ChangeType.({ model: op.model, target: Registration }).model
        reg = Registration::SignUp.({ model: reg }).model
        Rails.logger.info { "#{reg} created #{e.backtrace.join('\n')}" }
      rescue => e
        Rails.logger.error { "#{e.message} #{e.backtrace.join('\n')}" }
      end

      return redirect_to portal_index_path
    end
    render :action => :new
  end

  def new
    if current_user
      render :action => :edit
    else
      form Inscription::Create
    end
    render_form
  end

  def edit
    @reg = current_user.inscription

    form Inscription::Update
    render action: :new
  end


  def show
    @i = Inscription.find(params[:id])

    present Inscription::Update
  end

  def update
    # this is where an invitation is automatically converted to registration, and
    # the state is moved to accepted, if the user has been invited previously!
    # TODO check this when going to a open-registration system!
    run  Inscription::Update do |op|
      flash[:notice] = "Updated registration for \"#{op.model.name} with email: #{op.model.user.email}\""
      if op.model.user.invitation_accepted_at? and op.model.user.invitation_accepted_at.past? & op.model
        reg = Inscription::ChangeType.({ model: op.model, target: Registration }).model
        reg = Registration::SignUp.({ model: op.model }).model
        reg = Registration::Accept.({ model: reg }).model
        reg = Registration::SendPleasePay.({ user: op.model.user }).model
        # NOTE Auto-sends please pay email!
      end
      sign_in User.find_by_email(op.model.user.email), :bypass => true
      return redirect_to portal_index_path
    end

    render action: :new
  end


  def destroy
  end

  private
  def render_form
    render text: concept("inscription/cell/form", @operation),
      layout: true
  end
end
