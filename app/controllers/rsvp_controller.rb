class RsvpController < ApplicationController
  before_filter :authenticate_user!, except: [:decline, :expired, :about]

  def index
    @deactivate_footer = :true

    @user       = current_user
    @invitation = @user.invitation

    if @user.admin
      redirect_to backend_path
    elsif @user.nil?
      # Invitation was not converted to Registration yet
      redirect_to root_path
    end
  end

  def decline
    @user = current_user
    if @user.admin
      redirect_to backend_path
    elsif @user.nil?
      # Invitation was not converted to Registration yet
      redirect_to root_path
    end
    if @user
      Invitation::Decline.(user: @user)
      sign_out @user
    end
  end

  def accept
    @deactivate_footer = :true
    @user              = User.find(current_user.id)
    Invitation::Accept.(user: @user)
    @invitation = @user.invitation

    if @user.nil? or @invitation.nil?
      # Invitation was not converted to Registration yet
      redirect_to root_path
    elsif @user.admin
      redirect_to backend_path
    end
  end

  def expired
  end

  def about
  end

end
