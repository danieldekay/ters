class PortalController < ApplicationController
  before_filter :logged_in
  layout 'portal'

  def index
    @user = current_user
    @reg  = @user.registration
    Rails.logger.info "#{@user} in portal"

    if @user.admin
      redirect_to backend_path
    elsif @reg and (@reg.state == "start")
      redirect_to edit_registration_path @reg
    elsif @reg
      @suggestions = @reg.suggestions

      # if we have signed up with a partner, make that a suggestion, if it's not yet a suggestion
      if @reg.partner
        # if the partner has signed up yet, then there is no need to convert that partner to another suggestion
      elsif (@suggestions.size == 0) and !@reg.partner_email.blank? and !@reg.partner_first_name.blank? and !@reg.partner_last_name.blank?
        SuggestedParticipant::SuggestPartner.(model: @reg)
        @suggestions = @reg.suggestions
      end

    else
      # Invitation was not converted to Registration yet
      redirect_to '/rsvp/index'
    end
  end

  def map

  end

  def guestlist
    @registrations = Registration.all

    @user = current_user

    @markers = MapWorker.generate_markers

  end

  def suggest
    @user                  = current_user
    @reg                   = @user.inscription
    @suggestions           = @reg.suggestions

    # suggestion rules
    @remaining_suggestions = Settings.features.suggestions.number - @suggestions.count

  end

  def update_dietary
    @user                                                = current_user
    @reg                                                 = @user.registration
    params[:registration][:dietary_incompatibility_list] = params[:registration][:dietary_incompatibility_list].join(',')
    if @reg.update_attributes(reg_params)
      redirect_to portal_path :notice => "Successfully updated dietary options."
    else
      redirect_to portal_path :error => "Did not update dietary options."
    end
  end


  private

  def reg_params
    params.require(:registration).permit(:dietary_incompatibility_list,
                                         :dietary_class_list)
  end

end
