class BackendStatisticsController < ApplicationController
  before_filter :logged_in_as_admin

  layout 'backend'

  def regional
    ## To make a list of countries, descending, split by role
    @sorted_countries           = Registration.group(:country).count.sort_by { |k, v| v }.reverse
    @registrations_by_countries = Registration.group(:country, :role).count
  end

  def age
    @leaders   = Registration.basically_in.leaders
    @followers = Registration.basically_in.followers
    @both      = Registration.basically_in.both

    leaders   = @leaders.group_by { |x| x.birthday.nil? ? 99 : x.how_old? }.sort
    @l_age    = Hash[leaders.map { |n| [n[0], n[1].count] }]
    followers = @followers.group_by { |x| x.how_old? }.sort
    @f_age    = Hash[followers.map { |n| [n[0], n[1].count] }]

    @ages_hash = Hash.new

    for i in 0..25
      # age group i..i+4
      age_group = (5*i).to_s + '+'

      counter      = Hash.new
      counter['l'] = 0
      counter['f'] = 0

      for j in 5*i..5*i+4
        counter['l'] = counter['l'] + @l_age[j] unless @l_age[j] == nil
        counter['f'] = counter['f'] - @f_age[j] unless @f_age[j] == nil
      end

      @ages_hash[age_group] = counter
    end

    script_hash = ""
    @ages_hash.each do |key, val|
      script_hash += "{age: '#{key}', l: '#{val['l']}', f: '#{val['f']}'},\n"
    end
    script_hash += "{age: 'last', l: '30', f: '-30'}"


    @ages_script =
        "Morris.Bar({
        element: 'age-histogram',
        data: [#{script_hash}],
        xkey: 'age',
        ykeys: ['l', 'f'],
        labels: ['Men', 'Women'],
        barColors: ['blue', 'red'],
        stacked: 'true',
        hideHover: 'auto',
        });"


  end
end
