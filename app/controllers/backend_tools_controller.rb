class BackendToolsController < ApplicationController
  before_filter :logged_in_as_admin


  def guest_emails
    @regs = Registration.guestlist.order('updated_at DESC')
  end
end
