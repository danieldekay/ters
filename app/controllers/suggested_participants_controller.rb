# == Schema Information
#
# Table name: suggested_participants
#
#  id             :integer          not null, primary key
#  first_name     :string
#  last_name      :string
#  email          :string
#  gender         :string
#  role           :string
#  city           :string
#  country        :string
#  facebook       :string
#  message        :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  inscription_id :integer
#

class SuggestedParticipantsController < ApplicationController
  respond_to :html

  def index
    redirect_to portal_suggest_path
  end

  def create
    @me = current_user.inscription
    run SuggestedParticipant::Create do |op|
      flash[:notice] = "Suggested \"#{op.model.name} with email: #{op.model.user.email rescue 'unkown'}\""

      begin
        sugg = op.model
        SuggestedParticipant::SuggestedBy.(model: @me, suggests: sugg)
        Rails.logger.info { "#{current_user.inscription.name} suggested #{sugg}" }
      rescue => e
        Rails.logger.error { "#{e.message} #{e.backtrace.join('\n')}" }
      end

      return redirect_to portal_suggest_path
    end
    render :action => :new
  end

  def new
    form SuggestedParticipant::Create
  end

  def edit
    form SuggestedParticipant::Update
    render action: :new
  end

  def show
    present SuggestedParticipant::Update
  end

  def update
    run SuggestedParticipant::Update do |op|
      return redirect_to portal_suggest_path
    end
    render action: :new
  end


  def destroy
  end

  private
  def render_form
    render text:   concept("suggested_participant/cell/form", @operation),
           layout: true
  end

end
