class CreateDemoDatabase

  def call(number=10)
    number.times do
      create(leader)
      create(leader)
      create(follower)
      create(follower)
      create(follower)
      create_couple(leader, follower)
      create_couple(leader, follower)
    end
  end

  def move_along
    Registration.signed_up.sample((0.8*Registration.signed_up.count).to_i).each do |reg|
      Registration::Accept.(model: reg)
    end
    Registration.accepted.sample((0.4*Registration.accepted.count).to_i).each do |reg|
      Registration::Pay.(model: reg, amount: Settings.payment.price)
    end
  end

  private

  def create (params)
    model = Inscription::Create.(params).model
    model = Inscription::ChangeType.({ model: model, target: Registration }).model
    model = Registration::SignUp.({ model: model }).model

    puts "#{model.role}: #{model.name}/#{model.email} created"
  end

  def create_couple (l_params, f_params)
    l_params[:inscription][:partner_email] = f_params[:inscription][:user][:_email]
    f_params[:inscription][:partner_email] = l_params[:inscription][:user][:_email]

    lead = Inscription::Create.(l_params).model
    foll = Inscription::Create.(f_params).model

    puts "Couple: #{foll.role}: #{foll.name}/#{foll.email} & #{lead.role}: #{lead.name}/#{lead.email} created"
  end

  def couple
    lead = Inscription::Create.(leader).model
    puts "Leader: #{lead.name}/#{lead.email} created"

    foll = Inscription::Create.(follower).model
    puts "Follower: #{foll.name}/#{foll.email} created"

  end

  def leader
    l_params               = Hash.new
    f_name                 = FFaker::Name.first_name
    l_name                 = FFaker::Name.last_name
    e_mail                 = "#{f_name}.#{l_name}@#{Faker::Internet.domain_name}".downcase
    l_params[:inscription] = {
        first_name: f_name,
        last_name:  l_name,
        gender:     :man,
        role:       :leader,
        birthday:   Faker::Date.birthday(min_age=18, max_age=60),
        street:     Faker::Address.street_address,
        city:       Faker::Address.city,
        zip:        Faker::Address.zip_code,
        country:    Faker::Address.country,
        user:       {
            email:                 e_mail,
            password:              'password1234',
            password_confirmation: 'password1234'
        }
    }
    return l_params
  end


  def follower
    f_params               = Hash.new
    f_name                 = FFaker::Name.first_name
    l_name                 = FFaker::Name.last_name
    e_mail                 = "#{f_name}.#{l_name}@#{Faker::Internet.domain_name}".downcase
    f_params[:inscription] = {
        first_name: f_name,
        last_name:  l_name,
        gender:     :woman,
        role:       :follower,
        birthday:   Faker::Date.birthday(min_age=18, max_age=60),
        street:     Faker::Address.street_address,
        city:       Faker::Address.city,
        zip:        Faker::Address.zip_code,
        country:    Faker::Address.country,
        user:       {
            email:                 e_mail,
            password:              'password1234',
            password_confirmation: 'password1234'
        }
    }
    return f_params
  end

end
