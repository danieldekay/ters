class CreateAdminService
  def call
    %w(daniel@tangomarathons.com juliana@tangomarathons.com).each do |email|
      user = User.find_or_create_by!(email: email) do |user|
        user.password              = email
        user.password_confirmation = email
        user.admin                 = true
      end
      puts "#{user.email} created as admin user"
    end
  end
end
