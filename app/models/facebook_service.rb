class FacebookService

  def self.convert_facebook_id(text)
    # take different kinds of input (user generated!) and convert to facebook ID if possible
    # 3 Cases
    # https://www.facebook.com/tugbatanoren
    # sandramilena.sm
    # Hans gruber

    if text
      url = Addressable::URI.parse(text)

      if url and url.scheme # we have a URL
        facebook_id = url.path.tr('/', '')
      elsif text.include? " " # we have a facebook name, but not ID or username
        facebook_id = nil
      elsif text.include? "@" # we have a facebook email
        facebook_id = text.split("@").first
      elsif text.include? "www" # we have an incoplete URL
        facebook_id = Addressable::URI.parse("http://" + text).path.tr('/', '')
      elsif text.blank?
        facebook_id = nil
      else
        facebook_id = text.downcase
      end
    end
  end

  def self.profile_picture_tag(id)
    if id.blank?
      return "<i class='fa fa-fw fa-user fa-2x text-muted'></i>".html_safe
    else
      url = ('http://graph.facebook.com/' + id + '/picture').html_safe
      return "<img class='profile_picture_small' src='#{url} />'".html_safe
    end
  end


end
