class MapWorker
  def self.generate_markers
    regs = Registration.all

    output = "L.marker([49.407498, 8.1895652], {
                  icon: L.mapbox.marker.icon({
                      'marker-size': 'small',
                      'marker-symbol': 'museum',
                      'marker-color': '#fa0'
                  })
              }).addTo(map);
              var leader = L.mapbox.marker.icon({
                      'marker-size': 'small',
                      'marker-symbol': 'marker',
                      'marker-color': '#6b85ff'
              });
              var follower = L.mapbox.marker.icon({
                      'marker-size': 'small',
                      'marker-symbol': 'marker',
                      'marker-color': '#fe679e'
              });
              var both = L.mapbox.marker.icon({
                      'marker-size': 'small',
                      'marker-symbol': 'marker',
                      'marker-color': '#8bfe8d'
              });
              "
    regs.each do |reg|
      if reg.geocoded?
        output += "L.marker([#{(reg.latitude.round(5) + rand(-5..5)*0.005 + rand(-5..5)*0.0009).round(7)},
                             #{(reg.longitude.round(5) + rand(-5..5)*0.05 + rand(-5..5)*0.0009).round(7)}],
                             {icon: #{reg.role.to_s}}).addTo(map);"
      end
    end
    output
  end

  def self.print_no_geocode
    # '<small><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span></small>'.html_safe
    '<small>?</small>'.html_safe
  end
end
