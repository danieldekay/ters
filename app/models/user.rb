# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string
#  admin                  :boolean          default(FALSE)
#  invitation_token       :string
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_id          :integer
#  invited_by_type        :string
#  invitations_count      :integer          default(0)
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :inscription

  validates_uniqueness_of :email

  def registration
    if inscription.class == Registration
      inscription
    else
      nil
    end
  end

  def invitation
    if inscription.class == Invitation
      inscription
    else
      nil
    end
  end

  def send_reset_password_instructions
    super if invitation_token.nil?
  end

  def is_guest?
    registration ? registration.is_guest? : false
  end

  def is_admin?
    admin
  end

  def reset_invitation_token
    self.invitation_token = nil
    self.invitation_accepted_at= nil
    self.invitation_sent_at= nil
    self.save
  end

end
