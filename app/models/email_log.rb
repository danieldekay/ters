# == Schema Information
#
# Table name: email_logs
#
#  id             :integer          not null, primary key
#  message_ID     :string
#  status         :string
#  sent_at        :datetime
#  smart_email_ID :string
#  email          :string
#  message        :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class EmailLog < ActiveRecord::Base
  belongs_to :inscription

  def self.new_with_response(response, message, email, smart_email_id)
    # response = [#<Hashie::Mash MessageID="4cc13bb0-12d3-11e6-81a6-4af6ba00cfdc" Recipient="\"Da Ka\" <daniel@dekay.org>" Status="Accepted">]
    new = EmailLog.new
    new.message = message
    new.message_ID = response.first['MessageID']
    new.email = email
    new.sent_at = DateTime.now
    new.status = response.first['Status']
    new.smart_email_ID=smart_email_id

    new.inscription = User.find_by_email(email).inscription
    new.save!

    new
  end

end
