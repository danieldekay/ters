class RegistrationDecorator
  def self.format_registration_state(state)
    case state.to_sym
      when :start
        "<span class='label label-danger'>Error</span>".html_safe
      else
        "<span class='label #{state}'>#{state}</span>".html_safe
    end
  end

end
