# == Schema Information
#
# Table name: previous_participants
#
#  id             :integer          not null, primary key
#  first_name     :string
#  last_name      :string
#  email          :string
#  city           :string
#  country        :string
#  birthday       :date
#  role           :string
#  known_from     :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  zodiac_sign_id :integer
#  facebook       :string
#

class PreviousParticipant < ActiveRecord::Base
  acts_as_taggable_on :events
  zodiac_reader :birthday

  scope :followers, -> { where(role: :follower) }
  scope :leaders, -> { where(role: :leader) }
  scope :both, -> { where(role: :both) }

  def print_events
    self.event_list.to_s
  end

  def to_s
    "#{first_name} #{last_name}, #{print_events}"
  end

  def name
    first_name + " " + last_name rescue "error"
  end

  def gender
    case role.to_s.downcase
      when 'leader'
        'Man'
      when 'follower'
        'Woman'
      when 'both'
        'Woman'
    end
  end
end

