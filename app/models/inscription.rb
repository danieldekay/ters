# == Schema Information
#
# Table name: inscriptions
#
#  id                       :integer          not null, primary key
#  type                     :string
#  first_name               :string
#  last_name                :string
#  email                    :string
#  gender                   :string
#  role                     :string
#  birthday                 :date
#  street                   :string
#  city                     :string
#  zip                      :string
#  country                  :string
#  facebook                 :string
#  gmaps                    :boolean
#  latitude                 :float
#  longitude                :float
#  distance                 :decimal(, )
#  zodiac_sign_id           :integer
#  dietary_class            :integer          default(0)
#  dietary_incompatibility  :integer          default(0)
#  permission_to_store_data :boolean          default(TRUE)
#  message                  :text
#  telephone                :string
#  partner_email            :string
#  partner_first_name       :string
#  partner_last_name        :string
#  user_id                  :integer
#  suggestion_id            :integer
#  partner_id               :integer
#  created_at               :datetime
#  updated_at               :datetime
#  state                    :string           default("start")
#  processing_list          :string           default("start")
#  email_state              :string           default("start")
#

class Inscription < ActiveRecord::Base

  include Geotagging

  belongs_to :user, dependent: :destroy

  auto_strip_attributes :first_name, :last_name, :email

  has_many :partnerships, dependent: :destroy
  has_many :matched_inscriptions, through: :partnerships do
    def with_match_data
      select('inscriptions.*, partnerships.created_at AS partnership_created_at')
    end

    def count(column_name = :all)
      super
    end
  end

  has_many :suggested_participants
  has_many :email_logs
  acts_as_taggable_on :dietary_incompatibility, :dietary_class

  # We will need a way to know which animals
  # will subclass the Animal model
  def self.subtypes
    %w(Invitation Registration)
  end


  scope :registrations, -> { where(type: 'Registration') }
  scope :invitations, -> { where(type: 'Invitations') }
  scope :inscriptions, -> { where(type: nil) }

  scope :followers, -> { where(role: :follower) }
  scope :leaders, -> { where(role: :leader) }
  scope :both, -> { where(role: :both) }

  scope :men, -> { where(gender: 'man') }
  scope :women, -> { where(gender: 'woman') }
  scope :no_gender, -> { where(gender: '') }

  scope :error_without_user, -> { where(:user_id => nil) }

  def partnership_created_at
    Time.zone.parse(self[:partnership_created_at]) if self[:partnership_created_at]
  end

  ## self-registration
  # incomplete >sign_up> signed_up >accept> accepted >payment_arrived> paid
  ## pre-registration
  # invited >> accepted >payment_arrived> paid


  state_machine :processing_list, :initial => :start, :namespace => 'list' do
    state :start
    state :waitinglist
    state :blacklist
    state :wishlist
    state :nolist
    state :accepted
    state :guestlist
    state :cancelledlist

    event :init_state do
      transition :start => :nolist
    end

    event :send_to_waiting do
      transition [:nolist, :accepted, :wishlist] => :waitinglist
    end
    event :send_to_black do
      transition [:nolist, :accepted, :cancelledlist] => :blacklist
    end
    event :send_to_wish do
      transition [:nolist, :waitinglist] => :wishlist
    end
    event :mrs_reset do
      transition [:blacklist, :accepted, :wishlist, :waitinglist, :guestlist, :cancelledlist] => :nolist
    end
    event :accept do
      transition [:nolist, :wishlist, :waitinglist] => :accepted
    end
    event :guest do
      transition [:accepted] => :guestlist
    end
    event :send_to_cancelled do
      transition [:accepted, :guestlist] => :cancelledlist
    end

  end

  state_machine :email_state, :initial => :start, :namespace => 'email' do

    state :start
    state :registration_received
    state :waitinglist
    state :please_pay
    state :you_are_in
    state :you_are_in_2
    state :you_are_out
    state :ws_info
    state :ws_pay
    state :final_info

    event :init_state do
      transition :start => nil
    end
    event :send_received do
      transition nil => :registration_received
    end
    event :send_registration_received do
      transition nil => :registration_received
    end
    event :send_waitinglist do
      transition all => :waitinglist
    end
    event :send_please_pay do
      transition all => :please_pay
    end
    event :send_you_are_in do
      transition all - [:you_are_out] => :you_are_in
    end
    event :send_you_are_out do
      transition all => :you_are_out
    end
    event :send_workshop_reservation_info do
      transition all => :ws_info
    end
    event :send_workshop_please_pay do
      transition all => :ws_pay
    end
    event :send_final_info do
      transition all => :final_info
    end
  end

  def name
    name = self.first_name.capitalize + ' ' + self.last_name.capitalize
    name.html_safe
  end

  def email
    if user
      user.email
    else
      email
    end
  end

  def email=(newemail)
    if user
      user.email=email
    else
      email=newemail
    end
  end

  def how_old?
    if birthday
      now = Time.now.utc.to_date
      now.year - self.birthday.year - (self.birthday.to_date.change(:year => now.year) > now ? 1 : 0)
    else
      99
    end
  end


  def status_print_as_text
    #TODO refactor
  end

  def status_print #TODO

    case self.state_name
      when :accepted
        icon = ENV['STATUS_SYMBOL_REGISTRATION_SUBMITTED'].to_s + ' ' + decorate_status_label(self.state_name.to_s, self.state_name.to_s)
      when :signed_up
        icon = ENV['STATUS_SYMBOL_REGISTRATION_SIGNED_UP'].to_s + ' ' + decorate_status_label(self.state_name.to_s, self.state_name.to_s)
      when :paid
        icon = ENV['STATUS_SYMBOL_REGISTRATION_PAID'].to_s + ' ' + decorate_status_label(self.state_name.to_s, self.state_name.to_s)
      when :confirmed
        icon = ENV['STATUS_SYMBOL_REGISTRATION_PAID'].to_s + ' ' + decorate_status_label(self.state_name.to_s, self.state_name.to_s)
      when nil
        icon = 'ERROR nil state'.to_s
      when :free_ticket
        icon = "<i class='fa fa-heart-o'></i> " + decorate_status_label(self.state_name.to_s, self.state_name.to_s)
      else
        icon = self.state_name.to_s
    end

    icon.html_safe
  end

  ## STATE_MACHINE #TODO
  def transfer_status_to_state
    case self.status[:registration_status]
      when :accepted
        self.submit
      when :paid
        self.submit
        self.payment_arrived
    end
  end


  ## WORKFLOW EMAILS
  def send_email_receipt_of_sign_up
    InvitationMailer.send_email_receipt_self_suggestion(self).deliver

    if self.telephone.present?
      SMS_Sender.instance.send 'Thank you for signing up for G.Pampa Tango Marathon!', self.telephone
    end

  end

  def send_email(email_key)
    eval("InvitationMailer.send_email_#{email_key.to_s}(self).deliver")
    eval("self.send_#{email_key.to_s}_email")

    if self.telephone.present?
      SMS_Sender.instance.send email_key, self.telephone
    else
      Rails.logger.info "No SMS sent because no valid phone-number was present. Message #{email_key}"
    end
  end


  def to_html #TODO
    temp = self.name + ", #{street}, #{zip} #{city}, #{country}<br/>email: #{email}<br/>".html_safe
    temp += "phone: #{telephone}</br>".html_safe if telephone.present?
    temp += "partner: #{partner} with email: #{partner_email}" if (partner.present? and partner_email.present?)
    temp.html_safe
  end

  def partner # returns the partner inscription, if present
    # do we already have a matched partner?

    p = Partnership::GetPartner.(model: self).partner
    q = User.find_by_email(partner_email)

    if p # if we have a partner set, return it
      return p
    elsif q and (q.inscription.partner_email == self.email)
      # if we signed up with a partner, but that partner has not yet signed up
      res, operation = Partnership::DefinePartner.run(model: self, partner: q.inscription) do |op|
        # this is not run, because validation not successful.
        return Partnership::GetPartner.(model: self).partner
      end
      Rails.logger.error "That's wrong: #{operation.errors}"
    end

    return nil
  end

  def suggestions
    # returns the suggestions, if present

    p = SuggestedParticipant::Get.(model: self, kind: :suggestion).all

    if p # if we have a partner set, return it
      return p
    end
    return nil
  end

  def suggested_by
    # returns the suggestions, if present

    p = SuggestedParticipant::Get.(model: self, kind: :suggested_by).all

    if p # if we have a partner set, return it
      return p
    end
    return nil
  end

  def partner_name
    partner_first_name + " " + partner_last_name
  end

end
