class ApplicationWorkflowDecorator

  def self.print_user_status_tag_by_workflow_state(email)
    @user = User.find_by_email(email)
    @sugg = SuggestedParticipant.find_by_email(email)
    output = ""
    if @user
      # user, and
      @inscription = @user.inscription
      if @inscription
        if @inscription.type == "Invitation"
          # Invitation is here
          output = "<span class='btn btn-xs'>Inv</span>  #{InvitationDecorator.format_invitation_state @inscription.state} / #{@inscription.id}"
        elsif @inscription.type == "Registration"
          # already registered
          output = "<span class='btn btn-xs'>Reg</span> #{RegistrationDecorator.format_registration_state @inscription.state } / #{@inscription.id}"
        else
          # inscription is not in expected possible state
          output = "error, wrong inscription"
        end
      else
        # user has no inscription = bad, should not be
        output = "error, no inscription"
      end
    elsif @sugg
      output = "suggested"
    else
      # no user = bad, should not be
      output = "not invited yet: #{email}"
    end

    output.html_safe
  end


  def self.ratio_color(percentage)
    if percentage > 110
      "danger"
    elsif percentage < 90
      "danger"
    else
      "success"
    end
  end
end
