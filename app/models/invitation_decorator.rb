class InvitationDecorator
  def self.format_invitation_state(state)
    case state.to_sym
      when :start
        "<span class='true'>Not sent</span>".html_safe
      when :sent
        "<span class='true'>Open</span>".html_safe
      when :declined
        "<span class='false'>Declined</span>".html_safe
      when :expired
        "<span class='false'>Expired</span>".html_safe
    end
  end

  def self.print_status_tag_with_date(invitation)
    temp = ""
    temp += ApplicationDecorator.format_boolean !InvitationWorkflowHelper.invitation_expired?(invitation)
    temp += " "
    temp += InvitationDecorator.format_invitation_state invitation.state
    if !InvitationWorkflowHelper.invitation_expired?(invitation)
      temp += invitation.user.invitation_sent_at ? "<small> #{InvitationWorkflowHelper.invitation_expiry_date(invitation)}</small>".html_safe : "" rescue "error"
    end
    temp
  end

end
