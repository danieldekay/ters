module RegistrationStateable
  extend ActiveSupport::Concern

  included do
    state_machine :state, initial: :start do

      state :start
      state :accepted # triggered when registration is actually moved to guestlist and waiting for payment
      state :signed_up # self-registration state after registration is sent to us
      state :paid # = IS IN
      state :pays_at_door # TODO needs to be noted on CHECKIN-List
      state :free_ticket # TODO behaves like :paid, but could be in some statistics
      state :cancelled #
      state :refund # must be refunded

      # after_transition :on => :sign_up do |registration, transition|
      #   registration.send_email(:registration_received)
      #   registration.send_received_email
      # end

      after_transition :accepted => [:paid, :free_ticket, :pays_at_door], :do => :guest_list
      after_transition :signed_up => [:free_ticket], :do => :guest_list
      after_transition :to => :cancelled, :do => :send_to_cancelled_list
      after_transition :paid => :refund, :do => :send_to_cancelled_list
      after_transition :start => :signed_up, :do => :init_state_list
      event :sign_up do
        transition :start => :signed_up
      end
      event :submit do
        transition :start => :accepted
      end
      event :accept do
        transition :signed_up => :accepted
      end
      event :remind do
        transition :accepted => :accepted
      end
      event :has_not_paid do
        transition :accepted => :signed_up
      end
      event :payment_arrived do
        transition [:accepted, :pays_at_door, :free_ticket] => :paid
      end
      event :free_ticket do
        transition :signed_up => :free_ticket, :accepted => :free_ticket
      end
      event :pays_at_door do
        transition [:accepted, :paid, :free_ticket] => :pays_at_door
      end
      event :cancel_registration do
        transition :paid => :refund, [:pays_at_door, :accepted, :free_ticket] => :cancelled
      end
      event :do_refund do
        transition :refund => :cancelled
      end
      event :reset_state do
        transition all => :start
      end
    end

  end


end
