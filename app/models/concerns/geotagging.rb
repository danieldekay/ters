module Geotagging
  extend ActiveSupport::Concern

  included do
    geocoded_by :address_for_geocoding
    after_validation :geocode
  end

  def address_for_geocoding
    self.street.to_s + ' ' + self.city.to_s + ' ' + self.country.to_s
  end

  def address
    temp = "#{self.street.try(:titleize)}, #{self.zip} #{self.city.try(:titleize)}, #{self.country}"
    temp.html_safe
  end

  def distance_to_venue
    if self.distance
    else
      # Registration.record_timestamps=false
      # Registration.find(self.id).update_attributes(:distance => self.distance_to([49.4370744, 8.1710311]).to_f)
      # Registration.record_timestamps=true
      # self.reload
    end
    self.distance.to_f
  end

  def distance_to_venue_classified
    classify = 50
    temp     = self.distance_to_venue
    temp     = temp / classify
    temp.round(0)*classify
  end

  def geocoded?
    if self.latitude.blank? or self.longitude.blank?
      self.geocode
    else
      true
    end
  end

  def reset_geocode
    write_attribute(:latitude, nil)
    write_attribute(:longitude, nil)
    self.save
  end

end
