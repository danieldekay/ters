module Payable
  extend ActiveSupport::Concern

  def to_pay #TODO
    topay = Settings.payment.price
    topay
  end

  def has_paid #TODO
    # if amount_paid
    #   amount_paid
    # else
    #   0
    # end
  end

  def outstanding_balance #TODO
    # balance = to_pay - has_paid
    # balance
    0
  end

  def has_paid_in_euro #TODO
    "#{self.has_paid} &euro;".html_safe
  end

  def payment_difference #TODO
    temp = self.to_pay - self.has_paid
    if temp < 6 and temp > -6
      temp = 0
    end
    "#{temp} &euro;".html_safe
  end

  def is_paid? #TODO
    self.payment_status == 'paid' ? true : false
  end

  def set_pay_date #TODO
    write_attribute(:payment_status, 'to_pay')
    write_attribute(:to_pay_date, DateTime.now + eval(Settings.payment.deadline))
    self.save!
  end

  #def submit (resubmit = false) #TODO
  #  if self.status.nil? || resubmit == true
  #    write_attribute(:status, {:registration_status => :accepted, :status_change_date => DateTime.now})
  #    write_attribute(:payment_status, 'to_pay')
  #    write_attribute(:to_pay_date, DateTime.now + eval(Settings.payment.deadline))
  #	write_attribute(:amount_paid, 0)
  #  end
  #  self.save!
  #end

  def pay (amount) #TODO
    self.amount_paid=amount.to_i
    if self.outstanding_balance > 0
      write_attribute(:list, { registration_status: :partially_paid, status_change_date: DateTime.now })
      write_attribute(:payment_status, 'partially_paid')
    elsif self.outstanding_balance <= 0
      write_attribute(:list, { registration_status: :paid, status_change_date: DateTime.now })
      write_attribute(:payment_status, 'paid')
    end
    self.save!
  end

  def pay_fully #TODO
    self.amount_paid = Settings.payment.price
    write_attribute(:list, { registration_status: :paid, status_change_date: DateTime.now })
    write_attribute(:payment_status, 'paid')
    self.save!
  end

  def pay_date #TODO
    self.to_pay_date ? self.to_pay_date : DateTime.now
  end

  def extend_pay_date #TODO
    write_attribute(:to_pay_date, DateTime.now + 3.days)
    self.save!
    self.to_pay_date
  end
end
