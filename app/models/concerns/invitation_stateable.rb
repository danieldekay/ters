module InvitationStateable
  extend ActiveSupport::Concern

  included do
    scope :declined, -> { where(state: 'declined') }
    scope :open, -> { where(state: 'sent') }
    scope :not_sent, -> { where(state: 'start') } # {joins(:user).where(:users => {:invitation_sent_at => nil})}
    scope :sent, -> { where(state: 'sent') }
    scope :expired, -> { where(state: 'expired') }
    scope :closed, -> { where(state: ['expired', 'declined']) }

    state_machine :state, initial: :start do
      state :start
      state :sent
      state :accepted
      state :declined
      state :expired

      event :decline do
        transition [:start, :sent, :expired] => :declined
      end

      event :reset_key do
        transition [:start, :sent, :accepted, :declined, :expired] => :start
      end

      event :email_is_sent do
        transition [:start, :sent] => :sent
      end

      event :expire do
        transition [:start, :sent] => :expired
      end

      after_transition [:start, :sent] => :declined, :do => :delete_invitation_token
      after_transition [:start, :sent] => :expired, :do => :delete_invitation_token

    end

    def delete_invitation_token
      user                       = self.user
      user.invitation_sent_at    = nil
      user.invitation_token      = nil
      user.invitation_created_at = nil
      user.save!
    end

  end


end
