class ApplicationDecorator

  def self.format_boolean(b)
    result = b ? "<span class='glyphicon glyphicon-ok true' aria-hidden='true'></span>" : "<span class='glyphicon glyphicon-remove false' aria-hidden='true'></span>"
    result = result.html_safe
    return result
  end

  def self.format_role(role)
    if role
      case role.to_sym
        when :leader
          "<span class='btn btn-xs leader'>Leader</span>".html_safe
        when :follower
          "<span class='btn btn-xs follower'>Follower</span>".html_safe
        when :both
          "<span class='btn btn-xs both'>Both</span>".html_safe
      end
    end
  end

  def self.format_date(date)
    "<small>#{date}</small>".html_safe
  end

  def self.dietary(inscription)
    if inscription.dietary_class_list != []
      if inscription.dietary_incompatibility_list != []
        "#{inscription.dietary_class_list}, #{inscription.dietary_incompatibility_list}".html_safe
      else
        "#{inscription.dietary_class_list}".html_safe
      end
    else
      "omnivore (?)"
    end
  end

  def self.dietary_class(inscription)
    if inscription.dietary_class_list != []
        "#{inscription.dietary_class_list}".html_safe
    else
      "omnivore (?)"
    end
  end

  def self.dietary_incompatibility(inscription)
    "#{inscription.dietary_incompatibility_list}".html_safe
  end



  def self.flip_role(role)
    role == 'leader' ? 'follower' : 'leader'
  end

  def self.flip_gender(gender)
    gender == 'man' ? 'woman' : 'man'
  end


end
