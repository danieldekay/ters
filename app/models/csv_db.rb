require 'csv'
class CsvDb
  class << self
    def convert_save(model_name, csv_data, delete_keys, tag, colsep)
      csv_file = csv_data.read

      csv_file = csv_file.encode("UTF-8", :invalid => :replace, :undef => :replace, :replace => "?")

      colsep ||= ";"
      lines    = CSV.parse(csv_file, options = {:col_sep => colsep})
      header   = lines.shift

      header.map! { |e| e.snake }

      @tag          = tag
      @target_model = model_name.classify.constantize

      lines.each do |line|
        attributes = Hash[header.zip line]
        delete_keys.each do |key|
          attributes.delete(key)
        end

        ## Facebook conversion from various formats
        attributes['facebook'] = FacebookService.convert_facebook_id(attributes['facebook'])

        ## Date conversion from various formats
        if attributes['birthday']
          bday = attributes['birthday']
          date = ParseDateService.parse(bday, :short_form => '%d.%m.%y') rescue nil
          date ||= Date.strptime(bday, "%d.%m.%y") rescue nil
          date ||= Date.strptime(bday, "%d/%m/%y") rescue nil
          date ||= Date.strptime(bday, "%Y-%m-%d") rescue nil
        end
        attributes['birthday'] = date if date


        new_item = @target_model.find_by_email(attributes["email"]) || @target_model.create(attributes)
        new_item.update_attribute('birthday', attributes['birthday'])
        new_item.update_attribute('facebook', attributes['facebook'])

        if new_item.birthday && (new_item.birthday.year > 1998)
          new_item.birthday = new_item.birthday - 100.years
        end
        new_item.save

        new_item.reload
        new_item.event_list.add(@tag, parse: true)
        new_item.save

      end
    end
  end
end
