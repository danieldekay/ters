# == Schema Information
#
# Table name: inscriptions
#
#  id                       :integer          not null, primary key
#  type                     :string
#  first_name               :string
#  last_name                :string
#  email                    :string
#  gender                   :string
#  role                     :string
#  birthday                 :date
#  street                   :string
#  city                     :string
#  zip                      :string
#  country                  :string
#  facebook                 :string
#  gmaps                    :boolean
#  latitude                 :float
#  longitude                :float
#  distance                 :decimal(, )
#  zodiac_sign_id           :integer
#  dietary_class            :integer          default(0)
#  dietary_incompatibility  :integer          default(0)
#  permission_to_store_data :boolean          default(TRUE)
#  message                  :text
#  telephone                :string
#  partner_email            :string
#  partner_first_name       :string
#  partner_last_name        :string
#  user_id                  :integer
#  suggestion_id            :integer
#  partner_id               :integer
#  created_at               :datetime
#  updated_at               :datetime
#  state                    :string           default("start")
#  processing_list          :string           default("start")
#  email_state              :string           default("start")
#

class Registration < Inscription

  include Payable
  include Listable
  include RegistrationStateable
  include Geotagging
  zodiac_reader :birthday

  delegate :email, to: :user


  scope :accepted, -> { where(state: 'accepted') }
  scope :signed_up, -> { where(state: 'signed_up') }
  scope :paid, -> { where(state: 'paid') }

  scope :error, -> { where(state: 'start') }

  scope :refund, -> { where(state: 'refund') }
  scope :cancelled, -> { where(state: 'cancelled') }
  scope :free, -> { where(state: 'free_ticket') }
  scope :pays_at_door, -> { where(state: 'pays_at_door') }

  scope :nolist, -> { where(processing_list: 'nolist') }
  scope :waitinglist, -> { where(processing_list: 'waitinglist') }
  scope :blacklist, -> { where(processing_list: 'blacklist') }
  scope :wishlist, -> { where(processing_list: 'wishlist') }
  scope :guestlist, -> { where(processing_list: 'guestlist') }
  scope :cancelledlist, -> { where(processing_list: 'cancelledlist') }
  scope :mostly_in, -> { where(state: %w(accepted paid free_ticket pays_at_door)) }
  scope :basically_in, -> { where(state: %w(paid free_ticket pays_at_door)) }

  def is_guest?
    (self.paid? or self.pays_at_door? or self.free_ticket?)
  end

end
