# == Schema Information
#
# Table name: partnerships
#
#  id                     :integer          not null, primary key
#  inscription_id         :integer
#  matched_inscription_id :integer
#  kind                   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class Partnership < ActiveRecord::Base
  # as described in http://collectiveidea.com/blog/archives/2015/07/30/bi-directional-and-self-referential-associations-in-rails/
  # alice.matched_inscriptions << bob

  belongs_to :inscription
  belongs_to :matched_inscription, class_name: 'Inscription'

  after_create :create_inverse, unless: :has_inverse?
  after_destroy :destroy_inverses, if: :has_inverse?


  def create_inverse
    self.class.create(inverse_partnership_options)
  end

  def destroy_inverses
    inverses.destroy_all
  end

  def has_inverse?
    self.class.exists?(inverse_partnership_options)
  end

  def inverses
    self.class.where(inverse_partnership_options)
  end

  def inverse_partnership_options
    { matched_inscription_id: inscription_id, inscription_id: matched_inscription_id }
  end

  def to_s
    "#{inscription_id}->#{matched_inscription_id} as #{kind}"
  end

end
