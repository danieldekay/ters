class SuggestionWorkflowDecorator

  def self.format_event_tags(list)
    output = ''
    list.each do |item|
      if item.include? "out"
        output += "<span class='btn btn-xs btn-danger'>#{item}</span>"
      else
        output += "<span class='btn btn-xs btn-success'>#{item}</span>"
      end
      output += " "
    end
    output.html_safe
  end
end
