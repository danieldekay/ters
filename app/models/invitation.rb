# == Schema Information
#
# Table name: inscriptions
#
#  id                       :integer          not null, primary key
#  type                     :string
#  first_name               :string
#  last_name                :string
#  email                    :string
#  gender                   :string
#  role                     :string
#  birthday                 :date
#  street                   :string
#  city                     :string
#  zip                      :string
#  country                  :string
#  facebook                 :string
#  gmaps                    :boolean
#  latitude                 :float
#  longitude                :float
#  distance                 :decimal(, )
#  zodiac_sign_id           :integer
#  dietary_class            :integer          default(0)
#  dietary_incompatibility  :integer          default(0)
#  permission_to_store_data :boolean          default(TRUE)
#  message                  :text
#  telephone                :string
#  partner_email            :string
#  partner_first_name       :string
#  partner_last_name        :string
#  user_id                  :integer
#  suggestion_id            :integer
#  partner_id               :integer
#  created_at               :datetime
#  updated_at               :datetime
#  state                    :string           default("start")
#  processing_list          :string           default("start")
#  email_state              :string           default("start")
#

class Invitation < Inscription
  include InvitationStateable
  delegate :email, to: :user

end
