class InvitationWorkflowHelper

  def self.invitation_expiry_date_text
    date = eval "DateTime.now #{Settings.features.invitations.expiry}"
    date.stamp("Thursday, Aug 5th")
    "until #{date.stamp('Monday, August 1st 2013')}"
  end

  def self.invitation_expiry_date(invitation)
    date = eval "invitation.user.invitation_sent_at #{Settings.features.invitations.expiry}"
    date.stamp("Thursday, Aug 5th")
    "until #{date.stamp('Monday, August 1st 2013')}" unless invitation.declined?
  end

  def self.invitation_expired?(invitation)
    if invitation.declined? or invitation.expired?
      true
    elsif invitation.sent?
      date = eval "invitation.user.invitation_sent_at #{Settings.features.invitations.expiry}"
      date += 3.days
      date.past?
    else
      false
    end
  end

  def self.invitation_acceptance_url (user)
    if Rails.env.development?

      if user.invitation_token.nil?
        return "http://localhost:3000"
      else
        return "http://localhost:3000/invitation/accept/#{user.id}/#{user.invitation_token}"
      end

    else
      if user.invitation_token.nil?
        return Settings.marathon.registration_website
      else
        return Settings.marathon.registration_website + "/invitation/accept/#{user.id}/#{user.invitation_token}"
      end
    end
  end



end
