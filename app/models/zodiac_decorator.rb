class ZodiacDecorator
  def self.format_zodiac_sign_for_inscription(inscription)
    if inscription.zodiac_sign_id
      zodiacs = [0, "2648", "2649", "264A", "264B", "264C", "264D", "264E", "264F", "2650", "2651", "2652", "2653"]
      return "<span class='zodiac'>&#x#{zodiacs[inscription.zodiac_sign_id]}</span>".html_safe
    end
  end

  def self.format_zodiac_sign_id(zodiac_sign_id)
    if zodiac_sign_id
      zodiacs = [0, "2648", "2649", "264A", "264B", "264C", "264D", "264E", "264F", "2650", "2651", "2652", "2653"]
      return "<span class='zodiac'>&#x#{zodiacs[zodiac_sign_id]}</span>".html_safe
    end
  end

  def self.format_birthday(registration)
    if registration.birthday
      day = registration.birthday.stamp("Nov 5th").html_safe
      if (["Nov 11th", "Nov 12th", "Nov 13th"].include? day)
        day = "<b>" + day + "</b>"
      end
      return day.html_safe
    end
  end


end
