class SuggestedParticipant::Cell::Form < ::Cell::Concept
  inherit_views SuggestedParticipant::Cell

  include ActionView::RecordIdentifier
  include SimpleForm::ActionViewExtensions::FormHelper
  include ApplicationHelper

  property :contract

  def show
    render :form
  end


end
