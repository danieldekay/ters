class SuggestedParticipant < ActiveRecord::Base
  class SuggestPartner < Trailblazer::Operation
    # call with
    # SuggestedParticipant::SuggestPartner(model: registration)


    def process(params)
      @model = params[:model]

      if make_it_happen
        Rails.logger.info "#{@model.name} has suggested #{@model.partner_email}"
      else
        Rails.logger.error 'Suggesting the Partner failed'
      end
    end

    private

    def make_it_happen
      begin
        if @model
          @params = ActionController::Parameters.new({
                                                         suggested_participant: {
                                                             first_name: @model.partner_first_name,
                                                             last_name:  @model.partner_last_name,
                                                             email:      @model.partner_email,
                                                             role:       ApplicationDecorator.flip_role(@model.role),
                                                             gender:     ApplicationDecorator.flip_gender(@model.role),
                                                             city:       @model.city,
                                                             country:    @model.country,
                                                             message:    "was set as partner during sign-up"
                                                         }
                                                     })
          @sugg   = SuggestedParticipant.new(sugg_params)

          if @sugg.save
            SuggestedParticipant::SuggestedBy.(model: @model, suggests: @sugg)
          else
            Rails.logger.error { "Suggestion created failed #{@params}}" }
            false
          end
        else
          false
        end
      rescue => e
        Rails.logger.error { "Suggestion created failed:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

    def sugg_params
      @params.require(:suggested_participant).permit(:first_name, :last_name, :email, :role, :gender, :city, :country, :message)
    end

  end

end
