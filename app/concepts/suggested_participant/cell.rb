class SuggestedParticipant::Cell < Cell::Concept
  property :email

  def show
    render
  end

  private
end
