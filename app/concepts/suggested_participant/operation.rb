class SuggestedParticipant < ActiveRecord::Base
  class Create < Trailblazer::Operation

    include Model
    model SuggestedParticipant, :create

    contract do
      property :first_name, validates: { presence: true }
      property :last_name, validates: { presence: true }
      property :email, validates: { presence: true }
      property :gender, validates: { presence: true }
      property :role, validates: { presence: true }
      property :city, validates: { presence: true }
      property :country, validates: { presence: true }
      property :facebook
      property :message
    end

    def process(params)
      validate(params[:suggested_participant]) do |f|
        f.save
      end
    end

  end

  class Update < Create
    model SuggestedParticipant, :update
  end

end
