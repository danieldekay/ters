module SuggestedParticipant::Contract
  class Create < Reform::Form
    model SuggestedParticipant

    property :first_name, validates: { presence: true }
    property :last_name, validates: { presence: true }
    property :gender, validates: { presence: true }
    property :role, validates: { presence: true }
    property :city, validates: { presence: true }
    property :country, validates: { presence: true }
    property :facebook
    property :message

    private

  end


  class Update < Create
  end
end
