class SuggestedParticipant < ActiveRecord::Base
  class SuggestedBy < Trailblazer::Operation

    def process(params)
      @model = params[:model]
      @sugg  = params[:suggests]

      if make_it_happen
        Rails.logger.info "#{@model.name} has suggested #{@sugg.name}"
      else
        Rails.logger.error "SuggestedBy: could not associate #{params}"
      end
    end

    private

    def make_it_happen
      begin
        if @model and @sugg
          @model.suggested_participants << @sugg
          @model.save
        else
          false
        end
      rescue => e
        Rails.logger.error { "GetSuggestions:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

  end

end
