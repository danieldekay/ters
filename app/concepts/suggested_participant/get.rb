class SuggestedParticipant < ActiveRecord::Base
  class Get < Trailblazer::Operation

    def process(params)
      @model       = params[:model]
      @kind        = params[:kind]
      @suggestions = @model.suggested_participants

      if make_it_happen
        Rails.logger.info 'GetSuggestions: this model has some'
      else
        Rails.logger.error 'GetSuggestions: this model does not have any'
      end
    end

    def all # returns array of suggestions
      if @suggestions
        return @suggestions
      end
      nil
    end

    private

    def make_it_happen
      begin
        if @suggestions
          true
        else
          false
        end
      rescue => e
        Rails.logger.error { "GetSuggestions:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

  end

end
