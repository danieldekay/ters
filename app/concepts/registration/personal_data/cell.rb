class Registration::PersonalData < Cell::Concept
  property :name
  property :telephone
  property :email
  property :street
  property :city
  property :zip
  property :country
  property :permission_to_store_data

  def show
    render
  end

  private
  def address
    model.street + ' <br/>' + model.zip + ' ' + model.city + '<br/>' + model.country rescue ""
  end
  def email
    model.email
  end
  def permission?
    model.permission_to_store_data
  end
end
