class Registration::Partner < Cell::Concept
  property :partner
  property :partner_email

  def show
    if model.partner.present?
      render
    elsif model.partner_email.present?
      render :partner_not_registered
    else
      render :no_partner
    end
  end

  private
  def partner_email
    partner.email
  end
end
