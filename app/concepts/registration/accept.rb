class Registration < Inscription
  class Accept < Trailblazer::Operation
    @action = 'Sending invitation email'

    def process(params)
      @model = params[:model]

      if accept
        Rails.logger.info "#{@action} for  #{@model.email}"
      else
        Rails.logger.error "#{@action} failed"
      end
    end

    private

    def accept
      begin
        @model.accept
        @model.accept_list
      rescue
        raise 'something weird happened'
      end
    end

  end

end
