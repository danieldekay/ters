class Registration < Inscription

  class SendYouAreIn < Trailblazer::Operation
    @action   = 'Sending you_are_in email'
    @response = ''

    def process(params)
      @model = params[:user]

      if make_it_happen
        Rails.logger.info "#{@action} succeeded for #{@model.email}: response code: #{@response}"
      else
        Rails.logger.error "#{@action} failed with response #{@response}"
      end
    end

    private

    def make_it_happen
      begin
        if @model
          if send_via_api(@model.registration)
            @model.invitation_sent_at = DateTime.now
            @model.save!
          end
          return true
        else
          false
        end
      rescue => e
        Rails.logger.error { "#{@action}:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

    def send_via_api(reg)
      require 'createsend'

      # Authenticate with your API key
      auth            = { :api_key => Rails.application.secrets.email_provider_apikey }

      # The unique identifier for this smart email
      smart_email_id  = Settings.mailtemplate.you_are_in

      # Create a new mailer and define your message
      tx_smart_mailer = CreateSend::Transactional::SmartEmail.new(auth, smart_email_id)
      message         = {
          'To'   => "#{reg.first_name} #{reg.last_name} <#{reg.user.email}>",
          'Data' => {
              'lang=x-wrapper'         => 'lang=x-wrapperTestValue',
              'lang=x-size-8'          => 'lang=x-size-8TestValue',
              'lang=x-size-9'          => 'lang=x-size-9TestValue',
              'lang=x-size-10'         => 'lang=x-size-10TestValue',
              'lang=x-size-11'         => 'lang=x-size-11TestValue',
              'lang=x-size-12'         => 'lang=x-size-12TestValue',
              'lang=x-size-13'         => 'lang=x-size-13TestValue',
              'lang=x-size-14'         => 'lang=x-size-14TestValue',
              'lang=x-size-15'         => 'lang=x-size-15TestValue',
              'lang=x-size-16'         => 'lang=x-size-16TestValue',
              'lang=x-size-17'         => 'lang=x-size-17TestValue',
              'lang=x-size-18'         => 'lang=x-size-18TestValue',
              'lang=x-size-20'         => 'lang=x-size-20TestValue',
              'lang=x-size-22'         => 'lang=x-size-22TestValue',
              'lang=x-size-24'         => 'lang=x-size-24TestValue',
              'lang=x-size-26'         => 'lang=x-size-26TestValue',
              'lang=x-size-28'         => 'lang=x-size-28TestValue',
              'lang=x-size-30'         => 'lang=x-size-30TestValue',
              'lang=x-size-32'         => 'lang=x-size-32TestValue',
              'lang=x-size-34'         => 'lang=x-size-34TestValue',
              'lang=x-size-36'         => 'lang=x-size-36TestValue',
              'lang=x-size-40'         => 'lang=x-size-40TestValue',
              'lang=x-size-44'         => 'lang=x-size-44TestValue',
              'lang=x-size-48'         => 'lang=x-size-48TestValue',
              'lang=x-size-56'         => 'lang=x-size-56TestValue',
              'lang=x-size-64'         => 'lang=x-size-64TestValue',
              'x-apple-data-detectors' => 'true',
              'href^="tel"'            => 'href^="tel"TestValue',
              'href^="sms"'            => 'href^="sms"TestValue',
              'owa'                    => 'owaTestValue',
              'portal_link'            => Settings.marathon.registration_website,
              'firstname'              => reg.first_name,
          }
      }

      # Send the message and save the response
      @response        = tx_smart_mailer.send(message)

      log = EmailLog.new_with_response(@response, :you_are_in, reg.email, smart_email_id)
      if log.save
        Rails.logger.info { "#{@action}: sent and logged" }
      else
        Rails.logger.error { "#{@action}: sent and NOT logged" }
      end

      # [#<Hashie::Mash MessageID="4cc13bb0-12d3-11e6-81a6-4af6ba00cfdc" Recipient="\"Da Ka\" <daniel@dekay.org>" Status="Accepted">]
      ## TODO use the response for error handling
      @response.first['Status'] == 'Accepted'
    end

  end

end
