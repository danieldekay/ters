class Registration < Inscription
  class Cancel < Trailblazer::Operation
    def process(params)
      @model  = params[:model]

      if cancel
      else
        false
      end
    end

    private

    def cancel
      begin
        @model.cancel_registration
      rescue
        raise 'something weird happened'
      end
    end

  end

end
