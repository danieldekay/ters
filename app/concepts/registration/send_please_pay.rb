class Registration < Inscription

  class SendPleasePay < Trailblazer::Operation
    @action   = 'Sending please_pay email'
    @response = ''

    def process(params)
      @model = params[:user]

      if make_it_happen
        Rails.logger.info "#{@action} for  #{@model.email}: response code: #{@response}"
      else
        Rails.logger.error "#{@action} failed"
      end
    end

    private

    def make_it_happen
      begin
        if @model
          if send_mail_via_api(@model.registration)
            return true
          end
        else
          false
        end
      rescue => e
        Rails.logger.error { "#{@action}:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

    def send_mail_via_api(reg)
      require 'createsend'

      # Authenticate with your API key
      auth            = { :api_key => Rails.application.secrets.email_provider_apikey }

      # The unique identifier for this smart email
      smart_email_id  = Settings.mailtemplate.please_pay

      # Create a new mailer and define your message
      tx_smart_mailer = CreateSend::Transactional::SmartEmail.new(auth, smart_email_id)
      message         = {
          'To'   => "#{reg.first_name} #{reg.last_name} <#{reg.user.email}>",
          'Data' => {
              'x-apple-data-detectors' => 'true',
              'payment_date'           => RegistrationWorkflowDecorator.payment_deadline(reg),
              'firstname'              => reg.first_name,
              'price'                  => Settings.payment.bank.price,
              'IBAN'                   => Settings.payment.bank.IBAN,
              'BIC'                    => Settings.payment.bank.BIC,
              'Bank'                   => Settings.payment.bank.bank,
              'Account'                => Settings.payment.bank.account,
              'Price_paypal'           => Settings.payment.paypal.price,
              'link_paypal'            => RegistrationWorkflowDecorator.paypal_link
          }
      }

      # Send the message and save the response
      @response        = tx_smart_mailer.send(message)

      log = EmailLog.new_with_response(@response, :please_pay, reg.email, smart_email_id)
      if log.save
        Rails.logger.info { "#{@action}: sent and logged" }
      else
        Rails.logger.error { "#{@action}: sent and NOT logged" }
      end

      # [#<Hashie::Mash MessageID="4cc13bb0-12d3-11e6-81a6-4af6ba00cfdc" Recipient="\"Da Ka\" <daniel@dekay.org>" Status="Accepted">]
      ## TODO use the response for error handling
      @response.first['Status'] == 'Accepted'
    end

  end

end
