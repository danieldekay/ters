class Registration < Inscription
  class SignUp < Trailblazer::Operation
    def process(params)
      @model = params[:model]

      if sign_up
      else
        raise 'Could not sign_up!'
      end
    end

    private

    def sign_up
      begin
        @model.sign_up
        if @model.state == 'signed_up'
          true
        else
          false
        end
      rescue
        false
      end
    end

  end

end
