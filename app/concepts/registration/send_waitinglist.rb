class Registration < Inscription

  class SendWaitinglist < Trailblazer::Operation
    @action   = 'Sending waitinglist email'
    @response = ''

    def process(params)
      @model = params[:user]

      if make_it_happen
        Rails.logger.info "#{@action} for  #{@model.email}: response code: #{@response}"
      else
        Rails.logger.error "#{@action} failed"
      end
    end

    private

    def make_it_happen
      begin
        if @model
          if @response = send_via_api(@model.registration)
            @model.invitation_sent_at = DateTime.now
            @model.save!
          end
          return true
        else
          false
        end
      rescue => e
        Rails.logger.error { "#{@action}:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

    def send_invitation_via_api(reg)
      require 'createsend'

      # Authenticate with your API key
      auth            = { :api_key => Rails.application.secrets.email_provider_apikey }

      # The unique identifier for this smart email
      smart_email_id  = Settings.mailtemplate.waitinglist

      # Create a new mailer and define your message
      tx_smart_mailer = CreateSend::Transactional::SmartEmail.new(auth, smart_email_id)
      message         = {
          'To'   => "#{reg.first_name} #{reg.last_name} <#{reg.user.email}>",
          'Data' => {
              'x-apple-data-detectors' => 'true',
              'firstname'              => reg.first_name,
          }
      }

      # Send the message and save the response
      response        = tx_smart_mailer.send(message)

      # [#<Hashie::Mash MessageID="4cc13bb0-12d3-11e6-81a6-4af6ba00cfdc" Recipient="\"Da Ka\" <daniel@dekay.org>" Status="Accepted">]
      ## TODO use the response for error handling
    end

  end

end
