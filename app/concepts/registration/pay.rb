class Registration < Inscription
  class Pay < Trailblazer::Operation
    def process(params)
      @model  = params[:model]
      @amount = params[:amount]
      @ticket = params[:ticket]

      if pay
      else
        false
      end
    end

    private

    def pay
      begin
        if @amount & @amount == Settings.payment.price
          @ticket = :payment
        elsif @amount
          @ticket = :partial_payment
        end

        case @ticket
          when :free
            @model.free_ticket
            @model.guest_list
          when :payment
            @model.payment_arrived
            @model.guest_list
          when :pays_at_door
            @model.pays_at_door
            @model.guest_list
          when :partial_payment
            @model.pays_at_door
            @model.guest_list
          else
            false
        end

      rescue
        raise 'something weird happened'
      end
    end

  end

end
