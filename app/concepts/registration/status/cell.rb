class Registration::Status < Cell::Concept
  property :state
  property :name

  def show
    render state.to_sym
  end

  private


end
