module Registration::Contract
  class Create < Reform::Form
    model Registration
    feature Disposable::Twin::Persisted

    property :first_name, validates: { presence: true }
    property :type
    property :last_name, validates: { presence: true }
    property :gender, validates: { presence: true }
    property :role, validates: { presence: true }
    property :birthday, validates: { presence: true }
    property :street, validates: { presence: true }
    property :city, validates: { presence: true }
    property :zip, validates: { presence: true }
    property :country, validates: { presence: true }
    property :facebook
    property :gmaps
    property :latitude
    property :longitude
    property :distance
    property :zodiac_sign_id
    property :dietary_class
    property :dietary_incompatibility
    property :permission_to_store_data
    property :message
    property :telephone
    property :partner_email
    property :partner_first_name
    property :partner_last_name

    validates :user, presence: true

    property :user,
             # prepopulator:      lambda { |options| self.user = User.new },
             populator: :populate_user! do
      property :password, validates: { presence: true }
      property :password_confirmation, validates: { presence: true }
      validates :password, length: { in: 4..20 }

      property :email
      validates :email, presence: true, email: true
      validates_uniqueness_of :email # might break with Reform 2.2
    end

    private

    def populate_user!(fragment:, **)
      self.user = (model.user or (User.new))
    end

    def setup_model!(params)
      model.user ||= User.new
    end

  end


  class Update < Create
    model Registration
  end
end
