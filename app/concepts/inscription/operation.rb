class Inscription < ActiveRecord::Base
  class Create < Trailblazer::Operation
    include Model

    model Inscription, :create

    contract Contract::Create

    def process(params)
      if params[:inscription]
        params[:inscription][:dietary_incompatibility_list] = params[:inscription][:dietary_incompatibility_list].join(',')
        validate(params[:inscription]) do |f|
          f.save # success.
        end
      elsif params[:registration]
        params[:registration][:dietary_incompatibility_list] = params[:registration][:dietary_incompatibility_list].join(',')
        validate(params[:registration]) do |f|
          f.save # success.
        end
      end
    end

    private

    def setup_model!(params)
      model.user ||= User.new
    end
  end

  class Update < Create
    self.builder_class = Create.builder_class
    action :update

    contract Contract::Update
  end

end
