class Inscription::Cell < Cell::Concept
  property :name
  property :telephone
  property :email
  property :telephone
  property :dietary_incompatibility
  property :dietary_class
  property :state
  property :processing_list
  property :partner_email
  property :partner

  def show
    render
  end

  private
  def address
    model.street + ' <br/>' + model.zip + ' ' + model.city + '<br/>' + model.country
  end
  def email
    model.email
  end
end
