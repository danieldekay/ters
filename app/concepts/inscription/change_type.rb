class Inscription < ActiveRecord::Base
  class ChangeType < Trailblazer::Operation

    def process(params)
      @action = 'Changing registration type'
      @model = params[:model] # Inscription-instance
      @target = params[:target] # Class

      if make_it_happen
        Rails.logger.info "#{@action} for  #{@model.email}"
      else
        Rails.logger.error "#{@action} failed"
      end
    end

    private

    # TODO implement some logic that checks if it makes sense to actually change the type in this case.
    # And then also change the error handling.

    def make_it_happen
      begin
        if @model and @target
          @model = @model.becomes!(@target)
          @model.state=:start
          @model.save
          return true
        else
          false
        end
      rescue
        false
      end
    end

  end

end
