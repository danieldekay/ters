module PreviousParticipant::Contract
  class Create < Reform::Form
    model PreviousParticipant

    property :first_name, validates: { presence: true }
    property :last_name, validates: { presence: true }
    property :gender, validates: { presence: true }
    property :role, validates: { presence: true }
    property :city, validates: { presence: true }
    property :country, validates: { presence: true }
    property :facebook

    private

  end

  class New < Create
    model PreviousParticipant
  end

  class Update < Create
    model PreviousParticipant
  end
end
