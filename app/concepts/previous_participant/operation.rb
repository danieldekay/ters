class PreviousParticipant < ActiveRecord::Base
  class Create < Trailblazer::Operation

    include Model
    model PreviousParticipant, :create

    contract do
      property :first_name, validates: { presence: true }
      property :last_name, validates: { presence: true }
      property :email, validates: { presence: true }
      property :role, validates: { presence: true }
      property :city
      property :country
      property :facebook
    end

    def process(params)
      validate(params[:previous_participant]) do |f|
        f.save
      end
    end

  end

  class Update < Create
    model PreviousParticipant, :update
  end

end
