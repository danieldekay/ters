class PreviousParticipant::Cell::Form < ::Cell::Concept
  inherit_views PreviousParticipant::Cell

  include ActionView::RecordIdentifier
  include SimpleForm::ActionViewExtensions::FormHelper
  include ApplicationHelper

  property :contract

  def show
    render :form
  end


end
