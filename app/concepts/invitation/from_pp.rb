class Invitation < Inscription
  class FromPP < Trailblazer::Operation
    @action = 'Converting PP to Invitation'


    def process(params)
      @model = PreviousParticipant.find(params[:pp])

      if make_it_happen
        Rails.logger.info "#{@action} for  #{@model.email}"
      else
        Rails.logger.error "#{@action} failed"
      end
    end

    private

    def make_it_happen
      begin
        if @model
          valid_params = {invitation: nil}
          valid_params[:invitation] = { first_name: @model.first_name,
                                        last_name:  @model.last_name,
                                        city:  @model.city,
                                        country: @model.country,
                                        role: @model.role,
                                        email: @model.email,
          }

          Invitation::Create.(valid_params)
          return true
        else
          false
        end
      rescue => e
        Rails.logger.error { "#{@action}:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

  end

end
