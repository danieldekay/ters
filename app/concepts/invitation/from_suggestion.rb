class Invitation < Inscription
  class FromSuggestion < Trailblazer::Operation
    @action = 'Converting Suggestion to Invitation'


    def process(params)
      @suggestion = params[:suggestion]
      @model = User.find_by_email(@suggestion.email).try(:inscription)

      if @model
        Rails.logger.info "#{@action} failed, since #{@model.email} was already there."
      elsif make_it_happen
        Rails.logger.info "#{@action} for  #{@model.email}"
      else
        Rails.logger.error "#{@action} failed"
      end
    end

    private

    def make_it_happen
      begin
        if @suggestion
          @suggestion.role ||= 'follower' # fix empties, since some pp imported wrongly

          valid_params = {invitation: nil}
          valid_params[:invitation] = { first_name: @suggestion.first_name,
                                        last_name:  @suggestion.last_name,
                                        city:  @suggestion.city,
                                        country: @suggestion.country,
                                        role: @suggestion.role,
                                        gender: 'Man',
                                        email: @suggestion.email,
                                        user: {
                                            email: @suggestion.email,
                                            name: @suggestion.name,
                                            password: 'password1234',
                                            password_confirmation: 'password1234',
                                        }
          }

          @model = Invitation::Create.(valid_params).model
        else
          false
        end
      rescue => e
        Rails.logger.error { "#{@action}:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

  end

end
