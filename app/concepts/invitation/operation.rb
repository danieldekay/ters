class Invitation < Inscription
  class Create < Trailblazer::Operation

    include Model
    model Invitation, :create

    contract do
      include Reform::Form::ModelReflections

      property :first_name, validates: { presence: true }
      property :last_name, validates: { presence: true }
      property :gender
      property :role, validates: { presence: true }
      property :city
      property :country
      property :facebook
      property :message

      validates :user, presence: true

      property :user,
               # prepopulator:      lambda { |options| self.user = User.new },
               populator: :populate_user! do
        property :password, validates: { presence: true }
        property :password_confirmation, validates: { presence: true }
        validates :password, length: { in: 4..20 }

        property :email
        validates :email, presence: true, email: true
        validates_uniqueness_of :email # might break with Reform 2.2
      end

      def populate_user!(fragment:, **)
        self.user ||= User.new
      end
    end

    def process(params)

      begin
      if params[:invitation][:user_attributes][:password].blank?
        params[:invitation][:user_attributes][:password]              = 'password1234'
        params[:invitation][:user_attributes][:password_confirmation] = 'password1234'
      end
      rescue

      end


      validate(params[:invitation]) do |f|
        f.save
      end
    end

    private

    def setup_model!(params)
      model.user ||= User.new
    end
  end

  class Update < Create
    action :update
  end

end
