class Invitation < Inscription
  class PrepareUser < Trailblazer::Operation
    @action = 'User.invite!'

    def process(params)
      @model = params[:invitation]

      if make_it_happen
        Rails.logger.info "#{@action} for  #{@model.email}"
      else
        Rails.logger.error "#{@action} failed"
      end
    end

    private

    def make_it_happen
      begin
        if @model
          @user = @model.user
          @user.invite!(current_user) do |u|
            u.skip_invitation = true
          end
          return true
        else
          false
        end
      rescue => e
        Rails.logger.error { "#{@action}   #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

    unless (defined? current_user)
      def current_user
        nil
      end
    end

    end

  end
