class Invitation < Inscription

  class SendEmail < Trailblazer::Operation
    @action   = 'Sending invitation email'
    @response = ''

    def process(params)
      @model = params[:user]

      if make_it_happen
        Rails.logger.info "#{@action} succeeded for #{@model.email}: response code: #{@response}"
      else
        Rails.logger.error "#{@action} failed with response #{@response}"
      end
    end

    private

    def make_it_happen
      begin
        if @model
          if send_invitation_via_api(@model.invitation)
            @model.save!
          else
            Rails.logger.error { "#{@action}: sending email failed. #{@response}" }
            false
          end
          true
        else
          false
        end
      rescue => e
        Rails.logger.error { "#{@action}:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

    def send_invitation_via_api(invitation)
      require 'createsend'

      # Authenticate with your API key
      auth            = { :api_key => Rails.application.secrets.email_provider_apikey }

      # The unique identifier for this smart email
      smart_email_id  = Settings.mailtemplate.invitation

      # Create a new mailer and define your message
      tx_smart_mailer = CreateSend::Transactional::SmartEmail.new(auth, smart_email_id)
      message         = {
          'To'   => "#{invitation.first_name} #{invitation.last_name} <#{invitation.user.email}>",
          'Data' => {
              'x-apple-data-detectors'   => 'true',
              'firstname'                => invitation.first_name,
              'invitation_deadline'      => InvitationWorkflowHelper.invitation_expiry_date_text,
              'personal_invitation_link' => InvitationWorkflowHelper.invitation_acceptance_url(invitation.user),
              'price'                    => Settings.payment.price,
              'price_paypal'             => Settings.payment.paypal.price,
              'price_cash'               => Settings.payment.cash.price,
              'message'                  => invitation.message,
          }
      }

      # Send the message and save the response
      @response        = tx_smart_mailer.send(message)

      log = EmailLog.new_with_response(@response, :invitation, invitation.email, smart_email_id)

      if log.save && log.status == 'Accepted'
        Rails.logger.info { "#{@action}: sent and logged" }
        @model.invitation_sent_at = DateTime.now
        @model.invitation.email_is_sent
      else
        Rails.logger.error { "#{@action}: sent and NOT logged" }
      end

      # [#<Hashie::Mash MessageID="4cc13bb0-12d3-11e6-81a6-4af6ba00cfdc" Recipient="\"Da Ka\" <daniel@dekay.org>" Status="Accepted">]
      ## TODO use the response for error handling
      @response.first['Status'] == 'Accepted'
    end

  end

end
