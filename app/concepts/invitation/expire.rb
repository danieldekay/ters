class Invitation < Inscription
  class Expire < Trailblazer::Operation
    @action = 'Expiring invitation'


    def process(params)
      @model = params[:model]

      if make_it_happen
        Rails.logger.info "#{@action} #{@model}"
      else
        Rails.logger.error "#{@action} failed"
      end
    end

    private

    def make_it_happen
      begin
        if @model.expire
          true
        else
          false
        end
      rescue => e
        Rails.logger.error { "#{@action}:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

  end

end
