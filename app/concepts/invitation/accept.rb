class Invitation < Inscription
  class Accept < Trailblazer::Operation
    @action = 'Accepting invitation'


    def process(params)
      @model = params[:user]

      if make_it_happen
        Rails.logger.info "#{@action} for  #{@model.email}"
      else
        Rails.logger.error "#{@action} failed"
      end
    end

    private

    def make_it_happen
      begin
        if @model
          @model.accept_invitation!
          return true
        else
          false
        end
      rescue => e
        Rails.logger.error { "#{@action}:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

  end

end
