class Suggestion < Inscription
  class Create < Trailblazer::Operation

    include Model
    model Suggestion, :create

    contract do
      property :first_name, validates: { presence: true }
      property :last_name, validates: { presence: true }
      property :email, validates: { presence: true }
      property :gender, validates: { presence: true }
      property :role, validates: { presence: true }
      property :city, validates: { presence: true }
      property :country, validates: { presence: true }
      property :facebook
      property :message

    end

    def process(params)
      validate(params[:suggestion]) do |f|
        f.save
      end
    end

    private
  end

end
