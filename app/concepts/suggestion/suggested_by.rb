class Suggestion < Inscription
  class SuggestedBy < Trailblazer::Operation

    def process(params)
      @model = params[:model]
      @sugg  = params[:suggests]

      if make_it_happen
        Rails.logger.info "#{@model.name} has suggested #{@sugg.name}"
      else
        Rails.logger.error 'GetSuggestions: this model does not have any'
      end
    end

    private

    def make_it_happen
      begin
        if @model and @sugg
          p = Partnership.create(inscription_id: @model.id, matched_inscription_id: @sugg.id, kind: :suggestion)
          q = Partnership.find_by(inscription_id: @sugg.id, matched_inscription_id: @model.id, kind: nil)
          if p and q
            q.kind = :suggested_by
            q.save
          end
        else
          false
        end
      rescue => e
        Rails.logger.error { "GetSuggestions:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

  end

end
