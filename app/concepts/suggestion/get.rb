class Suggestion < Inscription
  class Get < Trailblazer::Operation

    def process(params)
      @model       = params[:model]
      @kind        = params[:kind]
      @suggestions = Partnership.where(inscription_id: @model.id, kind: @kind)

      if make_it_happen
        Rails.logger.info 'GetSuggestions: this model has some'
      else
        Rails.logger.error 'GetSuggestions: this model does not have any'
      end
    end

    def all # returns array of suggestions
      if @suggestions
        output = Array.new
        @suggestions.each do |ps|
          output.push Inscription.find_by(id: ps.matched_inscription_id)
        end
        return output
      end
      nil
    end

    private

    def make_it_happen
      begin
        if @suggestions
          true
        else
          false
        end
      rescue => e
        Rails.logger.error { "GetSuggestions:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

  end

end
