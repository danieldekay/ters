class Partnership < ActiveRecord::Base
  class GetPartner < Trailblazer::Operation

    def process(params)
      @model       = params[:model]
      @partnership = Partnership.find_by(inscription_id: @model.id, kind: :partner)

      if make_it_happen
        Rails.logger.info  'GetPartner: this model has a partnership, and you can get the partner via .partner'
      else
         Rails.logger.error  'GetPartner: this model does not have a partnership that we can find'
      end
    end

    def partner
        if @partnership
          Inscription.find(@partnership.matched_inscription_id)
        else
          nil
        end
    end

    private

    def make_it_happen
      begin
        if @partnership
          true
        else
          false
        end
      rescue => e
        Rails.logger.error { "GetPartner:  #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

  end

end
