class Partnership < ActiveRecord::Base
  class DefinePartner < Trailblazer::Operation

    def process(params)
      @me      = params[:model]
      @partner = params[:partner]

      if make_it_happen
      else
        Rails.logger.error { "DefinePartner: Could not set partnership" }
      end
    end

    private

    def make_it_happen
      begin
        if @me and @partner
          @me.matched_inscriptions << @partner
          my_side = Partnership.find_by(inscription_id: @me.id)
          my_side.kind = :partner
          their_side = Partnership.find_by(inscription_id: @partner.id)
          their_side.kind = :partner
          my_side.save
          their_side.save
        else
          false
        end
      rescue => e
        Rails.logger.error { "DefinePartner: #{e.message} #{e.backtrace.join('\n')}" }
      end
    end

  end

end
