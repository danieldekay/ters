# == Schema Information
#
# Table name: invitations
#
#  id              :integer          not null, primary key
#  first_name      :string(255)      not null
#  last_name       :string(255)      not null
#  email           :string(255)      not null
#  city            :string(255)
#  country         :string(255)
#  role            :string(255)      not null
#  message         :text
#  facebook        :string(255)
#  inviter_id      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :integer
#  status          :text
#  scopable_status :string(255)
#  couple_status   :text             default("solo")
#

ActiveAdmin.register Invitation do
	menu :parent => 'Raw Database'

	config.per_page = 100

	batch_action :send_email do |selection|
		Invitation.find(selection).each do |invitation|
			InvitationMailer.invite(invitation.user).deliver
			invitation.user.invitation_sent_at = Time.now.utc
			invitation.user.save!(:validate => false)
		end
		redirect_to collection_path, :notice => "Invitations sent by email to #{selection.count} users!"
	end

	batch_action :fix_database do |selection|
		Invitation.find(selection).each do |invitation|
			invitation.role = invitation.role.downcase
			invitation.save!(:validate => false)
		end
		redirect_to collection_path, :notice => "#{selection.count} modified"
	end


	index do
		selectable_column
		column :first_name
		column :last_name
		column :country
		column :email
		column :role
		column :scopable_status
		end



end
