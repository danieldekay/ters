
ActiveAdmin.register Registration, :as => 'PROCESS' do
  menu :priority => 1
  config.per_page = 50
  batch_action :destroy, false

  filter :first_name
  filter :last_name
  filter :role, :as => :select, :collection => [:leader, :follower, :both]

  # if there are Inscriptions, we can do stuff with them
  # if Registration.last != nil
  #  filter :state, :as => :select, :collection => Registration.last.state_paths.to_states
  #  filter :processing_list, :as => :select, :collection => Registration.last.processing_list_paths.to_states
  #  filter :state
  #  filter :processing_list
  # end

  batch_action :accept_into_marathon do |selection|
    Registration.find(selection).each do |post|
      post.accept_list
      post.accept
    end
    redirect_to collection_path, :notice => "#{selection.count} registrations accepted, and are now state=accepted."
  end


  batch_action :send_to_waitinglist do |selection|
    listname = 'waitinglist'
    Registration.find(selection).each do |post|
      post.send_to_waiting_list
    end
    redirect_to collection_path, :notice => "#{selection.count} registrations sent to #{listname}!"
  end

  batch_action :send_to_blacklist do |selection|
    listname = 'blacklist'
    Registration.find(selection).each do |post|
      post.send_to_black_list
    end
    redirect_to collection_path, :notice => "#{selection.count} registrations sent to #{listname}!"
  end

  batch_action :send_to_wishlist do |selection|
    listname = 'wishlist'
    Registration.find(selection).each do |post|
      post.send_to_wish_list
    end
    redirect_to collection_path, :notice => "#{selection.count} registrations sent to #{listname}!"
  end



  batch_action :payment_arrived do |selection|
    Registration.find(selection).each do |post|
      post.payment_arrived
      post.guest_list
    end
    redirect_to collection_path, :notice => "#{selection.count} registrations have been paid."
  end

  batch_action :free_ticket do |selection|
    Registration.find(selection).each do |post|
      post.free_ticket ? post.set_comment('free_ticket') : ''
    end
    redirect_to collection_path, :notice => "#{selection.count} registrations marked with free ticket."
  end

  batch_action :pays_at_door do |selection|
    Registration.find(selection).each do |post|
      post.pays_at_door ? post.set_comment('pays at door') : ''
      post.guest_list
    end
    redirect_to collection_path, :notice => "#{selection.count} registrations marked as pays_at_door."
  end

  batch_action :reset_list do |selection|
    listname = 'no_list'
    Registration.find(selection).each do |post|
      post.mrs_reset_list
    end
    redirect_to collection_path, :notice => "#{selection.count} registrations lists resetted!"
  end

  batch_action :reset_status_and_list do |selection|
    Registration.find(selection).each do |post|
      post.state = :signed_up
      post.mrs_reset_list
      post.save!
    end
    redirect_to collection_path, :notice => "#{selection.count} registration states resetted!"
  end

  batch_action :has_not_paid_thus_out do |selection|
    listname = 'blacklist'
    Registration.find(selection).each do |post|
      post.has_not_paid
      post.send_to_black_list
    end
    redirect_to collection_path, :notice => "#{selection.count} registrations sent to #{listname}!"
  end

  batch_action :has_cancelled do |selection|
    Registration.find(selection).each do |post|
      post.cancel_registration
    end
    redirect_to collection_path, :notice => "#{selection.count} registrations were cancelled!"
  end

  batch_action :do_refund do |selection|
    Registration.find(selection).each do |post|
      post.do_refund
    end
    redirect_to collection_path, :notice => "#{selection.count} registrations were refunded!"
  end


  scope :leaders
  scope :followers
  scope :both
  scope :all
  scope :accepted
  scope :signed_up
  scope :refund


  index do
    selectable_column
    column :id
    column :updated, :sortable => :updated_at  do |reg|
      # reg.updated_at.stamp("2013-11-01 23:45")
      distance_of_time_in_words_to_now(reg.updated_at)
    end
    column 'M' do |reg|
      unless reg.message.to_s.empty?
        result = "<span class='label' title='#{reg.message.to_s.tr('\"\'', '')}'><img style='width:12px;' src='#{asset_path 'glyphicons_free/glyphicons/png/glyphicons_120_message_full.png'}'/></span>".html_safe
      end
    end
    column :first_name
    column :last_name
    column 'P' do |reg|
      if reg.partner
        result = "<span class='label' title='#{reg.partner.name.to_s + '/' + reg.partner_email}'><img style='width:15px;' src='#{asset_path 'glyphicons_free/glyphicons/png/glyphicons_043_group.png'}'/></span><span class='small'>#{reg.partner.id}</span>".html_safe
      end
    end
    column :city
    column :country
    column 'whois?' do |reg|
      result = link_to 'fb', "https://www.facebook.com/search/more/?q=#{reg.first_name}%20#{reg.last_name}", target: '_blank', class: 'label-fb'
      result += ' '
      result += link_to 'g', "https://www.google.de/#newwindow=1&q=#{reg.first_name}+#{reg.last_name}+tango", target: '_blank', class: 'label-g'
    end

    column :role do |reg|
      status_tag(reg.role[0..3])
    end

    column :processing_list do |reg|
      status_tag(reg.processing_list)
    end
    column :list do |reg|
      status_tag(reg.state)
    end


  end

end

