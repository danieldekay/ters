ActiveAdmin.register Registration, :as => 'Dorm' do
  menu :priority => 7000

  batch_action :destroy, false
  remove_filter :sleeps_in

  scope :all

  scope :signed_up
  scope :submitted
  scope :paid
  scope :guestlist
  scope :waitinglist

  config.per_page = 30

  index do
    selectable_column
    column :id do |reg|
      result = link_to reg.id, :controller => '/registrations', :id => reg.id, :action => 'admin_show', target: 'blank'
    end

    column 'Updt', :sortable => :updated_at  do |reg|
      reg.updated_at.stamp('11/01 23:45')
    end

    column 'Name' do |reg|
      reg.name
    end

    column 'Status' do |reg|
      test = reg.incomplete?
      sign = test ? status_tag('error') : status_tag(reg.state_name.to_s)
    end

    column 'Dorm?' do |reg|
	    test = reg.dormitory_booked?
	    sign = status_tag(test.to_s)
    end

    column 'Which Dorm' do |reg|
	    sign = ''
			 reg.dormitories.each do |dorm|
				 sign += dorm.name.to_s + ' '
			 end
	    sign
    end



  end

  # Labels can decorate_date_label changed en.yml

  batch_action :send_two_month_reminder_to , :priority => 5 do |selection|
	  Registration.find(selection).each do |post|
		  post.send_email(:two_month_reminder)	    if post.list_guestlist?
	  end
	  redirect_to collection_path, :notice => "#{selection.count} /two month reminder/ emails sent!"
  end

  batch_action :send_final_info_to , :priority => 5 do |selection|
    Registration.find(selection).each do |post|
	    post.send_email(:final_info)	    if post.list_guestlist?
    end
    redirect_to collection_path, :notice => "#{selection.count} /final info/ emails sent!"
  end

end
