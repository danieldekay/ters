ActiveAdmin.register Registration, :as => 'Guestlist' do
  menu :priority => 900
  config.per_page = 240
  batch_action :destroy, false
  remove_filter :sleeps_in

  controller do
    def scoped_collection
      Registration.guestlist
    end
  end

  csv force_quotes: true, col_sep: ';' do
    column :id
    column :first_name
    column :last_name
    column :email
    column :street
    column :city
    column :zip
    column :country
    column :processing_list
    column :state
    column :created_at
    column :updated_at
    column :dob do |reg|
      reg.birthday.stamp('03-31-2001')
    end
  end

  index do
    selectable_column
    column :id
    column :updated, :sortable => :updated_at  do |reg|
      reg.updated_at.stamp('2013-11-01 23:45')
    end
    column :first_name
    column :last_name
    column :country

    column :role  do |reg|
      status_tag(reg.role)
    end
    column :state do |reg|
      status_tag(reg.state)
    end
    column :dob do |reg|
      reg.birthday.stamp('03-31-2001')
    end
  end


end
