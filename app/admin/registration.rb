ActiveAdmin.register Registration, :as => 'Messages' do
  menu :priority => 600
  config.per_page = 100

  remove_filter :sleeps_in

  controller do
    def scoped_collection
        Registration.where("message <> ''")
    end
  end


  index do
    column :first_name
    column :last_name
    column :message
  end
end

ActiveAdmin.register Registration, :as => 'Couples' do
  menu :priority => 600
  config.per_page = 50

  controller do
    def scoped_collection
        Registration.where("partner_email <> ''")
    end
  end


  index do

    column :state do |reg|
      status_tag(reg.state)
    end

    column :role do |reg|
      status_tag(reg.role[0..3])
    end


    column :name do |reg|
      link_to reg.name, admin_inscription_path(reg)
    end
    column :partner

    column 'P Role' do |reg|
      if reg.partner
        result = status_tag(reg.partner.role[0..3])
      end
    end

    column 'P Status' do |reg|
      if reg.partner
        result = status_tag(reg.partner.state)
      end
    end

  end
end


ActiveAdmin.register Registration, :as => 'Wishlist' do
  menu :priority => 2
  menu :parent => 'Lists'

  config.per_page = 240
  batch_action :destroy, false
  remove_filter :sleeps_in

  scope :all
  scope :leaders
  scope :followers
  scope :both


  controller do
    # def scoped_collection
    #   Registration.wishlist
    # end
  end

  batch_action :accept_into_marathon do |selection|
    Registration.find(selection).each do |post|
      post.accept_list
      post.accept
    end
    redirect_to collection_path, :notice => "#{selection.count} Inscriptions accepted, and are now state=accepted."
  end

  batch_action :move_to_waitinglist do |selection|
    Registration.find(selection).each do |post|
      post.send_to_waiting_list
    end
    redirect_to collection_path, :notice => "#{selection.count} Inscriptions sent to waitinglist."
  end


  config.sort_order = 'distance_desc'

  index do
    selectable_column
    column :id do |reg|
      result = link_to reg.id, :controller => '/Inscriptions', :id => reg.id, :action => 'admin_show', target: '_blank'
    end
    column :updated, :sortable => :updated_at do |reg|
      reg.updated_at.stamp('11-01 23:45')
    end
    column 'M' do |reg|
      unless reg.message.to_s.empty?
        result = "<span class='label' title='#{reg.message.to_s}'><img style='width:12px;' src='#{asset_path 'glyphicons_free/glyphicons/png/glyphicons_120_message_full.png'}'/></span>".html_safe
      end
    end
    column 'P' do |reg|
      if reg.partner
        result = "<span class='label' title='#{reg.partner.name.to_s + '/' + reg.partner_email.to_s}'><img style='width:15px;' src='#{asset_path 'glyphicons_free/glyphicons/png/glyphicons_043_group.png'}'/></span><span class='small'>#{reg.partner.id}</span>".html_safe
      end
    end
    column 'whois?' do |reg|
      result = link_to 'fb', "https://www.facebook.com/search/more/?q=#{reg.first_name}%20#{reg.last_name}", target: '_blank', class: 'label-fb'
      result += ' '
      result += link_to 'g', "https://www.google.de/#newwindow=1&q=#{reg.first_name}+#{reg.last_name}+tango", target: '_blank', class: 'label-g'
    end

    column :first_name
    column :last_name

    column 'dist', :sortable => :distance do |reg|
      result = reg.distance.to_i.to_s + ' km'
      result.html_safe
    end

    column :city
    column :country

    column :birthday do |reg|
      reg.birthday.stamp('2000-11-01')
    end

    column :role do |reg|
      status_tag(reg.role[0..3])
    end

    column 'Workshop' do |reg|
      reg.print_workshop
    end

  end


end


ActiveAdmin.register Registration, :as => 'Waitinglist' do
  menu :priority => 2
  menu :parent => 'Lists'
  config.per_page = 240
  batch_action :destroy, false
  remove_filter :sleeps_in

  scope :all
  scope :leaders
  scope :followers
  scope :both

  controller do
    # def scoped_collection
    #   Registration.waitinglist
    # end
  end

  batch_action :accept_into_marathon do |selection|
    Registration.find(selection).each do |post|
      post.accept_list
      post.accept
    end
    redirect_to collection_path, :notice => "#{selection.count} Inscriptions accepted, and are now state=accepted."
  end

  index do
    selectable_column
    column :id do |reg|
      result = link_to reg.id, :controller => '/Inscriptions', :id => reg.id, :action => 'admin_show', target: '_blank'
    end
    column :updated, :sortable => :updated_at do |reg|
      reg.updated_at.stamp('11-01 23:45')
    end
    column 'M' do |reg|
      unless reg.message.to_s.empty?
        result = "<span class='label' title='#{reg.message.to_s}'><img style='width:12px;' src='#{asset_path 'glyphicons_free/glyphicons/png/glyphicons_120_message_full.png'}'/></span>".html_safe
      end
    end
    column 'P' do |reg|
      if reg.partner
        result = "<span class='label' title='#{reg.partner.name.to_s + '/' + reg.partner_email.to_s}'><img style='width:15px;' src='#{asset_path 'glyphicons_free/glyphicons/png/glyphicons_043_group.png'}'/></span><span class='small'>#{reg.partner.id}</span>".html_safe
      end
    end
    column 'whois?' do |reg|
      result = link_to 'fb', "https://www.facebook.com/search/more/?q=#{reg.first_name}%20#{reg.last_name}", target: '_blank', class: 'label-fb'
      result += ' '
      result += link_to 'g', "https://www.google.de/#newwindow=1&q=#{reg.first_name}+#{reg.last_name}+tango", target: '_blank', class: 'label-g'
    end
    column :first_name
    column :last_name

    column :city
    column :country

    column :birthday do |reg|
      reg.birthday.stamp('2000-11-01')
    end

    column :role do |reg|
      status_tag(reg.role[0..3])
    end

    column 'Workshop' do |reg|
      reg.print_workshop
    end

  end


end

ActiveAdmin.register Registration do
  menu :parent => 'Raw Database'
  remove_filter :sleeps_in

  config.per_page = 100

  csv do
    column :first_name
    column :last_name

    column :email

    column :city
    column :country

    column :birthday
    column :role
    column :state
    column :processing_list
  end


end
