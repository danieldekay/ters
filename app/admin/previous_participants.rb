ActiveAdmin.register PreviousParticipant do
  menu :priority => 900
  config.per_page = 100

  action_item :only => :index do
    link_to 'Upload CSV', :action => 'upload_csv'
  end

  collection_action :upload_csv do
    render "admin/csv/upload_csv"
  end

  collection_action :import_csv, :method => :post do
    CsvDb.convert_save("PreviousParticipant", params[:dump][:file], ["state", "processing_list", 'status', 'zip', 'street', 'gender', 'id'], params[:dump][:tags], params[:dump][:colsep])
    redirect_to :action => :index, :notice => "CSV imported successfully!"
  end

  def scoped_collection
    PreviousParticipant.includes(:taggings)
  end

  index do
    column :id
    column :role
    column :first_name
    column :last_name
    column :email
    column :birthday
    column :city
    column :country
    column :facebook
    column :event_list do |item|
      output = item.event_list
    end
  end
end
