class CustomDeviseMailer < ActionMailer::Base
    include Devise::Mailers::Helpers
    helper InvitationMailerHelper

    default from: "#{Settings.marathon.name} <#{Settings.marathon.email}>"
    default bcc: "#{Settings.marathon.name} <#{Settings.marathon.email}>"

    def confirmation_instructions(record, opts={})
      devise_mail(record, :confirmation_instructions)
    end

    def reset_password_instructions(record, opts={})
      @user = record
      mail to: "#{@user.name} <#{@user.email}>", subject: "#{Settings.marathon.name} - Password reset"

    end

    def unlock_instructions(record, opts={})
      devise_mail(record, :unlock_instructions)
    end

    # you can then put any of your own methods here

    def invitation_instructions(record, opts={})

      @user = record
      mail to: "#{@user.name} <#{@user.email}>", subject: "This is your personal invitation to #{Settings.marathon.name}! RSVP"

    end


  end
