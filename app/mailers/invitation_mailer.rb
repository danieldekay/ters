class InvitationMailer < ActionMailer::Base

  default from: "#{Settings.marathon.name} <#{Settings.marathon.email}>"
  default cc: "#{Settings.marathon.name} <#{Settings.marathon.email}>"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.invitation_mailer.invite.subject
  #
  def invite(user)
    @user = user
    mail to: "#{user.name} <#{user.email}>",
         subject: "This is your personal invitation to #{Settings.marathon.name}! RSVP"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.invitation_mailer.remind.subject
  #
  def remind(user)
  end


  def send_email_notification_on_payment_and_suggestion_feature(reg)
    @user = reg.user
    # attachments.inline['invitation.png'] = File.read(Rails.root.join('app', 'assets', 'images', 'invitation.png'))
    mail to: "#{@user.name} <#{@user.email}>", subject: "You are IN the #{Settings.marathon.name}"
  end

  def send_payment_reminder(user)
    @user = user
    # attachments.inline['invitation.png'] = File.read(Rails.root.join('app', 'assets', 'images', 'invitation.png'))
    @subject = @user.registration.to_pay_date.past? ? "#{Settings.marathon.name} - please confirm/payment reminder" : "#{Settings.marathon.name} - payment instructions"
    mail to: "#{user.name} <#{user.email}>", subject: @subject
  end

  def send_email_receipt_self_suggestion(suggestion)
    @suggestion = suggestion
    mail to: "#{suggestion.name} <#{suggestion.email}>", subject: "Thank you for signing up for #{Settings.marathon.name}"
  end

  def new_comment(recipient, comment)
    @comment = comment
    @recipient = recipient
    mail to: "#{recipient.name} <#{recipient.email}>", subject: "#{Settings.marathon.name}- new comment posted"
  end

  def new_request(request)
    @request = request
    mail to: Settings.marathon.email, subject: "#{Settings.marathon.name}- new travel request posted"
  end

  #state :registration_received
  #state :waitinglist
  #state :please_pay
  #state :you_are_in

  def send_email_registration_received(reg)
    @reg = reg
    mail to: "#{reg.name} <#{reg.email}>", subject: "Thank you for signing up for #{Settings.marathon.name}"
  end
  def send_email_waitinglist(reg)
    @reg = reg
    mail to: "#{reg.name} <#{reg.email}>", subject: "You are currently on the waiting list for #{Settings.marathon.name}"
  end
  def send_email_please_pay(reg)
    @reg = reg
    @user = @reg.user
    mail to: "#{reg.name} <#{reg.email}>", subject: "Payment details for #{Settings.marathon.name}"
  end
  def send_email_you_are_in(reg)
    @reg = reg
    mail to: "#{reg.name} <#{reg.email}>", subject: "You are IN! #{Settings.marathon.name}"
  end
  def send_email_you_are_out(reg)
    @reg = reg
    mail to: "#{reg.name} <#{reg.email}>", subject: "We have moved you to the waiting list! #{Settings.marathon.name}"
  end

  def send_email_final_info(reg)
    @reg = reg
    @dormitories = @reg.dormitories

    mail to: "#{reg.name} <#{reg.email}>", subject: "[#{Settings.marathon.name}] #{reg.country == "Germany" ? "Letzte Infos" : "Final Facts"}"
  end

  def send_custom_email_via_backend(email, to)
    @email=email
    @to=to
    @message = @email.message
    @subject = @email.subject
    mail to: "#{@to.name} <#{@to.email}>", subject: "[#{Settings.marathon.name}] #{@subject}"
    logger.debug "MRS sending: #{@to.name}/#{@to.email}"
  end

  def send_shuttle_info(registration)
    @to=registration
    @shuttle_trips = registration.shuttle_trips
    mail to: "#{@to.name} <#{@to.email}>", subject: "[#{Settings.marathon.name}] Shuttle details"
    logger.debug "MRS sending: #{@to.name}/#{@to.email}"
  end

  def send_email_two_month_reminder(reg)
    @reg = reg
    @duration = (DateTime.parse(Settings.marathon.event.start)-DateTime.now).to_i
    mail to: "#{reg.name} <#{reg.email}>", subject: "#{@duration} days until #{Settings.marathon.name}"
  end

  def send_email_hotel_reservation(reg)
    @reg = reg
    @hotel_room = @reg.hotel_room
    mail to: "#{reg.name} <#{reg.email}>", subject: "[#{Settings.marathon.name}] Hotel Room Reservation"
  end

  def send_email_workshop_reservation_info(reg)
    @reg = reg
    @workshops = @reg.workshop_reservations

    mail to: "#{@reg.name} <#{@reg.email}>", subject: "Workshops at #{Settings.marathon.name}"
  end

  def send_email_workshop_confirmation(reg)
    @reg = reg
    @workshops = @reg.workshop_reservations

    mail to: "#{@reg.name} <#{@reg.email}>", subject: "Workshops at #{Settings.marathon.name} - Confirmation"
  end


end
