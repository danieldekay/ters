class ApplicationMailer < ActionMailer::Base
  default from: "#{Settings.marathon.name} <#{Settings.marathon.email}>"
  default cc: "#{Settings.marathon.name} <#{Settings.marathon.email}>"

  helper ApplicationHelper

  layout 'mailer'
end
