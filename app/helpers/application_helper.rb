module ApplicationHelper

  def roles
    [['leader', :leader], ['follower', :follower], ['both', :both]]
  end

  def dietary_options
    [:omnivore, :vegetarian, :vegan]
  end

  def dietary_options_incompatibility
    [:no_lactose, :no_gluten]
  end

  def gender_options
    [['man', :man], ['woman', :woman]]
  end

  def flip_role(role)
    role == :leader ? :follower : :leader
  end

  def profile_picture_tag(reg)
    picture_url = " "
    image_tag picture_url, :alt => 'profile picture', :size => '40x40', :title => reg.name
  end

  def profile_picture_tag_mini(user)
    if !user.facebook.nil?
      picture_url = user.facebook.blank? ? user.gravatar_url : 'http://graph.facebook.com/' + user.facebook + '/picture'
    else
      picture_url = ''
    end
    image_tag picture_url, :alt => 'profile picture', :size => '20x20', :title => user.name
  end

  def profile_picture_tag_for_facebook(id)
    if id.blank?
      return "<i class='fa fa-fw fa-user fa-3x text-muted'></i>".html_safe
    elsif id
      if id.start_with?('http')
        require 'uri'
        uri = URI::parse(id)
        id  = uri.path # results in: /questions/ask
      end
      picture_url = 'http://graph.facebook.com/' + id + '/picture'
      temp        = image_tag picture_url, :alt => 'profile picture', :size => '40x40', :class => 'text-center'
      link_to temp, "http://www.facebook.com/#{id}", target: "_blank"
    else
      return "<i class='fa fa-fw fa-user fa-3x text-muted'></i>".html_safe
    end
  end

  ## FORMATTERS & DECORATORS
  def print_in_euro(value)
    return "#{value} &euro;".html_safe
  end


  ## public_activity
  # For generating time tags calculated using jquery.timeago
  def timeago(time, options = {})
    options[:class] ||= "timeago"
    content_tag(:abbr, time.to_s, options.merge(:title => time.getutc.iso8601)) if time
  end

  # Shortcut for outputing proper ownership of objects,
  # depending on who is looking
  def whose?(user, object)
    case object
      when Post
        owner = object.author
      when Comment
        owner = object.user
      else
        owner = nil
    end
    if user and owner
      if user.id == owner.id
        "his"
      else
        "#{owner.nickname}'s"
      end
    else
      ""
    end
  end

  # Check if object still exists in the database and display a link to it,
  # otherwise display a proper message about it.
  # This is used in activities that can refer to
  # objects which no longer exist, like removed posts.
  def link_to_trackable(object, object_type)
    if object
      link_to object_type.downcase, object
    else
      "a #{object_type.downcase} which does not exist anymore"
    end
  end

  def invitation_acceptance_url (user)
    if Rails.env.development?

      if user.invitation_token.nil?
        return "http://localhost:3000"
      else
        return "http://localhost:3000/invitation/accept/#{user.id}/#{user.invitation_token}"
      end

    else
      if user.invitation_token.nil?
        return Settings.marathon.website
      else
        return Settings.marathon.registration_website + "/invitation/accept/#{user.id}/#{user.invitation_token}"
      end
    end
  end


  def asset_url asset
    "#{request.protocol}#{request.host_with_port}#{asset_path(asset)}"
  end

  def print_contentblock_field(content_block, field)
    block = Content.where(:usage => content_block).first_or_create(:usage => content_block, :content => "NEW BLOCK named #{content_block}", :content_type => "content_block")
    return block.try(field)
  end

  def search_links_social(first_name, last_name)
    result = link_to "<i class='fa fa-facebook-official' aria-hidden='true'></i>".html_safe, "https://www.facebook.com/search/more/?q=#{first_name}%20#{last_name}", target: "_blank"
    result += " "
    result += link_to "<i class='fa fa-google' aria-hidden='true'></i>".html_safe, "https://www.google.de/#newwindow=1&q=#{first_name}+#{last_name}+tango", target: "_blank"
    result += " "
    result += link_to "<i class='fa fa-linkedin' aria-hidden='true'></i>".html_safe, "https://www.linkedin.com/vsearch/p?keywords=#{first_name}+#{last_name}&trk=vsrp_people_sel", target: "_blank"

    result = result.html_safe
    return result
  end

  def format_boolean(b)
    result = b ? "<span class='glyphicon glyphicon-ok true' aria-hidden='true'></span>" : "<span class='glyphicon glyphicon-remove false' aria-hidden='true'></span>"
    result = result.html_safe
    return result
  end

  def format_role(role)
    if role
      case role.to_sym
        when :leader
          "<span class='btn btn-xs leader'>Leader</span>".html_safe
        when :follower
          "<span class='btn btn-xs follower'>Follower</span>".html_safe
        when :both
          "<span class='btn btn-xs both'>Both</span>".html_safe
      end
    end
  end

  def format_date(date)
    "<small>#{date}</small>".html_safe
  end

  def format_invitation_state(state)
    case state.to_sym
      when :start
        "<span class='true'>Not sent</span>".html_safe
      when :sent
        "<span class='true'>Open</span>".html_safe
      when :declined
        "<span class='false'>Declined</span>".html_safe
      when :expired
        "<span class='false'>Expired</span>".html_safe
    end
  end

  def format_registration_state(state)
    case state.to_sym
      when :start
        "<span class='label label-danger'>Error</span>".html_safe
      else
        "<span class='label #{state}'>#{state}</span>".html_safe
    end
  end

  def status_of_registration_with_email(email)
    begin
      user    = User.find_by_email(email)
      temp    = user.try(:invitation_sent_at)
      temp    = temp ? distance_of_time_in_words(Time.now - temp) : nil
      invited = temp ? "invited " + temp + " ago" : "suggested"

      if user && user.inscription && user.inscription.try(:start?)
        registered = nil
      elsif user
        registered = user.inscription.try(:updated_at)
      end

      registered ? "registered " + distance_of_time_in_words(Time.now - registered) + " ago" : invited
    rescue => e
      Rails.logger.error "Status of Registration failed. #{e}"
      return "error"
    end
  end

  def invitation_expiry_date_text
    "until #{eval(Settings.features.invitations.expiry).stamp('Monday, August 1st 2013')}"
  end

end
