// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require turbolinks
//= require bootstrap.min
//= require bootstrap-sprockets
// require_tree .
//= require ./backend/plugins/metisMenu/jquery.metisMenu
//= require ./backend/plugins/flot/jquery.flot.js
//= require underscore/underscore
//= require lbd/light-bootstrap-dashboard
//= require lbd/bootstrap-checkbox-radio-switch-tags
//= require lbd/bootstrap-selectpicker
// require lbd/jquery.bootstrap.wizard.min
//= require lbd/bootstrap-table
// require lbd/jquery.validate.min
