# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

#= require turbolinks
#= require bootstrap

#= require chosen/chosen.jquery.js
#= require wice_grid.js

#= require lbd/bootstrap-table
#= require lbd/light-bootstrap-dashboard
#= require lbd/chartist.min

#= require backend_statistics

# require split_table

$ ->
# enable chosen js
  $('.chosen-select').chosen
    allow_single_deselect: true
    no_results_text: 'No results matched'
    width: '200px'
  $('.table').on 'click', (e) ->
    cb = $(e.target).children(':checkbox')
    cb.attr 'checked', !cb.attr('checked')
    return
  $('.table').on 'click', 'tbody td, thead th:first-child', (e) ->
    $(this).parent().find('input[type="checkbox"]').trigger 'click'
    return

